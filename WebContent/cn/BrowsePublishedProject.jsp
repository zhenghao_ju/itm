<%-- 
    Document   : BrowsePublishedProject
    Created on : Apr 25, 2013, 1:16:24 PM
    Author     : Stan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Browse Inquiry Projects Published in the ITM Network</title>
        <LINK REL=StyleSheet HREF="../css/stickyhead.css" TYPE="text/css"></LINK>
        <LINK REL=StyleSheet HREF="../css/BrowsePublishProject.css" TYPE="text/css"></LINK>
    </head>
    <body>
    <div id="sticky">
    	<div id="header">
    <table border="0">
      <tbody>
        <tr>
          <td><img src="../img/itmlogo100x100.gif" alt="ITM LOGO" height="100" width="100"></td>
          <td style="background-color: #01B0F1;color:yellow;font-family:Arial, Helvetica, sans-serif;font-size:30px;" height="15" width="900">&nbsp;&nbsp;
            <center>
              Browse Inquiry Projects Published in the ITM Network
            </center></td>
        </tr>
      </tbody>
    </table>
    <div id = "wrapper" style="overflow:auto">
   	  <p>ITM网络致力于提供一个平台给全球的师生们来分享他们的知识。在这里大家可以发布各自的想法，思维以及建设性的对话交流。 </p>
    	<p>Published inquiry projects:  </p>
    	
    	<form name="form1" method="post" action="">
    	  <label for="searchProject">Search:</label>
    	  <input type="search" name="searchProject" id="searchProject" placeholder="Published Project">
          <label for="searchProject">in</label>
    	  <input type="text" name="searchProject" id="searchProject" value="任何地方">
   	  </form>
    	<p>(Later, we can have a world map here to assist the  navigation)</p>
      <table>
	<thead>
	<tr>
		<th width="13%">Topic of study</th>
		<th width="10%">Grade level</th>
		<th width="8%">Teacher</th>
        <th width="7%">School</th>
        <th width="11%">School year</th>
        <th width="35%">Knowledge Forum host and database name</th>
        <th width="16%">Action</th>
	</tr>
	</thead>
	<tbody>
	<tr>
		<td></td>
		<td></td>
		<td></td>
        <td></td>
        <td></td>
        <td></td>
        <td><a href="" >Teacher Reflection</a><br><a href="" >View Project</a>
</td>
	</tr>	
	</tbody>
</table>
	<div id="button">
            <a href="New_HomePage.jsp?database=${database}" onclick="" id="close" >Close</a>
	</div>
    </div>
   	 </div>
    </div>
    </body>
</html>
