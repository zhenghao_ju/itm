<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.*,java.sql.*,code.*,database.*;"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <jsp:directive.page contentType="text/html; charset=UTF-8"/>
<% 
/************validate the user session*******************/
request.setCharacterEncoding("UTF-8"); 
String username =" ";
String usertype =" ";
	session = request.getSession(false);
	if(session != null){
		if (session.getAttribute("username") == null){
			response.sendRedirect("/itm/index.jsp");
		}
		else
		{
		  username = (String)session.getAttribute("username");
		  usertype = (String)session.getAttribute("usertype");
		}
	}
%>
<script type="text/javascript" charset="UTF-8">
<%
String strsucceed = request.getParameter("ifsucceed");
String database = request.getParameter("database");
System.out.println("Database is ....."+database);

if(strsucceed != null){
	if(strsucceed.equals("yes")){
		String strname = request.getParameter("projectname");
	%>
		alert("这个探究课题已被保存!");
		window.opener.location='/itm/cn/New_HomePage.jsp?projectname=<%=strname%>&database=<%=database%>';
		window.close();
		
	<%}else if(strsucceed.equals("no")){%>
		alert("这个探究课题的名称已经存在!");
	<%}
}%>


	function OnSave(){
		if(document.Create_form.projectname.value=="")
			{
			  window.alert("请输入探究课题的名称!");  
			  return false;  
			}
		if(document.Create_form.teacher.value=="")
		{
		  window.alert("请输入老师的名字!");  
		  return false;  
		}
		if(document.Create_form.school.value=="")
		{
		  window.alert("请输入学校名称!");  
		  return false;  
		}
		if(document.Create_form.grade.value=="")
		{
		  window.alert("请选择年级!");  
		  return false;  
		}
		
		var from = parseInt(document.Create_form.fromyear.value);
		var to = parseInt(document.Create_form.toyear.value);
		if(from > to){
			alert("学年开始时间大于结束时间, 请重新选择!");
			return false;
		}
		/*if(document.Create_form.group.value=="")
		{
		  window.alert("please choose group!");  
		  return false;  
		}*/	
		return true;
	}
	
	function UpdateDefaultGroup(usrgroup)
	{
	  var index = 0;
	  var g = document.getElementById("groupNames");
	  //alert(g.length);
	  //alert(p[0].innerHTML);
	  for(var i =0; i<g.length; i++ )
	    {
	      var gName = g[i].innerHTML;
	      if(gName == usrgroup)
	        {
	          index = i;
	        }
	    }
	  g.selectedIndex=index;
	}
</script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>创建新课题</title>

<LINK REL=StyleSheet HREF="../css/stickyhead.css" TYPE="text/css"></LINK>
<LINK REL=StyleSheet HREF="../css/createProject.css" TYPE="text/css"></LINK>
</head>
<% 
    // delete status for new project is false 
    int deleted = 0;
    String usrgroup = null;
    //connect to database
	sqls s = new sqls();
	Operatedb opdb = new Operatedb(s,database);
	ResultSet rs = null;
	//get user group name
	String strcon ="group_table.idgroup = group_author.group_id and group_author.author_id = author_table.authorid and username ='" + username + "';";
    String[] tableNames = new String[3];
    tableNames[0] = "group_table";
    tableNames[1] = "group_author";
    tableNames[2] = "author_table";
    rs = opdb.MulGetRecordsFromDB(tableNames,"group_table.title",strcon,3);
    if(rs.next())
    {
        usrgroup = rs.getString(1);
        System.out.println("usergroup is###" +usrgroup);
    }
    //get group from database
	rs = opdb.GetRecordsFromDB("group_table","title","");
	
	//get the grade list
	List gradelist = new ArrayList();
	//int i=0;
	/*for(GradeSelect gradesopt:GradeSelect.values()){
		gradelist.add(gradesopt);
		//System.out.println("The "+ i + "th value of the Grade is :" + (GradeSelect)gradelist.get(i));
		//i++;
	}*/
	
	for(int i=1;i<=12;i++){
		gradelist.add(i);
		//System.out.println(gradelist.get(i-1));
	}

%>

<%
	//get the current year
	int curyear = Calendar.getInstance().get(Calendar.YEAR);
%>

<body onload="UpdateDefaultGroup('<%= usrgroup %>');">
<div id="sticky">
	<div id="header">
	<table border="0">
		<tr>
			<td><img src="../img/itmlogo100x100.gif" alt="ITM LOGO" height="100" width="100" /></td>
			<td style="background-color: #01B0F1;color:yellow;font-family:Arial, Helvetica, sans-serif;font-size:30px;" height="60" width="850">
			&nbsp;&nbsp;探究课题的基本信息
			</td>
		</tr>
	</table>
			
</div>
</div>
<div id="wrapperu">
<div id="wrapper">
	<form name="Create_form" action="../CreateProject?database=<%=database%>&username=<%= username %>&deleted=<%= deleted %>" method="post">
	<table>
		<tr>
			<td><b>课题名称:</b></td>
			<td><input class="fields" name="projectname" id="user" type = "text" size = "18" value = "" /></td>
			<td><b>*年级:</b></td>
			<th rowspan="2"><select name = "grade" multiple="multiple" size="4">
            	<option value="K">K</option>
            	<% int j=0;
            	for(j=0;j<gradelist.size(); j++){%>
            		<option value="<%=(Integer)gradelist.get(j)%>"><%=(Integer)gradelist.get(j)%></option>
            	<% 
            	}  %>	
            	<option value="UG">UG</option>
            	<option value="PG">PG</option>
            	<option value="Other">其他</option>              
            </select></th>
		</tr>
		<tr>
			<td><b>教师姓名:</b></td>
			<td><input class="fields" name = "teacher" id = "address" type = "text" size = "15" value = ""/></td>
			<td></td>
		</tr>
		<tr>
			<td><b>学校:</b></td>
			<td><input class="fields" name = "school" id = "year" type = "text" size = "30" value = ""/></td>
			<td><b>选择知识论坛用户组:</b></td>
			<td colspan="2"><select name="group" multiple="multiple" id="groupNames" size="4">
			    <% while(rs.next()){%>
			           <option value="<%=rs.getString(1)%>"><%=rs.getString(1)%></option>
			    <%}%>      
        	</select>
        	</td>
		</tr>
		<tr>
			<td><b>学年:</b>
			<td colspan="3" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;<b>从</b><select name="fromyear" style="float:none;">
				<option value="<%=curyear%>"><%=curyear%></option>
				<%for(int i=curyear-5; i<curyear+5;i++){%>			
					<option value="<%=i%>"><%=i%></option>
				<%}%>
			</select><b>至</b><select name="toyear" style="float:none;">
				<option value="<%=curyear%>"><%=curyear%></option>
				<%for(int i=curyear-5; i<curyear+5;i++){%>
					<option value="<%=i%>"><%=i%></option>
				<%}%>
			</select></td>
		</tr>
		<tr>
			<td></td>
			<td colspan="3">(*)CTRL+单击(Windows) 或者 command+单击(Mac)以选择多个项目</td>
		</tr>
	</table>
	<br>
	<br>
	<center>&nbsp;&nbsp;&nbsp;<input name="save" type="submit" id="create_save" value= "保存" onclick="return OnSave();"/>&nbsp;&nbsp;&nbsp;<input name="save" type="button" id="create_save" value= "取消" onclick="javascript: window.close();"/>&nbsp;&nbsp;&nbsp;</center>
     </form>
</div>
</div>
</body>
</html>
<%
	//release the connection to database
	if(rs != null){
		   try{
			   rs.close();
		   }catch (SQLException e){
			   e.printStackTrace();
		   }
	}
	if(opdb != null){
		   try{
			   opdb.CloseCon();
		   }catch (Exception e){
			   e.printStackTrace();
		   }
	}
	//s.Close();
%>