<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ page session = "true" %>
<%@ page import="java.util.*,java.sql.*,code.*,database.*"%>
<%@page import="code.AccessToProjects"%>
<%@page import="itm.servlets.Connect"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<%
request.setCharacterEncoding("UTF-8"); 
/************validate the user session*******************/
	String struser = " ";
    String usertype =" ";
    String host =" ";
    String dbase=" ";
    
	boolean ifvalid = false;
	session = request.getSession(false);
	if(session != null){
		if (session.getAttribute("username") == null){
			response.sendRedirect("/itm/index.jsp");
		}else{
			struser = (String)session.getAttribute("username");
			usertype = (String)session.getAttribute("usertype");
			host = (String)session.getAttribute("host");
			dbase = (String)session.getAttribute("dbase");
			ifvalid = true;
		}
	}
        String project=request.getParameter("projectname");
       
        session.setAttribute("project", project);
        
%>

<%
/*******************Fetch permissions for the existing usertype***************/
AccessToProjects atp = new AccessToProjects();
int publishedPermission = atp.fetchPermission("Published",usertype);
int currentKFdbPermission = atp.fetchPermission("Current_KF_db",usertype);
int groupPermission = atp.fetchPermission("Group", usertype);
int ownPermission = atp.fetchPermission("Own", usertype);
String projectn = request.getParameter("projectname");
String db = request.getParameter("database");
session.setAttribute("database", db);
System.out.println("Host name is "+host);
System.out.println("Database is "+dbase);

boolean canEditThread = false;
if( projectn == null )
{
  //do nothing
}
else
{
    System.out.println("Username is" + struser);
	System.out.println("Usertype is" + usertype);
	System.out.println("Project  name is" + projectn);
	System.out.println("Database name is" + db);
    canEditThread = atp.canEdit(struser,usertype,projectn,db);
}

System.out.println("Username is" + struser);
System.out.println("Usertype is" + usertype);
System.out.println("Project  name is" + projectn);
System.out.println("Database name is" + db);
System.out.println("Published Permission" + publishedPermission);
System.out.println("currentKFdbPermission" + currentKFdbPermission);
System.out.println("groupPermission" + groupPermission);
System.out.println("ownPermission" + ownPermission);
System.out.println("############Can edit thread" +canEditThread);

%>
<%
		String database = request.getParameter("database");
		ResultSet rs = null,rs_exist = null;
		ResultSet rs_deleted = null,rs_read = null,rs_author_group_id = null;
		
		Operatedb opdb = null,opdb_thread=null,opdb_deleted = null,opdb_existng = null;
		sqls s = new sqls();
		String author_group_id = null;
		String delete = "1";
		/********************read all existing projects from database*************************/
		if(database != null){
			Statement stmt = s.Connect(database);
			String strcon = null;
			if(usertype.equalsIgnoreCase("manager") || usertype.equalsIgnoreCase("editor"))
			{
			   
			   String strsql_existing = "SELECT projectname,teacher,fromyear,toyear FROM project where deleted = 0";
			   rs = stmt.executeQuery(strsql_existing);	
			}
			else if(usertype.equalsIgnoreCase("reader") || usertype.equalsIgnoreCase("visitor"))
			{
				
				String strsql_read = "SELECT projectname,teacher,fromyear,toyear FROM project where deleted = 0";
				rs_read = stmt.executeQuery(strsql_read);
			}
			else if(usertype.equalsIgnoreCase("writer") || usertype.equalsIgnoreCase("researcher"))
			{
				
				String strsql_author_group = "Select group_id from author_table,group_author where author_table.authorid = group_author.author_id and username ='"+ struser +"';";
				System.out.println(strsql_author_group);
				//rs_author_group_id = stmt.executeQuery(strsql_author_group);
				rs_author_group_id = stmt.executeQuery(strsql_author_group);
				if(rs_author_group_id.next())
				{
				   author_group_id = rs_author_group_id.getString("group_id");
				}
				System.out.println("author group id is "+ author_group_id);
				Statement stmt1 = s.Connect(database);
				String strsql_existing = "SELECT projectname,teacher,fromyear,toyear FROM project,project_group where deleted = 0 AND project.idproject = project_group.projectid "
                                        + "AND project_group.groupid ='"+author_group_id+"'"
                                        + " or project.projectowner = '"+ struser+"' and project.deleted=0 group by project.projectname;";
				rs = stmt1.executeQuery(strsql_existing);
				Statement stmt2 = s.Connect(database);
				String strsql_read = "SELECT projectname,teacher,fromyear,toyear from project where deleted =0 and project.projectname NOT IN(SELECT projectname from project,project_group where deleted = 0 and project.idproject = project_group.projectid and project_group.groupid ='"+author_group_id+"' or project.projectowner = '"+ struser+"' group by project.projectname);";
			    rs_read = stmt2.executeQuery(strsql_read);
			}
			else 
			{
				//do nothing for now.
			}
			Statement stmt1 = s.Connect(database);
			String strsql_deleted = "SELECT projectname,teacher,fromyear,toyear FROM project where deleted = 1";
			rs_deleted = stmt1.executeQuery(strsql_deleted);
		}
		
		ResultSet rs_thread = null;
		String r_thread[] = new String[1000];
		String teachername = null;
	   /**********************read current project information *******************************/
		String strname = request.getParameter("projectname");                        
		String strcon = null;
		if(strname != null){
                 
                    // strname = new String(request.getParameter("projectname").getBytes("ISO8859-1"),"UTF-8");   //Support recursive loading
			opdb = new Operatedb(s,database);
			
			strcon = "projectname='"+strname+"';";
			rs_exist = opdb.GetRecordsFromDB("project","projectname,teacher,school",strcon);
			if(rs_exist.next()){
				teachername = rs_exist.getString(2);
			}
		}
		

	/*******************read current project's thread*************************************/
	if(strname != null){
		//get existing threads
		String[] tablearray = new String[2];
		opdb_thread = new Operatedb(s,database);
		strcon = "projectname='"+strname+"' and idproject=projectid group by threadfocus";
		tablearray[0]="project";tablearray[1]="project_thread";
		rs_thread = opdb_thread.MulGetRecordsFromDB(tablearray,"threadfocus",strcon,2);
		int cr_thread = 0;
		while (rs_thread.next()){
			r_thread[cr_thread] = rs_thread.getString(1);
			cr_thread++;
		
		}
		if(rs_thread != null){
			try{
				rs_thread.close();
			}catch(SQLException e){
				e.printStackTrace();
			}
		}
	}
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>ITM主页面</title>
<LINK REL=StyleSheet HREF="../css/menu.css" TYPE="text/css">
<script language="javascript" type="text/javascript">
  var _projectname = "<%=request.getParameter("projectname")%>";
  var _database = "<%=request.getParameter("database")%>";
</script>
<script src="../js/header_mapthread.js"></script>
<script src="../js/cn/drawMap.js"></script>
<script src="../js/cn/loadMap.js"></script>
<script type="text/javascript">

/*set permissions for various usertype*/
function updateControls(publishedPermission, currentKFdbPermission, groupPermission, ownPermission)
{
	var selp = document.getElementById("Create_new_Project");
	
	if ((parseInt(ownPermission, 10) & 8) != 8)
	{
		console.log("[DEBUG] User does not has create new project permissions");
		
		// Disable create functionality
		selp.removeAttribute("href");
		selp.removeAttribute("onclick");
		selp.style.color  = "gray";
	}
}

/*Control update database by manager and editor*/
function updateDatabaseControl(usertype)
{
	var selc = document.getElementById("update_db");
	if(usertype != "editor" && usertype != "manager")
	{
		console.log("[DEBUG] User does not has update database permissions");
		//Disable update Database Functionality.
		selc.removeAttribute("href");
		selc.removeAttribute("onclick");
		//selc.setAttribute("id", "Create_new_Project_disabled");
		selc.style.color  = "gray";
	}
}

function updateDeletedProjectsControl(usertype)
{
  var del = document.getElementById("deleted_projects");
  var delp = document.getElementById("delete_p");
  if(usertype !="editor" && usertype != "manager")
  {
    console.log("[DEBUG] User does not has view deleted projects permissions");
	//Disable deleted projects Functionality.
	del.removeAttribute("href");
	del.removeAttribute("onclick");
	del.style.color  = "gray";
	if(delp != null)
	{
	  delp.style.visibility = "hidden";
	}
  }
 }

/*Control thread updates or creation*/
function updateControlThread(canEditThread)
{
	var selnt = document.getElementById("Create_new_Thread");
	if(selnt != null)
	{
		if (canEditThread == "false")
		{
			console.log("[DEBUG] User does not has create new thread permissions");
			
			// Disable create functionality
			selnt.removeAttribute("href");
			selnt.removeAttribute("onclick");
			selnt.style.color  = "gray";
		}
	}
}

</script>

<script type="text/javascript" charset="UTF-8" >
var timeout	= 500;
var closetimer	= 0;
var ddmenuitem	= 0;

// open hidden layer
function mopen(id)
{	
	// cancel close timer
	mcancelclosetime();

	// close old layer
	if(ddmenuitem) ddmenuitem.style.visibility = 'hidden';

	// get new layer and show it
	ddmenuitem = document.getElementById(id);
	ddmenuitem.style.visibility = 'visible';

}
// close showed layer
function mclose()
{
	if(ddmenuitem) ddmenuitem.style.visibility = 'hidden';
}

// go close timer
function mclosetime()
{
	closetimer = window.setTimeout(mclose, timeout);
}

// cancel close timer
function mcancelclosetime()
{
	if(closetimer)
	{
		window.clearTimeout(closetimer);
		closetimer = null;
	}
}

function OnCreate()
{
	window.open ('New_CreateProject.jsp?database=<%=database%>'); 
}
function OnOpenProject(name)
{
	window.location='New_HomePage.jsp?database=<%=database%>&projectname='+name;
}

function OnCreateThread(){
	window.open('thread.jsp?database=<%=database%>&projectname=<%=strname%>');
}

function OnOpenThread(focus){
	window.open('threadLoad.jsp?database=<%=database%>&projectname=<%=strname%>&threadfocus='+focus);
}
function OnRefresh(){
	
	window.open('New_Refresh.jsp?database=<%=database%>');

}
// close layer when click-out
document.onclick = mclose;
</script>
</head>
<body  onload="updateDatabaseControl('<%= usertype%>');updateDeletedProjectsControl('<%= usertype%>');updateControls('<%= publishedPermission%>', '<%= currentKFdbPermission%>', '<%= groupPermission%>', '<%= ownPermission%>');updateControlThread('<%= canEditThread%>');"> <!--bgcolor="#E3F5F9"  -->
	<div id="sticky">
	<div id="header">
	<table border="0">
		<tr>
			<td rowspan="2"><img src="../img/itmlogo100x100.gif" alt="ITM LOGO" height="100" width="100" /></td>
			<td><img src="../img/itmlogo840x70name.gif" alt="ITM LOGO" height="70" width="840" /></td>
		</tr>
		<tr>
			<td style="background-color:#01B0F1;">
			<ul id="sddm">
			<li><a href="#">探究课题</a>
			
			 <div class="m1">
			   <ul>
			  		<li><a onclick="OnCreate()"href="#" id ="Create_new_Project">设置新课题</a></li>
			  		<li id="existing_proj"><a href="" id="work_on_existing_project">打开/编辑</a> 
			              <% if(rs != null){
			            	  if(rs.isBeforeFirst()){
			              %>  
			              <div class ="exisitng_projects_div">
			                 <ul>
			              <%while(rs.next()){%>
				               <li> <a onclick="OnOpenProject('<%=rs.getString(1)%>')" href="#" title='<%=rs.getString(1)%> <%=rs.getString("teacher")%>, <%=rs.getString("fromyear")%> - <%=rs.getString("toyear")%>)'><%=rs.getString(1)%></a></li>    
				          <% 
				          } %> 
				          </ul>
				        </div>
			  		    <% } }%>
			        </li>
			        <li id="visit_proj"><a href="" id="visit_a_project">参观</a>
			  		     <%if(rs_read != null){
			  		    	 if(rs_read.isBeforeFirst()){
			  		     %>	
			  		     <div class ="visit_projects_div">
			           		<ul>
			  			  <%while(rs_read.next()){%>
				                <li> <a onclick="OnOpenProject('<%=rs_read.getString(1)%>')" href="#" title='<%=rs_read.getString(1)%> <%=rs_read.getString("teacher")%>, <%=rs_read.getString("fromyear")%> - <%=rs_read.getString("toyear")%>)'><%=rs_read.getString(1)%></a></li>
				         <% 
				         } %> 
				          </ul>
			             </div> 
			  		   <%  }}%>
			        </li>
			        <li id="deleted_proj"><a href=""  id="deleted_projects">回收站</a>
			            <% if(rs_deleted != null){
			            	if(rs_deleted.isBeforeFirst()){
			            %>  
			            <div class ="deleted_projects_div" id ="delete_p">
			  			   <ul>
			  			<%while(rs_deleted.next()){%>
				             <li> <a onclick="OnOpenProject('<%=rs_deleted.getString(1)%>')" href="#" title='<%=rs_deleted.getString(1)%> <%=rs_deleted.getString("teacher")%>, <%=rs_deleted.getString("fromyear")%> - <%=rs_deleted.getString("toyear")%>)'><%=rs_deleted.getString(1)%></a></li> 
				          <% 
				         }%>
				         </ul>
				        </div>  
			  		<% } }%>
			        </li>
			  </ul>  
			 </div>
			</li>
			<%if(strname!=null){%>
			<li><a href="#">思维脉络</a>
		        <div class="m1">
		        <ul>
			        <% if(strname != null){%>
			         	<li><a onclick="OnCreateThread()" href="#" id="Create_new_Thread">创建新脉络</a></li>
			        <% } %> 		
			  		<%if(r_thread[0] != null){%>
				        <%
				        int cnt = 0;
				        while(r_thread[cnt] != null){%>
				   
				       <li><a onclick="OnOpenThread('<%=r_thread[cnt]%>')"href="#"><%=r_thread[cnt]%></a></li>
				        <% 
				        cnt++;}
			        }%>      
			     </ul>
			    </div>
		      </li>
		    <li><a href="#">脉络网</a>
		      		<div class="m1" id="m3">
			      		<div id="m3threadlist">
			      		<form name="get_threads" id="get_threads">
			      		<%if(r_thread[0] != null){%>
			      		选择要显示的脉络<br>
			      		-------------<br>
					        <%
					        int cnt = 0;
					        while(r_thread[cnt] != null){%>
					        <div><input type="checkbox" name="threadcheck" value="<%=r_thread[cnt]%>"/><%=r_thread[cnt]%></div>
					        <% 
					        cnt++;}
				        }%>
			      		</form>
			      		</div>
			      		<div id="choosemapcontral">
			      		
			      		<a href="#" onclick="mapselectall()" style="float: left;width:98px;padding: 5px 0px;">全选</a>
			      		<a href="#" onclick="mapreset()"style="float: right;width:98px;padding: 5px 0px;">重置</a>
			      		</div>
		      		   
		      		<div>
		      		
		      		 <a href="#" onclick="MapLoad()">显示脉络网</a>
		      		
		      		</div>
		      	 </div> 		
		      </li>
			<% }else{%>
				<li class="active"><a href="#"><font color="gray">思维脉络</font></a></li>
				<li class="active"><a href="#"><font color="gray">脉络网</font></a></li>
			<% } %>
			    <li><a href="">网络管理</a>
			      <div class ="m1">
			          <ul> 
			    	     <li><a href="New_Refresh.jsp?database=<%=database%>&projectname=<%=strname%>&host=<%= host%>&dbase=<%= dbase %>" id ="update_db">更新数据库</a></li>
			    	    <li><a href="/itm/cn/PublishProject.jsp?database=<%=database%>" id ="publish_project">发布课题</a></li>
                                    <li><a href="/itm/cn/BrowsePublishedProject.jsp" id ="publish_project">浏览发布的课题</a></li>
			          </ul>
			       </div>
			     </li>
				<li><a href="/itm/index.jsp?logout=1&host=<%= host%>&dbase=<%= dbase %>">登出</a>
<!-- 				<div class="m1"> -->
<!-- 			  		<a href="/itm/index.jsp?logout=1">Logout ITM</a> -->
<!-- 			  	</div> -->
			  	</li>
			  </ul>
			</td>
		</tr>
	</table><%if(strname != null){%>
		<div id="projinfo">
			<%
			int threadcnt = 0;
    		while(r_thread[threadcnt] != null){threadcnt++;} 
    		%>
	<p><font>你选择的探究课题：</font>
	<a href="infoproject.jsp?database=<%=database%>&projectname=<%=strname%>" target="_blank"><%=strname%></a> (教师: <%=teachername%>)&nbsp;&nbsp;这个探究课题有 <%=threadcnt%> 条思维脉络. 点击“思维脉络”菜单以添加或查看/编辑思维脉络。点击 “脉络网”显示多条思维脉络。
	<%if (threadcnt>0){ %>  选择 "<bold>"思维线网"</bold>" 以显示 多条思维线索.<%} %></p>
	<hr>
</div>
	
	
	<%} %>
			
</div>
</div>
<div id="wrapperu">
<div id="wrapper">
<%
if(strname==null){%>
<div style="visibility: visible;">
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b></b>欢迎使用“团队思维脉络图谱”（Idea Thread Mapper, 简称 ITM）！ ITM是一个协作知识展示和反思工具，可以帮助学习共同体在网上讨论过程中进行有效的协作思维和探究。学生使用“知识论坛”等工具进行网上讨论和知识建构，使用ITM来找出讨论的主题，梳理思维的脉络，反思探究的进展，找出需要进一步深入探究的问题。ITM提供以下功能来帮助你们回顾和深化你们的讨论：</p>
<p><b>设置探究课题：</b>填写探究的内容（如课程单元）和相关的信息，包括学生的年级、教师的姓名、学校名称、学年、知识论坛用户组等。
<p><b>创建思维脉络：</b>设置思维脉络的主题，找出对这一主题有贡献的重要知识、看法和见解（如知识论坛上的短文），将包含这些内容的短文显示在一条时间轴上，以时间脉络的形式显示探究的进程，撰写关于探究进展的反思。
<p><b>显示思维脉络网：</b>将整个探究课题中的全部或部分思维脉络显示在同一条时间轴上，展示协作探究的历程，从而帮助学习共同体对探究的主要进展进行反思，找出下一步需要集中深入探究的问题和内容。
 <p>
 <p>随着探究和讨论的进一步深入，学生可以随时返回到ITM，重新审视或编辑原有的思维脉络，添加新的思维脉络。

 <p>让我们的思想和见解相互连接，不断建构新知识，并利用我们的知识建立一个更美好的世界！</p>
 

<hr>
<center><img src="../img/nsf.gif" alt="nsf" height="42" width="42" />团队思维脉络图谱（ITM）由美国国家科学基金会资助开发。
    <p>研究项目主持人：美国纽约州立大学奥本尼分校张建伟博士（教育学院）和陈美华博士（计算机系）。 </p>
</center>
</div>
<%	
}
%>
<div id="raw_area"></div>
<div id="ctr">
		<ul id="flag">
		<li>&nbsp;&nbsp;&nbsp;&nbsp;Note :&nbsp;&nbsp;</li>
		<li id="showalltitle">&nbsp;|&nbsp;<a href="javascript:void(0)" onclick="showalltitle()">显示标题</a>&nbsp;|&nbsp;</li>
		<li id="showallauthor"><a href="javascript:void(0)" onclick="showallauthor()">显示作者</a>&nbsp;|&nbsp;</li>
		<li id="showalllink">
		<a href="javascript:void(0)" onclick="showbldlink()">显示发展关系</a>&nbsp;|&nbsp;
		<a href="javascript:void(0)" onclick="showreflink()">显示引用关系</a>&nbsp;|&nbsp;
		</li>
		</ul>
		
		<ul id="zoom">
		<li>显示范围:&nbsp;&nbsp;</li>
		<li><a href="javascript:void(0)" onclick="bar(0)";>两天</a>&nbsp;|&nbsp; </li>
		<li><a href="javascript:void(0)" onclick="bar(1)";>一周</a>&nbsp;|&nbsp; </li>
		<li><a href="javascript:void(0)" onclick="bar(2)";>两周</a>&nbsp;|&nbsp; </li>
		<li><a href="javascript:void(0)" onclick="bar(3)";>单月</a>&nbsp;|&nbsp; </li>
		<li><a href="javascript:void(0)" onclick="bar(4)";>三月</a>&nbsp;|&nbsp; </li>
		<li><a href="javascript:void(0)" onclick="bar(5)";>全部</a> </li>
		</ul>
</div>
<div id="time_area">
	<div id="time_left">时间:</div>
	<div id="time_right"></div>
</div>
<div id="bar_area">
	<div class="l_left" id="bar_left"></div>
	<div class="l_right" id="bar_right">
	<div id="bar_w_left" onclick="windowmoveleft()"></div>
	<div id="bar_b">
		<div id="pointer" onmousedown="ptdown(event)" onmouseup="ptup(event)"></div>
	</div>
	<div id="bar_w_right" onclick="windowmoveright()"></div>
	</div>
</div>

<div id="draw_area"></div>
</div>
</div>	
<%
	//release the database connection
	if(rs != null){
		   try{
			   rs.close();
		   }catch (SQLException e){
			   e.printStackTrace();
		   }
	}

	if(rs_deleted != null){
		   try{
			   rs_deleted.close();
		   }catch (SQLException e){
			   e.printStackTrace();
		   }
	}


	if(opdb != null){
		opdb.CloseCon();
	}
	
	if(opdb_thread != null){
		opdb_thread.CloseCon();
	}
	
	if(s != null){
		s.Close();
	}
%>

</body>
</html>
