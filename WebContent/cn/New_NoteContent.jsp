<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="java.util.*,java.sql.*,code.*,database.*"%>
<%@ page import="itm.controllers.NoteController"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	/************validate the user session*******************/
	session = request.getSession(false);
	if(session != null){
		if (session.getAttribute("username") == null){
			response.sendRedirect("/itm/index.jsp");
		}
	}
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Note Content</title>
</head>
<%
	boolean ifscaffold = false;
	String strid = request.getParameter("noteid");
	String strdb = request.getParameter("database");
	NoteController nc = new NoteController(strdb);	
	//sqls s = new sqls();
	Operatedb opdb = new Operatedb(new sqls(),strdb);
	Operatedb opdb_author = new Operatedb(new sqls(),strdb);
	Operatedb opdb_scaffold = new Operatedb(new sqls(),strdb);
	String strtemp = "note_table.noteid="+strid+
	                 " AND note_table.noteid=view_note.noteid AND view_note.viewid=view_table.idview";
	String[] namearray=new String[3];
	namearray[0] = "note_table";namearray[1]="view_table";namearray[2]="view_note";
	ResultSet rs = opdb.MulGetRecordsFromDB(namearray,"notetitle,notecontent,createtime,view_table.title",strtemp,3);
	rs.next();
	
	//check if the note contains any scaffold
	String[] tablearray=new String[2];
	strtemp = "support_note.noteid="+strid+
			  " AND support_note.supportid=support_table.supportid";
	tablearray[0]="support_table";tablearray[1]="support_note";
	// FIXME scaffold feature commented for now
	//ResultSet rs_sf = opdb_scaffold.MulGetRecordsFromDB(tablearray,"support_table.text,support_note.text",strtemp,2);
	
%>
<body onload="window.resizeTo(document.getElementById('wrapper').offsetWidth, document.getElementById('wrapper').offsetHeight+78)">
<div id="wrapper">
<div style="background:#1ABDE6">
	 <center><b><%=rs.getString(1) %></b></center>
	 <center>作者：  <%List authorList = opdb_author.getMultipleAuthors(strid);
					  for(int i=0;i<authorList.size();i++){
						  String name = (String)authorList.get(i);
                                                  name=StringUtils.AdjustChineseName(name);
                                                  %>
						  <%=name %>
						  <%if(i < authorList.size()-1){
						  %>
						  ,&nbsp;
						  <%}%>	  
					  <%}%>
	</center>
	 <center><%=rs.getString(3) %></center>	
</div>
<%
	//while(rs_sf.next()){
		//ifscaffold = true;
		//break;
	//}
	if(!ifscaffold){%>
		<div>
<%-- 		<%=rs.getString(2)%> --%>
<%-- 		<%= nc.getOffsetContent(strid)%> --%>
		<%= nc.getContent(strid)%>
		</div>
	<%}else {%>
		<div>
			<%-- 	<%=rs_sf.getString(1)%> &nbsp;<%=rs_sf.getString(2) %> --%>
<%-- 			<%= nc.getOffsetContent(strid)%> --%>
			<%= nc.getContent(strid)%>
		</div>	
	
	<%}%>

<br/>
<br/>
<br/>
<br/>
<div style="background:#1ABDE6">
<center><b>短文视窗:</b> <%=rs.getString(4) %></center>
</div>
</div>
</body>
</html>
<%
	if(rs != null){
		try{
			rs.close();
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
 /*	if(rs_sf != null){
		try{
			rs_sf.close();
		}catch(SQLException e){
			e.printStackTrace();
		}
	}*/
	if(opdb != null){
		opdb.CloseCon();
	}
	if(opdb_author != null){
		opdb_author.CloseCon();
	}
	if(opdb_scaffold != null){
		opdb_scaffold.CloseCon();
	}
%>