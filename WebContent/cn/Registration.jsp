<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page session="true"%>
<%@ page import="java.util.*" %>
<%@ page import="javax.mail.*" %>                       
<%@ page import="javax.mail.internet.*" %>           
<%@ page import="javax.activation.*" %>               
<%@ page import="java.util.*,java.io.*" %>

<%@page import="database.Operatedb"%>
<%@page import="code.*"%>
<%@ page import="java.sql.*" %>

<!DOCTYPE html>
<html>
<%
	String strsucceed = request.getParameter("ifsucceed");
	String strlogout = request.getParameter("logout");
	if (strlogout != null) {
		session.invalidate();
	}
%>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>欢迎使用--团队思维脉络图谱</title>
<script type="text/javascript">
	var info = '<%=strsucceed%>';
	if (info == 'no') {
		alert("资料填写有勿，请重新尝试");
	}
	if (info == 'yes') {
		window.alert("您的申请已提交, 当申请通过后您将收到一封确认邮件。");
	}
	
</script>
        <script src="../js/jquery-1.6.min.js"></script>
        <script src="../js/jquery.validate.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#signup').validate({
            rules:{                   
                db:"required",
                port:{
                    required:true,
                    digits:true
                },
		id: {
				required: true,
				minlength: 2
			},
		password: {
				required: true,
				minlength: 5
			},
                Email:{
                    required:true,
                    email:true
                },
                URL:{
                    required:true
                },
                phone:{
                    minlength:7,
                    digits:true
                }
            },//end of rules
            messages:{
                Email:{
                    required:"请输入电子邮件地址",
                    email:"这不是有效的电子邮件地址"
                },
                port:{
                    required:"请填写URL端口",
                    digits:"URL端口不正确"
                },
                password:{
                    required:'请填写服务器登录密码',
                    minlength:'密码长度不符合标准'
                },
                id:{
                    required:'请填写服务器登录帐号',
                    minlength:'帐号长度不符合标准'
                },
                phone:{
                    minlength:'电话号码过短',
                    digits:"电话号码不正确"
                }
            }            
        });
    })

</script>

</head>
<LINK REL=StyleSheet HREF="../css/default.css" TYPE="text/css"></LINK>
<body bgcolor="aliceblue">
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<center>
		<form style="border-width:1px;border-style:solid;border-color: #006600;width:50%"
                      name="registration_form" action="../Register" method="post"  id="signup">
			<br />
			<h2>
				<font size="8" color="#71C2B1" face="Times New Roman"> 团队思维脉络图谱 </font>
			</h2>
			<br>
			<h3>请填写知识论坛信息</h3>
			<br>		 
			<table>		
			    <tr>
                                <td><font size="3" color="#71C2B1" face="Times New Roman"><label for="URL"><b>URL</b>
						</label></font></td>
					<td><input class="fields" name="URL" id="URL"
                                                   type="text" size="20" value="" class="required error" title="请填写URL"/></td>
				</tr>
				
				<tr>
                                    <td><font size="3" color="#71C2B1" face="Times New Roman"><label for="port"><b>URL端口:</b>
						</label></font></td>
					<td><input class="fields" name="port" id="port"
						type="text" size="20" value="80" class="required error"/></td>
				</tr>
				
				 <tr>
					<td><font size="3" color="#71C2B1" face="Times New Roman"><label><b>数据库：</b>
						</label></font></td>
					<td><input class="fields" name="db" id="db"
                                                   type="text" size="20" value="" class="required error" title="请填写数据库名"/></td>
				</tr>
			
				<tr>
					<td><font size="3" color="#71C2B1" face="Times New Roman"><label><b>组织：</b>
						</label></font></td>
					<td><input class="fields" name="org" id="org"
						type="text" size="20" value="" /></td>
				</tr>
				<tr>
					<td><font size="3" color="#71C2B1" face="Times New Roman"><label><b>用户名：</b>
						</label></font></td>
					<td><input class="fields" name="id" id="id"
						type="text" size="20" value="" class="required error"/></td>
				</tr>
				<tr>
                                    <td><font size="3" color="#71C2B1" face="Times New Roman"><label for="password"><b>密码：</b>
						</label></font></td>
					<td><input class="fields" name="password" id="password"
						type="password" size="20" value="" class="required error"/></td>
				</tr>
				
				
								
			</table>
			</br>
			<h3>请填写您的个人信息</h3>
			</br>
			<table>	
                                <tr>
					<td><font size="3" color="#71C2B1" face="Times New Roman"><label><b>姓:</b>
						</label></font></td>
					<td><input class="fields" name="lname" id="lname"
						type="text" size="20" value="" /></td>
				</tr>
                                
				<tr>
                                    <td><font size="3" color="#71C2B1" face="Times New Roman"><label for="fname" ><b>名:</b>
						</label></font></td>
					<td><input class="fields" name="fname" id="fname"
						type="text" size="20" value=""  class=""/></td>
				</tr>
				
				
				<tr>
                                    <td><font size="3" color="#71C2B1" face="Times New Roman"><label for="Email" generated="true" ><b>E-mail:</b>
						</label></font></td>
					<td><input class="fields" name="Email" id="Email"
                                                   type="text" size="20" value="" class="required error"/></td>
				</tr>
				
				<tr>
                                    <td><font size="3" color="#71C2B1" face="Times New Roman"><label for="phone"><b>联系电话:</b>
						</label></font></td>
					<td><input class="fields" name="phone" id="phone"
                                                   type="text" size="20" value="" title="请填写联系电话"/></td>
				</tr>
			</table>
			
			<font color="#4889EB" size="3" face="Times New Roman"><b><input
					style="width: 80px; height: 31" name="submit" id="" type="submit"
					value="确认"></b></font> 
					<br /> <br />
					<h6>Idea Thread Mapper (ITM) version 1.11, updated on April 08, 2013</h6>
					
			<input type="hidden" name="url"  id="url" />
			
		</form>
	</center>
</body>
</html>