<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="itm.controllers.Stemmer"%>
<%@page import="javax.sql.rowset.CachedRowSet"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.util.*,java.sql.*,com.knowledgeforum.k5.common.*,org.zoolib.*,org.zoolib.tuplebase.*,code.*,database.*;"%>
<html>
<head>
<link rel=stylesheet href="../css/stickyhead.css" type="text/css"></link>

<% response.setCharacterEncoding("UTF-8");
    request.setCharacterEncoding("UTF-8"); 
	/************validate the user session*******************/
	session = request.getSession(false);
	if(session != null){
		if (session.getAttribute("username") == null){
		response.sendRedirect("/itm/index.jsp");
		}
	}

	/**********read the noteid contained in the current thread from database*******/
   String strproname = request.getParameter("projectname");
   String strfocus = request.getParameter("threadfocus");
   String strdb = request.getParameter("database");
   String proid = null;
   
   ResultSet rs = null;
   CachedRowSet crs = null;
   ResultSet temprs = null;
   ResultSet ntidtime = null;
   
   sqls s = new sqls();
   Operatedb opdb = new Operatedb(s,strdb);
   Operatedb opdb_author = null;
   
 	//get the project id
	temprs=opdb.GetRecordsFromDB("project","idproject","projectname='"+strproname+"';");
	if(temprs.next()){
		proid = temprs.getString(1); 
	}
   
   String strcon = "projectid=" + proid +" and threadfocus='" + strfocus + "';";
   temprs = opdb.GetRecordsFromDB("thread_note","noteid",strcon);
   
   List<String> idlist = new ArrayList<String>();
   while(temprs.next()){
	   idlist.add(temprs.getString(1));
   }
   
   if(temprs != null)
   {
   	 try
     {
	   temprs.close();
     }
   	 catch(SQLException e)
     {
	   e.printStackTrace();
     }
   }
%>

<%//System.out.println("+++++++++++++++++++++++++searchnote.jsp++++++++++++++++++++++++");
//System.out.println("searchnote.jsp the project name is "+request.getParameter("projectname"));
//System.out.println("searchnote.jsp the thread focus name is "+request.getParameter("threadfocus"));%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
	<!-- link calendar resources -->
	<link rel="stylesheet" type="text/css" href="../css/tcal.css" />
        <link rel=stylesheet href="../css/thread.css" type="text/css"/>
	<script type="text/javascript" src="../js/tcal.js"></script> 
	<script type="text/javascript" src="../js/cn/thread_update.js"></script>

	<script type ="text/javascript">
	var count = 0;
	function UpdateCount(rowCnt)
	{
		count = rowCnt;
		document.getElementById("updatecnt").innerHTML = count;
		//alert(count);
	}
	var cnt = 0;
	function UpdateNoteSelected(rowObject)
	{
		//alert(rowObject);
		//alert(rowObject.checked);
		if(rowObject.checked == true)
		{
		  cnt++;
		}
		if(rowObject.checked == false)
		{
		  cnt--;
		}
		document.getElementById("updatenoteselected").innerHTML = cnt;
	}
	function SendNoteid()
	{
		// FIXME: Remove debug messages
		//alert("Calling SendNoteid in SearchMore.jsp");
		top.document.getElementById("framesetter").rows = "0%,100%";
		var idTimestampMapString = "";
		var num = document.searchnote_form.notecheck.length;
		var TimestampForAddNotes = new String(AddTimeForNotesInThread());
		
		// FIXME: Remove debug messages
		//alert(TimestampForAddNotes);
		//alert(document.searchnote_form.notecheck.length);
		
		var ids="@";
		var putvalue;
		var proname;
		var num=0;
		proname= "<%=request.getParameter("projectname")%>";
		if(document.searchnote_form.notecheck.length == undefined)
		{
			ids += document.searchnote_form.notecheck.value + "@";
			idTimestampMapString += document.searchnote_form.notecheck.value + "=" + TimestampForAddNotes + ";";
			num++;
		}
		else
		{
			for(var i=0;i<document.searchnote_form.notecheck.length;i++)
			{
				if(document.searchnote_form.notecheck[i].checked)
				{
					num++;
					ids += document.searchnote_form.notecheck[i].value + "@";
					idTimestampMapString += document.searchnote_form.notecheck[i].value + "=" + TimestampForAddNotes + ";";
				   	//alert(document.searchnote_form.notecheck[i].value);
				}
			}
		}
		
		if(num==0)
		{
			alert("Please select note first!");
			return false;
		}
		if(num > 500)
		{
			alert("You selected too many notes, please select again!");
			return false;
		}
		
		// Add the old id list
		<%
		if(idlist.size() > 0)
		{
		  for(int j = 0; j < idlist.size(); j++)
		  { 
		  %>
		    ids += <%=idlist.get(j)%> + "@";
		  <%
		  } 
	    }
		 
	    // Get the noteid and the addtime from thread_note table
	   	String[] columnNames = new String[2];
	    columnNames[0] = "noteid";
	   	columnNames[1] = "addtime";
		ntidtime = opdb.GetMultipleRecordsFromDB("thread_note",columnNames, strcon);
		   
		while(ntidtime.next())
		{
		%>	
		  idTimestampMapString += <%= ntidtime.getString("noteid")%> + "=" + "<%= ntidtime.getString("addtime")%>" + ";" ;
		<%
		}
		
		if(ntidtime != null)
		{
		  try
		  {
		    ntidtime.close();
		  }
		  catch(SQLException e)
		  {
		    e.printStackTrace();
		  }
		}
		%>
		
		//alert("Map is " +  idTimestampMapString);
		
		putvalue = ids;
		//alert(putvalue);
		
	//javascript form
		var f = document.createElement("form");
		f.setAttribute('method',"POST");
		f.setAttribute('action',"../cn/thread.jsp");
		
		var i = document.createElement("input");
		i.setAttribute('type',"hidden");
		i.setAttribute('name',"database");
		i.setAttribute('value','<%= request.getParameter("database")%>');
		
		var s = document.createElement("input");
		s.setAttribute('type',"hidden");
		s.setAttribute('name',"projectname");
		s.setAttribute('value','<%= request.getParameter("projectname")%>');
		
		var t = document.createElement("input");
		t.setAttribute('type',"hidden");
		t.setAttribute('name',"threadfocus");
		t.setAttribute('value',"<%= request.getParameter("threadfocus")%>");
		
		var u = document.createElement("input");
		u.setAttribute('type',"hidden");
		u.setAttribute('name',"getvalue");
		u.setAttribute('value',putvalue);
		
		var v = document.createElement("input");
		v.setAttribute('type',"hidden");
		v.setAttribute('name',"nidts");
		
		// Replace whitespace with the html code or else the http will truncate the string
		idTimestampMapString = idTimestampMapString.replace(/ /g, "&#32;");
		v.setAttribute('value',idTimestampMapString);
		
		var w = document.createElement("input");
		w.setAttribute('type',"submit");
		w.setAttribute('value',"Submit");
		
		f.appendChild(i);
		f.appendChild(s);
		f.appendChild(t);
		f.appendChild(u);
		f.appendChild(v);
		f.appendChild(w);
		document.body.appendChild(f);
		
		f.submit();
	}

	function OpenThreadPage(focus){
           top.window.location='threadLoad.jsp?database=<%=strdb%>&projectname=<%=strproname%>&threadfocus='+focus;          
        }
	


	function SelectAll(){
	    cnt = 0;
		if(document.searchnote_form.notecheck.length == undefined){
			document.searchnote_form.notecheck.checked = true;
		}else{
			for(var i=0;i<document.searchnote_form.notecheck.length;i++){
				document.searchnote_form.notecheck[i].checked = true;
				cnt++;
			}
		}
		document.getElementById("updatenoteselected").innerHTML = cnt;
	}
	function DeselectAll(){
		cnt = 0;
		if(document.searchnote_form.notecheck.length == undefined){
			document.searchnote_form.notecheck.checked = false;
		}else{
			for(var i=0;i<document.searchnote_form.notecheck.length;i++){
				document.searchnote_form.notecheck[i].checked = false;
			}
		}
		document.getElementById("updatenoteselected").innerHTML = cnt;
	}
	function CheckTime(){
		if(document.searchnote_form.fromtime.value=="" && document.searchnote_form.totime.value!="")
			{	
				window.alert("You can't leave the 'From' input box empty!");
				return false;
			
			}
		if(document.searchnote_form.fromtime.value !="" && document.searchnote_form.totime.value=="")
		{	
			window.alert("You can't leave the 'To' input box empty!");
			return false;
		
		}
		
		if(document.searchnote_form.views.value=="")
		{
		  window.alert("please choose one view!");  
		  return false;  
		}
	}
	function CheckValidate(){
		if(document.searchnote_form.fromtime.value=="")
		{
		  window.alert("please input 'From Time'!");  
		  return false;  
		}
		if(document.searchnote_form.totime.value=="")
		{
		  window.alert("please input 'To Time'!");  
		  return false;  
		}
		if(document.searchnote_form.views.value=="")
		{
		  window.alert("please choose one view!");  
		  return false;  
		}
		
		return true;
	}
	function ShowContent(noteid){
		var body = document.body,
	    html = document.documentElement;

		var height = Math.max( body.scrollHeight, body.offsetHeight, 
	                       html.clientHeight, html.scrollHeight, html.offsetHeight );
// 		alert("Height = "+height);
		window.open('../cn/New_NoteContent.jsp?database=<%=request.getParameter("database")%>&noteid='+noteid,"notewin","height='+height+', width=750,toolbar=no,scrollbars=yes,menubar=no");
	}
	
	/*function hideDiv()
	{
	   document.getElementById('divNote').style.visibility ='hidden';
	}*/
	/* Hide the number of notes selected and number of notes found until search notes is clicked*/
	function ShowDiv(searchVal)
	{
	  var s = searchVal;
	  var v = document.getElementById("divNote");
	  if(s == "null")
	  {
	     v.style.visibility='hidden';
	  }
	  else
	  {
	    v.style.visibility='visible';
	  }
	}
	/* Enable Disable select buttons until notes are listed */
	function ShowDivSelect(numNotes)
	{
	  var sButton = document.getElementById("selbutton").children;
	  //alert(numNotes);
	  //alert(document.getElementById("selbutton").children.length);
	  
	  if(numNotes < 1)
	  {
	    for(var i = 0; i<sButton.length;i++)
	    {
	      sButton[i].removeAttribute("href");
	      sButton[i].removeAttribute("onclick");
	      sButton[i].style.color ="gray";
	    }
	  }
	}
	
	</script>
</head>



<%
	/*********************************read all views title from database******************************/
	List vtitlelist = new ArrayList();
	String strtitle;
	//ResultSet rs = null;
	Statement stmt;
	//Operatedb opdb = null;
	String stryear ;
	String strmonth ;
	String strday ;
	String strfrtime=null;
	String strtotime=null;
	String strrange=null;
	String searchval =null;
	String[] strview = null;
	String strword1="", strword2="",strword="";
	stmt= s.Connect(strdb);
	
	try{
			rs = stmt.executeQuery("SELECT * from `view_table`;");
			while(rs.next()){
				strtitle = rs.getString(2);	
				vtitlelist.add(strtitle);			
			}
		}catch(SQLException e){
			e.printStackTrace();		
		}
	/*************************************************************************************************/
	/*************************Get all notes contained in view chosen from database*******************/
	if(request.getParameter("search") != null){ //user clicked on the search buttom
		System.out.println("user click on the search button"+request.getParameter("search"));
	    searchval = request.getParameter("search");
		strfrtime = request.getParameter("fromtime");
		strtotime = request.getParameter("totime");
		strview = request.getParameterValues("views");
		strword = request.getParameter("keywords");
		strrange = "";
		System.out.println("range is "+strrange);
		System.out.println("keywords is "+strword);
		
		if(request.getParameter("range")!= null)
		{
			strrange = request.getParameter("range");
			if(strrange.equals("短文标题")){
				strword2 = strword;
			}else if(strrange.equals("短文内容")){
				strword1 = strword;
			}else if(strrange.equals("任何地方")){
				strword1 = strword;
				strword2 = strword;
			}
		}
		
		
		//change the date format if the time period is not empty
		/*
		if( !strfrtime.equals("earliest") && !strtotime.equals("latest")){
			int ifirst = strfrtime.indexOf('/');
			int ilast = strfrtime.lastIndexOf('/');
			stryear = strfrtime.substring(ilast+1,strfrtime.length());
			strmonth = strfrtime.substring(0,ifirst);
			strday = strfrtime.substring(ifirst+1,ilast);
			
			strfrtime = stryear + "-"+strmonth+"-"+strday;
			
		    ifirst = strtotime.indexOf('/');
		    ilast = strtotime.lastIndexOf('/');
			stryear = strtotime.substring(ilast+1,strtotime.length());
			strmonth = strtotime.substring(0,ifirst);
			strday =strtotime.substring(ifirst+1,ilast);
			
			strtotime = stryear + "-"+strmonth+"-"+strday;		
		}*/
		
		if(!strfrtime.equals("earliest")){
			int ifirst = strfrtime.indexOf('/');
			int ilast = strfrtime.lastIndexOf('/');
			stryear = strfrtime.substring(ilast+1,strfrtime.length());
			strmonth = strfrtime.substring(0,ifirst);
			strday = strfrtime.substring(ifirst+1,ilast);
			
			strfrtime = stryear + "-"+strmonth+"-"+strday;
		}
		
		if(!strtotime.equals("latest")){
			int ifirst = strtotime.indexOf('/');
		    int ilast = strtotime.lastIndexOf('/');
			stryear = strtotime.substring(ilast+1,strtotime.length());
			strmonth = strtotime.substring(0,ifirst);
			strday =strtotime.substring(ifirst+1,ilast);
			
			strtotime = stryear + "-"+strmonth+"-"+strday;
		}
		
		
	    //opdb = new Operatedb(s,strdb);
		rs = opdb.GetNotes(strfocus,strfrtime,strtotime,strview,strword1,strword2,strrange,request.getParameter("match"));
// 		crs = opdb.GetStemmedNotes(strfrtime,strtotime,strview,strword1,strword2,strrange);

	}
	/*************************************************************************************************/
	 
%>

<body>
<div id="wrapperu" style="top:0px;">
<div id="wrapper">
	<form name="searchnote_form" action="../cn/SearchMore.jsp?database=<%=strdb%>&projectname=<%=request.getParameter("projectname")%>&threadfocus=<%=request.getParameter("threadfocus") %>" method="post">
	
		<div style="background:#1ABDE6">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			
			<label>查找短文： 从</label>&nbsp;&nbsp;
			<%if(request.getParameter("fromtime")!=null){%>
				<input type="text" style="background:white" id="ftimeid" name="fromtime" class="tcal" value="<%=request.getParameter("fromtime")%>" />&nbsp;&nbsp;&nbsp;
			<% }else{%>
				<input type="text" style="background:white" id="ftimeid" name="fromtime" class="tcal" value="earliest" />(开始)&nbsp;&nbsp;&nbsp;
			<%}%>
			<label>到</label>&nbsp;&nbsp;&nbsp;
			<%if(request.getParameter("totime")!=null){%>
			<input type="text" style="background:white" name="totime" class="tcal" value="<%=request.getParameter("totime") %>" />&nbsp;&nbsp;&nbsp;
			<% }else{%>
			<input type="text" style="background:white" name="totime" class="tcal" value="latest" />(现在)&nbsp;&nbsp;&nbsp;
			<%}%>
			<br/>
			
			 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<label>在视窗中</label>&nbsp;&nbsp;
			<select name="views" multiple="multiple" size="7" >
			<%if(request.getParameterValues("views")!=null){
			     String[] strtemp = request.getParameterValues("views");
			     for(int j=0; j<strtemp.length; j++){%>
			    	 <option selected="selected" value="<%=strtemp[j]%>"><%=strtemp[j]%></option>
			     <%}%>
			<% }else{%>
				<option selected="selected" value="所有视窗">所有视窗</option>
			<%}%>
			<%
				for(int i=0; i<vtitlelist.size();i++){
					strtitle = (String)vtitlelist.get(i);
			%>  <option value="<%=strtitle%>"><%=strtitle%></option>
			<%}%>	
				<option value="所有视窗">所有视窗</option>
							
			</select>&nbsp;&nbsp;&nbsp;	
			<label>(Shift+单击以选择多个)</label>
			<br/>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<label>关键词:</label>&nbsp;&nbsp;&nbsp;
			<%if(request.getParameter("keywords")!=null){%>
				<input class="fields" name = "keywords" id = "key" type = "text" size = "40" value = "<%=request.getParameter("keywords")%>"/>
			<%}
			  else{%>
			  	<input class="fields" name = "keywords" id = "key" type = "text" size = "40" value =""/>
			 <%}%>
			  &nbsp;&nbsp;
			 <label>in</label>&nbsp;&nbsp;
			 <select name="range" size="1" >
			<%if(request.getParameter("range")!=null){%>
				<option value="<%=request.getParameter("range")%>"><%=request.getParameter("range") %></option>
			<% }else{%>
				<option value="任何地方">任何地方</option>
			<%}%>
				<option value="任何地方">任何地方</option>
			    <option value="短文内容">短文内容</option>
			    <option value="短文标题">短文标题</option>		
			</select>
			
			<select name="match">
				<option value="exact" selected>精确匹配</option>
				<option value="related" <%=((request.getParameter("match")!=null && request.getParameter("match").contentEquals("related")) ? "selected":"")%>>相关匹配</option>
			</select>
			
			
			<input type="submit" style="height:40px" name="search" value="搜索更多短文" onClick="return CheckTime(); "/>
			<input type="button" style="height:40px" value="取消搜索" onclick="OpenThreadPage('<%=strfocus%>')"/>
			<br/>		  
		</div>
		<br/>
		<div id ="divNote" style="visibility:hidden;">
			<label>找到的短文数:</label>
	        <span id="updatecnt" >0</span>
	        &nbsp;&nbsp;
			<label>已选择的短文数:</label>
	        <span id="updatenoteselected" >0</span>
		</div>
		<br/>
		<Script type="text/javascript">
		   ShowDiv('<%= request.getParameter("search") %>');
		</Script>
		
		<div id = "selbutton" >
			<input type="button" name= "selectall"  onClick="SelectAll()"  id="selectall" value="全选">
			<input type="button" name= "deselectall"  onClick="DeselectAll()" id="deselectall" value="全部反选">
			<input type="button" name= "showthread" onClick="SendNoteid()" value="添加到思维脉络">	
		</div>
		
		<div style="overflow: auto; height: 300px; width:1000px;">
			<table style="width: 1000px;" cellpadding="5" cellspacing="10">
			<tr>
				<th></th>
				<th>标题</th>
				<th>视窗</th>
				<th>创建时间</th>
				<th>作者</th>
				<th>内容</th>
				</tr>	
		  <%int irow=0;
		  	opdb_author = new Operatedb(new sqls(),request.getParameter("database"));
		  	
		  	Stemmer stemmer = new Stemmer();		  	
			HashSet<String> strids=new HashSet<String>();
		  	Outer:
			while(rs.next()){
                            //detact unique id
                                String strid=rs.getString(4);
                                Boolean isDup=false;
                                
                                for(String id:strids){
                                    if(id.equals(strid)){                                                                                                                  
                                        isDup=true;
                                        break;
                                             }
                                }
                                if(isDup)
                                    continue;
				String ntcontent = stemmer.removeStopWordsAndStem(rs.getString("notecontent"));
				String nttitle = stemmer.removeStopWordsAndStem(rs.getString("notetitle"));
				String stemstrword1 = stemmer.removeStopWordsAndStem(strword1);
				String stemstrword2 = stemmer.removeStopWordsAndStem(strword2);
				
				if(request.getParameter("match").contentEquals("related")){
		    		if(strrange.equals("任何地方")){
								if(!(ntcontent.contains(stemstrword1) || nttitle.contains(stemstrword2)))
									continue Outer;
							}else if(strrange.equals("短文标题")){
								if(!(nttitle.contains(stemstrword2)))
									continue Outer;
							}else if(strrange.equals("短文内容")){
								if(!(ntcontent.contains(stemstrword1)))
									continue Outer;
							}
				}
				//System.out.println("irow = " +irow);
				irow++;	
                                
				if(!idlist.contains(rs.getString(4))){%>			
		       
				<tr>
					<td>
					<input type="checkbox" name="notecheck" onClick="UpdateNoteSelected(this);" value="<%=rs.getInt(4)%>"/>
					</td>
					<td>
                                            <div class="table_topic">
                                           <%=rs.getString(1)%> <!--display the note's title--><!--display the first 20 words-->
                                            </div>
					</td>
					
					<td>
					<%=rs.getString(2)%> <!--display the view's title-->
					</td>
					
					<td>
					<%=rs.getString(3)%><!--display the note's create time-->
					</td>
					
					<!-- display the note's authors -->
					<td>
					<%
					  List authorList = opdb_author.getMultipleAuthors(strid);
					  for(int i=0;i<authorList.size();i++){
						  String firstname = (String)authorList.get(i);
                                                  //adjust Chinese name
                                                  String fullName[]=firstname.split(" ");
                                                  firstname=fullName[1]+fullName[0];
          %>
						  <%=firstname %>
						  <%if(i < authorList.size()-1){
						  %>
						  ,&nbsp;
						  <%}%>	  
					  <%}%>
					</td>
							
					<td  style= "cursor:pointer">
                                            <div style="overflow:hidden;
                                                        width:450px;
                                                        white-space:nowrap;
                                                        text-overflow:ellipsis;"
                                            class="table_content" onclick="javascript:var a = document.searchnote_form.<%="pre"+strid%>.value;ShowContent(a);">
                                                <font color="#1ABDE6">
                                                    <U><%=rs.getString(5)%>...</U>
                                                </font><!--display the first 20 words-->
                                            </div>
					<input type="hidden" name="<%="pre"+rs.getString(4) %>" value="<%=rs.getString(4)%>"/> 
					</td>
				</tr>
			<% }                             
                        strids.add(strid);
}%>
		</table>	
		</div>	
       
		<script type="text/javascript">
		//No of rows found in search
		UpdateCount('<%= irow%>');
		ShowDivSelect('<%= irow%>');
		
// 		$("#selectall").click(function() {
// 			$(".notecheck").attr('checked', true);
// 		});
// 		$("#deselectall").click(function() {
// 			$(".notecheck").attr('checked', false);
// 		});
		</script>
	 </form>
	</div>
	</div>
</body>
</html>
<%
	//release the connection to the database
	if(rs != null){
		try{
			rs.close();
		}catch(SQLException e){		
			e.printStackTrace();
		}
	}
	if(stmt != null){
		try{
			stmt.close();
		}catch(SQLException e){
			e.printStackTrace();
		}
		
	}
	if(opdb != null){
		opdb.CloseCon();
	}
	
	if(opdb_author != null){
		opdb_author.CloseCon();
	}
	if(s != null){
		s.Close();
	}
	
%>