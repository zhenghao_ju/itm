<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.util.*,java.sql.*,com.knowledgeforum.k5.common.*,org.zoolib.*,org.zoolib.tuplebase.*,code.*,database.*"%>
<%@page import="itm.controllers.Stemmer"%>
<html>
<head>
<link rel=stylesheet href="../css/stickyhead.css" type="text/css"></link>
<link rel=stylesheet href="../css/thread.css" type="text/css"></link>
<%
request.setCharacterEncoding("UTF-8"); 
/************validate the user session*******************/
String username =" ";
String usertype =" ";
session = request.getSession(false);
if(session != null){
	if (session.getAttribute("username") == null){
		response.sendRedirect("/itm/index.jsp");
	}
	else
	{
	  username = (String)session.getAttribute("username");
	  usertype = (String)session.getAttribute("usertype");
	}
}

//System.out.println("+++++++++++++++++++++++++searchnote.jsp++++++++++++++++++++++++");
//System.out.println("searchnote.jsp the project name is "+request.getParameter("projectname"));
//System.out.println("searchnote.jsp the thread focus name is "+request.getParameter("threadfocus"));
System.out.println("searchnote.jsp the database name is "+request.getParameter("database"));
%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
	<!-- link calendar resources -->
	<link rel="stylesheet" type="text/css" href="../css/tcal.css" />
	<script type="text/javascript" src="../js/tcal.js"></script> 
	<script type="text/javascript" src="../js/cn/thread_update.js"></script>
	<script src="../js/jquery.js"></script>
	<script type ="text/javascript">
	<%
	String database=request.getParameter("database");
	String projectn=request.getParameter("projectname");
        String strfocus = request.getParameter("threadfocus");
	AccessToProjects atp = new AccessToProjects();
	System.out.println("projectname");
	boolean canEditThread = atp.canEdit(username,usertype,projectn,database);
	%>
	function UpdateNoteControl(canEditThread)
	{
		var selall = document.getElementById("select_all");
		var desall = document.getElementById("deselect_all");
		var athread = document.getElementById("add_to_thread");
		if(canEditThread == "false")
		{
			selall.removeAttribute("onclick");
			selall.setAttribute("id","select_all");
			selall.style.color = "gray";
			
			desall.removeAttribute("onclick");
			desall.setAttribute("id","select_all");
			desall.style.color = "gray";
			
			athread.removeAttribute("onclick");
			athread.setAttribute("id","select_all");
			athread.style.color = "gray";
		}
	}
	
	var count = 0;
	function UpdateCount(rowCnt)
	{
		count = rowCnt;
		//document.getElementById("updatecnt").value = count;
		//alert(count);
		document.getElementById("updatecnt").innerHTML = count;
	}
	
	var cnt = 0;
	function UpdateNoteSelected(rowObject)
	{
		//alert(rowObject);
		//alert(rowObject.checked);
		if(rowObject.checked == true)
		{
		  cnt++;
		}
		if(rowObject.checked == false)
		{
		  cnt--;
		}
		//document.getElementById("updatenoteselected").value = cnt;
		document.getElementById("updatenoteselected").innerHTML = cnt;
	}
	
	function SendNoteid(){
		
		// FIXME: Remove debug messages
		//alert("Calling SendNoteid in SearchNote.jsp");
	
	  	top.document.getElementById("framesetter").rows = "0%,100%";
		
		var idTimestampMapString = "";
		
		var TimestampForAddNotes = new String(AddTimeForNotesInThread());
		
		// FIXME: Remove debug messages
		//alert(TimestampForAddNotes);
		
		var ids="@";
		var putvalue;
		var proname;
		var num=0;
		proname= "<%=request.getParameter("projectname")%>";
		if(document.searchnote_form.notecheck.length==undefined){
			ids += document.searchnote_form.notecheck.value + "@";
			idTimestampMapString += document.searchnote_form.notecheck.value + "=" + TimestampForAddNotes + "%";
			num++;
		}else{
			for(var i=0;i<document.searchnote_form.notecheck.length;i++){
				if(document.searchnote_form.notecheck[i].checked){
					num++;
					ids += document.searchnote_form.notecheck[i].value + "@";
					idTimestampMapString += document.searchnote_form.notecheck[i].value + "=" + TimestampForAddNotes + ";";
				   	//alert(document.searchnote_form.notecheck[i].value);
				} 
			}
			
		}
		
		if(num==0){
			alert("Please select note first!");
			return;
		}
		if(num > 500){
			alert("you chose too many notes, please choose again!");
			return;
		}
        //alert("Map is " +  idTimestampMapString);
		
		//putvalue = ids;
		putvalue = ids + "&"+proname;
		
		//javascript form
		var f = document.createElement("form");
		f.setAttribute('method',"POST");
		f.setAttribute('action',"/itm/cn/thread.jsp");
		
		var i = document.createElement("input");
		i.setAttribute('type',"hidden");
		i.setAttribute('name',"database");
		i.setAttribute('value','<%= request.getParameter("database")%>');
		
		var s = document.createElement("input");
		s.setAttribute('type',"hidden");
		s.setAttribute('name',"projectname");
		s.setAttribute('value','<%= request.getParameter("projectname")%>');
		
		var t = document.createElement("input");
		t.setAttribute('type',"hidden");
		t.setAttribute('name',"threadfocus");
		t.setAttribute('value',"<%= request.getParameter("threadfocus")%>");
		
		var u = document.createElement("input");
		u.setAttribute('type',"hidden");
		u.setAttribute('name',"getvalue");
		u.setAttribute('value',putvalue);
		
		var v = document.createElement("input");
		v.setAttribute('type',"hidden");
		v.setAttribute('name',"nidts");
		
		// Replace whitespace with the html code or else the http will truncate the string
		idTimestampMapString = idTimestampMapString.replace(/ /g, "&#32;");
		v.setAttribute('value',idTimestampMapString);
		
		var w = document.createElement("input");
		w.setAttribute('type',"submit");
		w.setAttribute('value',"Submit");
		
		f.appendChild(i);
		f.appendChild(s);
		f.appendChild(t);
		f.appendChild(u);
		f.appendChild(v);
		f.appendChild(w);
		document.body.appendChild(f);
		
		f.submit();
	}
	
	function SelectAll(){
		cnt = 0;
		if(document.searchnote_form.notecheck.length == undefined){
			document.searchnote_form.notecheck.checked = true;
			cnt++;
		}else{
			for(var i=0;i<document.searchnote_form.notecheck.length;i++){
				document.searchnote_form.notecheck[i].checked = true;
				cnt++;
			}
		}
		document.getElementById("updatenoteselected").innerHTML = cnt;		
	}
	
	function DeselectAll(){
		cnt = 0;
		if(document.searchnote_form.notecheck.length == undefined){
			document.searchnote_form.notecheck.checked = false;
		}else{
			for(var i=0;i<document.searchnote_form.notecheck.length;i++){
				document.searchnote_form.notecheck[i].checked = false;
			}
		}
		document.getElementById("updatenoteselected").innerHTML = cnt;
	}
	
	function CheckTime(){
		if(document.searchnote_form.fromtime.value=="" && document.searchnote_form.totime.value!="")
			{	
				window.alert("You can't leave the 'From' input box empty!");
				return false;
			
			}
		if(document.searchnote_form.fromtime.value !="" && document.searchnote_form.totime.value=="")
		{	
			window.alert("You can't leave the 'To' input box empty!");
			return false;
		
		}
		
		if(document.searchnote_form.views.value=="")
		{
		  window.alert("please choose one view!");  
		  return false;  
		}
	}
	
	function CheckValidate(){
		if(document.searchnote_form.fromtime.value=="")
		{
		  window.alert("please input 'From Time'!");  
		  return false;  
		}
		if(document.searchnote_form.totime.value=="")
		{
		  window.alert("please input 'To Time'!");  
		  return false;  
		}
		if(document.searchnote_form.views.value=="")
		{
		  window.alert("please choose one view!");  
		  return false;  
		}
		
		return true;
	}
	
	function ShowContent(noteid){
		var body = document.body,
	    html = document.documentElement;

		var height = Math.max( body.scrollHeight, body.offsetHeight, 
	                       html.clientHeight, html.scrollHeight, html.offsetHeight );
// 		alert("Height = "+height);
		window.open('/itm/cn/New_NoteContent.jsp?database=<%=request.getParameter("database")%>&noteid='+noteid,"notewin","height='+height+', width=750,toolbar=no,scrollbars=yes,menubar=no");
	}
	
	/*function hideDiv()
	{
	   document.getElementById('divNote').style.visibility ='hidden';
	}*/
	/* Hide the number of notes selected and number of notes found until search notes is clicked*/
	function ShowDiv(searchVal)
	{
	  var s = searchVal;
	  var v = document.getElementById("divNote");
	  if(s == "null")
	  {
	     v.style.visibility='hidden';
	  }
	  else
	  {
	    v.style.visibility='visible';
	  }
	}
	/* Enable Disable select buttons until notes are listed */
	function ShowDivSelect(numNotes)
	{
	  var sButton = document.getElementById("selbutton").children;
	  //alert(numNotes);
	  //alert(document.getElementById("selbutton").children.length);
	  
	  if(numNotes < 1)
	  {
	    for(var i = 0; i<sButton.length;i++)
	    {
	      sButton[i].removeAttribute("href");
	      sButton[i].removeAttribute("onclick");
	      sButton[i].style.color ="gray";
	    }
	  }
	}
	</script>
</head>

<%
	/*********************************read all views title from database******************************/
	List vtitlelist = new ArrayList();
	String strtitle;
	ResultSet rs = null;
	Statement stmt;
	Operatedb opdb = null;
	Operatedb opdb_author = null;
	String stryear ;
	String strmonth ;
	String strday ;
	String strrange = "";
	String strfrtime=null;
	String strtotime=null;
	String[] strview;
	String strword1="", strword2="",strword="";
	sqls s=new sqls();
	stmt= s.Connect(request.getParameter("database"));
	
	try{
			rs = stmt.executeQuery("SELECT * from `view_table`;");
			while(rs.next()){
				strtitle = rs.getString(2);	
				vtitlelist.add(strtitle);			
			}
		}catch(SQLException e){
			e.printStackTrace();		
		}
	/*************************************************************************************************/
	/*************************Get all notes contained in view chosen from database*******************/
	if(request.getParameter("search") != null){ //user clicked on the search buttom
		//System.out.println("user click on the search button");
		strfrtime = request.getParameter("fromtime");
		strtotime = request.getParameter("totime");
		strview = request.getParameterValues("views");
		strword = request.getParameter("keywords");
		strrange = "";
		//System.out.println("range is "+strrange);
		//System.out.println("keywords is "+strword);
		
		if(request.getParameter("range")!= null)
		{
			strrange = request.getParameter("range");
			if(strrange.equals("短文标题")){
				strword2 = strword;
			}else if(strrange.equals("短文内容")){
				strword1 = strword;
			}else if(strrange.equals("任何地方")){
				strword1 = strword;
				strword2 = strword;
			}
		}
		
		
		//change the date format if the time period is not empty
		/*
		if( !strfrtime.equals("earliest") && !strtotime.equals("latest")){
			int ifirst = strfrtime.indexOf('/');
			int ilast = strfrtime.lastIndexOf('/');
			stryear = strfrtime.substring(ilast+1,strfrtime.length());
			strmonth = strfrtime.substring(0,ifirst);
			strday = strfrtime.substring(ifirst+1,ilast);
			
			strfrtime = stryear + "-"+strmonth+"-"+strday;
			
		    ifirst = strtotime.indexOf('/');
		    ilast = strtotime.lastIndexOf('/');
			stryear = strtotime.substring(ilast+1,strtotime.length());
			strmonth = strtotime.substring(0,ifirst);
			strday =strtotime.substring(ifirst+1,ilast);
			
			strtotime = stryear + "-"+strmonth+"-"+strday;		
		}*/
		
		if(!strfrtime.equals("earliest")){
			int ifirst = strfrtime.indexOf('/');
			int ilast = strfrtime.lastIndexOf('/');
			stryear = strfrtime.substring(ilast+1,strfrtime.length());
			strmonth = strfrtime.substring(0,ifirst);
			strday = strfrtime.substring(ifirst+1,ilast);
			
			strfrtime = stryear + "-"+strmonth+"-"+strday;
		}
		
		if(!strtotime.equals("latest")){
			int ifirst = strtotime.indexOf('/');
		    int ilast = strtotime.lastIndexOf('/');
			stryear = strtotime.substring(ilast+1,strtotime.length());
			strmonth = strtotime.substring(0,ifirst);
			strday =strtotime.substring(ifirst+1,ilast);
			
			strtotime = stryear + "-"+strmonth+"-"+strday;
		}
		
		
		opdb = new Operatedb(new sqls(),request.getParameter("database"));
		rs = opdb.GetNotes(strfocus,strfrtime,strtotime,strview,strword1,strword2,strrange,request.getParameter("match"));
	}
	/*************************************************************************************************/
%>

<body onload="UpdateNoteControl('<%= canEditThread%>');">
<div id="wrapperu" style="top:0px;">
<div id="wrapper">
	<form name="searchnote_form" action="../cn/SearchNoteServlet" method="post">
	
		<div style="background:#1ABDE6">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			
			<label>查找短文：从</label>&nbsp;&nbsp;
			<%if(request.getParameter("fromtime")!=null){%>
				<input type="text" style="background:white" id="ftimeid" name="fromtime" class="tcal" value="<%=request.getParameter("fromtime")%>" />&nbsp;&nbsp;&nbsp;
			<% }else{%>
				<input type="text" style="background:white" id="ftimeid" name="fromtime" class="tcal" value="earliest" />(开始)&nbsp;&nbsp;&nbsp;
			<%}%>
			<label>到</label>&nbsp;&nbsp;&nbsp;
			<%if(request.getParameter("totime")!=null){%>
			<input type="text" style="background:white" name="totime" class="tcal" value="<%=request.getParameter("totime") %>" />&nbsp;&nbsp;&nbsp;
			<% }else{%>
			<input type="text" style="background:white" name="totime" class="tcal" value="latest" />(现在)&nbsp;&nbsp;&nbsp;
			<%}%>
			<br/>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<label>选择视窗</label>&nbsp;&nbsp;
			<select name="views" multiple="multiple" size="7" >
			<%if(request.getParameterValues("views")!=null){
			     String[] strtemp = request.getParameterValues("views");
			     for(int j=0; j<strtemp.length; j++){%>
			    	 <option selected="selected" value="<%=strtemp[j]%>"><%=strtemp[j]%></option>
			     <%}%>
			<% }else{%>
				<option selected="selected" value="所有视窗">所有视窗</option>
			<%}%>
			<%
				for(int i=0; i<vtitlelist.size();i++){
					strtitle = (String)vtitlelist.get(i);
			%>  <option value="<%=strtitle%>"><%=strtitle%></option>
			<%}%>	
				<option value="所有视窗">所有视窗</option>
							
			</select>&nbsp;&nbsp;&nbsp;	
			<label>(Shift + 单击以选择多个)</label>
			<br/>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<label>关键词：:</label>&nbsp;&nbsp;&nbsp;
			<%if(request.getParameter("keywords")!=null){%>
				<input class="fields" name = "keywords" id = "key" type = "text" size = "40" value = "<%=request.getParameter("keywords")%>"/>
			<%}
			  else{%>
			  	<input class="fields" name = "keywords" id = "key" type = "text" size = "40" value =""/>
			 <%}%>
			 &nbsp;&nbsp;&nbsp;
			 <label>in</label>&nbsp;&nbsp;
			 <select name="range" size="1" >
			<%if(request.getParameter("range")!=null){%>
				<option value="<%=request.getParameter("range")%>"><%=request.getParameter("range") %></option>
			<% }else{%>
				<option value="任何地方">任何地方</option>
			<%}%>
				<option value="短文标题">短文标题</option>
				<option value="短文内容">短文内容</option>
				<option value="任何地方">任何地方</option>
			    
			    		
			</select>
			&nbsp;
			<select name="match">
				<option value="exact" selected>精确匹配</option>
				<option value="related" <%=((request.getParameter("match")!=null && request.getParameter("match").contentEquals("related")) ? "selected":"")%>>一般匹配</option>
			</select>
<!-- 			&nbsp;&nbsp;&nbsp; -->
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="submit" style="height:40px;width:100px" name="search" value="搜索" onClick="return CheckTime();"/>&nbsp;&nbsp;&nbsp;&nbsp;
			<br/>		  
		</div>
		<br/>
		<div id ="divNote" style="visibility:hidden;">
			<label>搜索到的短文数：</label>
	        <span id="updatecnt" >0</span>
	         &nbsp;&nbsp;
			<label>已选择的短文数：</label>
	        <span id="updatenoteselected">0</span>
        </div>
		<br/>
		 
		<Script type="text/javascript">
		   ShowDiv('<%= request.getParameter("search") %>');
		</Script>
		
		<div id = "selbutton" >
			<input type="button" name= "selectall" id="select_all" onClick="SelectAll()"  value="全选">
	        <input type="button" name= "selectall" id="deselect_all" onClick="DeselectAll()"  value="全部反选">
	        <input type="button" name= "showthread" id="add_to_thread" onClick="return SendNoteid()" value="添加到思维脉络">
        </div>
		 <div style="overflow: auto; height: 300px; width:1000px; text-overflow:ellipsis;">     
		     <table style="width: 1000px;" cellpadding="5" cellspacing="10">
				<tr>
				<th></th>
				<th>标题</th>
				<th>视窗</th>
				<th>创建时间</th>
				<th>作者</th>
				<th>内容</th>
				</tr>

		  <%int irow=0;
		  	opdb_author = new Operatedb(new sqls(),request.getParameter("database"));
		  	
		  	Stemmer stemmer = new Stemmer();		  	
			HashSet<String> strids=new HashSet<String>();
		  	Outer:
			while(rs.next()){
                            //detact unique id
                                String strid=rs.getString(4);
                                Boolean isDup=false;
                                
                                for(String id:strids){
                                    if(id.equals(strid)){                                                                                                                  
                                        isDup=true;
                                        break;
                                             }
                                }
                                if(isDup)
                                    continue;
				String ntcontent = stemmer.removeStopWordsAndStem(rs.getString("notecontent"));
				String nttitle = stemmer.removeStopWordsAndStem(rs.getString("notetitle"));
				String stemstrword1 = stemmer.removeStopWordsAndStem(strword1);
				String stemstrword2 = stemmer.removeStopWordsAndStem(strword2);
				
				if(request.getParameter("match").contentEquals("related")){
		    		if(strrange.equals("任何地方")){
								if(!(ntcontent.contains(stemstrword1) || nttitle.contains(stemstrword2)))
									continue Outer;
							}else if(strrange.equals("短文标题")){
								if(!(nttitle.contains(stemstrword2)))
									continue Outer;
							}else if(strrange.equals("短文内容")){
								if(!(ntcontent.contains(stemstrword1)))
									continue Outer;
							}
				}
				//System.out.println("irow = " +irow);
				irow++;	
				%>
				
		       
				<tr>
					<td>
					<input type="checkbox" name="notecheck" onClick="UpdateNoteSelected(this);" value="<%=rs.getInt(4)%>"/>
					</td>
					<td>
                                            <div class="table_topic">
                                           <%=rs.getString(1)%> <!--display the note's title--><!--display the first 20 words-->
                                            </div>
					</td>
					
					<td>
					<%=rs.getString(2)%> <!--display the view's title-->
					</td>
					
					<td>
					<%=rs.getString(3)%><!--display the note's create time-->
					</td>
					
					<!-- display the note's authors -->
					<td>
					<%
					  List authorList = opdb_author.getMultipleAuthors(strid);
					  for(int i=0;i<authorList.size();i++){
						  String firstname = (String)authorList.get(i);
                                                  //adjust Chinese name
                                                  String fullName[]=firstname.split(" ");
                                                  firstname=fullName[1]+fullName[0];
          %>
						  <%=firstname %>
						  <%if(i < authorList.size()-1){
						  %>
						  ,&nbsp;
						  <%}%>	  
					  <%}%>
					</td>
							
					<td  style= "cursor:pointer">
                                            <div style="overflow:hidden;
                                                        width:450px;
                                                        white-space:nowrap;
                                                        text-overflow:ellipsis;"
                                            class="table_content" onclick="javascript:var a = document.searchnote_form.<%="pre"+strid%>.value;ShowContent(a);">
                                                <font color="#1ABDE6">
                                                    <U><%=rs.getString(5)%>...</U>
                                                </font><!--display the first 20 words-->
                                            </div>
					<input type="hidden" name="<%="pre"+rs.getString(4) %>" value="<%=rs.getString(4)%>"/> 
					</td>
				</tr>
			<% 
                        strids.add(strid);
}%>
		</table>
		<script type="text/javascript">
		 UpdateCount('<%= irow %>');
		 ShowDivSelect('<%= irow%>');
	    </script>
		</div>								
	</form>
	
	</div>
	</div>
</body>
</html>
<%
	//release the connection to the database
	if(rs != null){
		try{
			rs.close();
		}catch(SQLException e){		
			e.printStackTrace();
		}
	}
	
	if(opdb != null){
		opdb.CloseCon();
	}
	
	if(opdb_author != null){
		opdb_author.CloseCon();
	}
	
	if(s != null){
		s.Close();
	}
	
%>