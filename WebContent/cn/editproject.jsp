<%@page import="itm.models.ProjectModel"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="java.util.*,java.sql.*,code.*,database.*;"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<% 
request.setCharacterEncoding("UTF-8");
response.setCharacterEncoding("utf-8");
/************validate the user session*******************/
	session = request.getSession(false);
	if(session != null){
		if (session.getAttribute("username") == null){
			response.sendRedirect("/itm/index.jsp");
		}
	}
%>
<script type="text/javascript"  charset="UTF-8">
<%


String strsucceed = request.getParameter("ifsucceed");
String database = request.getParameter("database");
System.out.println("Requested vals = suc "+strsucceed+ " db "+database + " projectname "+request.getParameter("projectname"));
if(strsucceed != null){
	if(strsucceed.equals("yes")){
		String strname = request.getParameter("projectname");
	%>
		alert("This project has been saved!");
		window.opener.location='/itm/cn/New_HomePage.jsp?projectname=<%=strname%>&database=<%=database%>';
		window.close();
		
	<%}else if(strsucceed.equals("no")){%>
		alert("The project name already exists!");
	<%}
}%>


	function OnSave(){
		if(document.Create_form.projectname.value=="")
			{
			  window.alert("please input the project name!");  
			  return false;  
			}
		if(document.Create_form.teacher.value=="")
		{
		  window.alert("please input the teacher's name!");  
		  return false;  
		}
		if(document.Create_form.school.value=="")
		{
		  window.alert("please input the school name!");  
		  return false;  
		}
		if(document.Create_form.grade.value=="")
		{
		  window.alert("please choose grade!");  
		  return false;  
		}
		
		var from = parseInt(document.Create_form.fromyear.value);
		var to = parseInt(document.Create_form.toyear.value);
		if(from > to){
			alert("the from-school-year is greater than the to-school-year, please select them again!");
			return false;
		}
		/*if(document.Create_form.group.value=="")
		{
		  window.alert("please choose group!");  
		  return false;  
		}*/	
		return true;
	}
</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Edit Project</title>

<LINK REL=StyleSheet HREF="../css/stickyhead.css" TYPE="text/css"></LINK>
<LINK REL=StyleSheet HREF="../css/createProject.css" TYPE="text/css"></LINK>
</head>
<% 
	ProjectModel p = new ProjectModel(database);
	// Get Project information
	String project = request.getParameter("projectname");

	ResultSet p_data = p.getProject(database, project);
	p_data.next();
	//get group from database
	sqls s = new sqls();
	Operatedb opdb = new Operatedb(s,database);
	ResultSet rs = null;
	rs = opdb.GetRecordsFromDB("group_table","title","");
	
	//get the grade list
	List gradelist = new ArrayList();
	//int i=0;
	/*for(GradeSelect gradesopt:GradeSelect.values()){
		gradelist.add(gradesopt);
		//System.out.println("The "+ i + "th value of the Grade is :" + (GradeSelect)gradelist.get(i));
		//i++;
	}*/
	gradelist.add("K");
	for(int i=1;i<=12;i++){
		gradelist.add(i);
		//System.out.println(gradelist.get(i-1));
	}

%>

<%
	//get the current year
	int curyear = Calendar.getInstance().get(Calendar.YEAR);
%>

<body>
<div id="sticky">
	<div id="header">
	<table border="0">
		<tr>
			<td><img src="../img/itmlogo100x100.gif" alt="ITM LOGO" height="100" width="100" /></td>
			<td style="background-color: #01B0F1;color:yellow;font-family:Arial, Helvetica, sans-serif;font-size:30px;" height="60" width="850">
			&nbsp;&nbsp;探究课题基本信息
			</td>
		</tr>
	</table>
			
</div>
</div>
<div id="wrapperu">
<div id="wrapper">
	<form name="Create_form" action="../editproject" method="post">
	<table>
		<tr>
			<td><b>研究主题：</b></td>
			<input name="database" type="hidden" value="<%=database%>"/>
			<input name="preproject" type="hidden" value="<%=project%>"/>
			<td><input class="fields" name="projectname" id="user" type = "text" size = "18" value = "<%=project%>" /></td>
			<td><b>*年级:</b></td>
			<th rowspan="2"><select name = "grade" multiple="multiple" size="4">
<!--             	<option value="K">K</option> -->
            	<% int j=0;
            	ResultSet rsGrade = p.getGrade(database,p_data.getString("idProject"));
            	for(j=0;j<gradelist.size(); j++){%>
            		<option 
            		<%
            		rsGrade.beforeFirst();
            		while(rsGrade.next()){
			           if(gradelist.get(j).toString().contentEquals(rsGrade.getString("grade")))
			        	   out.println("selected='selected'");
            		}
		           	%> 
            		value="<%=gradelist.get(j)%>">
            			<%=gradelist.get(j)%>
            		</option>
            	<% 
            	}
            	rsGrade.close();
            	
            	p_data = p.getProject(database, project);
            	p_data.next();
            	
            	System.out.println("ProjectID after the loop "+p_data.getString("idProject"));
            	%>	              
            </select></th>
		</tr>
		<tr>
			<td><b>教师:</b></td>
			<td><input class="fields" name = "teacher" id = "address" type = "text" size = "15" value = "<%=p_data.getString("teacher")%>"/></td>
			<td></td>
		</tr>
		<tr>
			<td><b>学校:</b></td>
			<td><input class="fields" name = "school" id = "year" type = "text" size = "30" value = "<%=p_data.getString("school")%>"/></td>
			<td><b>*选择知识论坛组:</b></td>
			<td colspan="2">
				<select name="group" multiple="multiple" size="4">
				    <% 
				    ResultSet rsGroup = p.getGroup(database,p_data.getString("idProject"));
				    
				    while(rs.next()){%>
				           <option
				           <%
				           rsGroup.beforeFirst();
				           while(rsGroup.next()){
					           if(rs.getString(1).contentEquals(rsGroup.getString("title")))
					        	   out.println("selected='selected'");
				           }
				           %> 
				           value="<%=rs.getString(1)%>">
				           <%=rs.getString(1)%>
				           </option>
				    <%}%>      
        		</select>
        	</td>
		</tr>
		<tr>
			<td><b>学年:</b>
			<td colspan="3" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;<b>从</b><select name="fromyear" style="float:none;">
				<option value="<%=curyear%>"><%=curyear%></option>
				<%for(int i=curyear-5; i<curyear+5;i++){%>			
					<option value="<%=i%>"><%=i%></option>
				<%}%>
			</select><b>到</b><select name="toyear" style="float:none;">
				<option value="<%=curyear%>"><%=curyear%></option>
				<%for(int i=curyear-5; i<curyear+5;i++){%>
					<option value="<%=i%>"><%=i%></option>
				<%}%>
			</select></td>
		</tr>
		<tr>
			<td></td>
			<td colspan="3">(*)ctrl+单击以选择多项</td>
		</tr>
	</table>
	<br>
	<br>
	<center>&nbsp;&nbsp;&nbsp;<input name="save" type="submit" id="create_save" value= "保存" onclick="return OnSave();"/>&nbsp;&nbsp;&nbsp;<input name="save" type="button" id="create_save" value= "取消" onclick="javascript: window.close();"/>&nbsp;&nbsp;&nbsp;</center>
     </form>
</div>
</div>
</body>
</html>
<%
	//release the connection to database
	p_data.close();

	if(rs != null){
		   try{
			   rs.close();
		   }catch (SQLException e){
			   e.printStackTrace();
		   }
	}
	if(opdb != null){
		   try{
			   opdb.CloseCon();
		   }catch (Exception e){
			   e.printStackTrace();
		   }
	}
	//s.Close();
%>