<%@page import="itm.models.ProjectModel"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="java.util.*,java.sql.*,code.*,database.*;"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<% 
/************validate the user session*******************/
String username =" ";
String usertype =" ";
	session = request.getSession(false);
	if(session != null){
		if (session.getAttribute("username") == null){
			response.sendRedirect("/itm/index.jsp");
		}
		username = (String)session.getAttribute("username");
		usertype = (String)session.getAttribute("usertype");
	}
%>

<%
/*******************Fetch permissions for the existing usertype***************/
AccessToProjects atp = new AccessToProjects();

int ProjectInfoPermission = atp.fetchPermission("ProjectInfo",usertype);
System.out.println("ProjectInfoPermission" + ProjectInfoPermission);
String projectn = request.getParameter("projectname"); 
//String projectn = new String(request.getParameter("projectname").getBytes("ISO8859-1"),"UTF-8"); 
String db=request.getParameter("database");
boolean canEditProjectInfo = atp.canEdit(username, usertype, projectn, db);
ProjectModel pmodel = new ProjectModel(db);
boolean IsProjectDeleted = pmodel.IsDeleted(projectn);
%>

<script type="text/javascript">
/*set permissions for various usertype*/
function updateControls(canEditProjectInfo)
{
	var sel = document.getElementById("edit_project");
	var del = document.getElementById("delete_restore_project");
	//if ((parseInt(ProjectInfoPermission, 10) & 2) != 2)	
	if(canEditProjectInfo == "false")
	{
		console.log("[DEBUG] User does not has edit permissions");
		
		// Disable edit and delete functionality
		sel.removeAttribute("href");
		del.removeAttribute("href");
		sel.removeAttribute("onclick");
		del.removeAttribute("onclick");
		sel.setAttribute("class", "disabled");
		del.setAttribute("class","disabled");
	}  
}
</script>
<script src="../js/jquery.js"></script>
<script type="text/javascript">
<%
String strsucceed = request.getParameter("ifsucceed");
String database = request.getParameter("database");
System.out.println("Requested vals = suc "+strsucceed+ " db "+database + " projectname "+request.getParameter("projectname"));
if(strsucceed != null){
	if(strsucceed.equals("yes")){
		String strname = request.getParameter("projectname");
	%>
		alert("This project has been saved!");
		window.opener.location='/itm/New_HomePage.jsp?projectname=<%=strname%>&database=<%=database%>';
		window.close();
		
	<%}else if(strsucceed.equals("no")){%>
		alert("The project name already exists!");
	<%}
}%>


	function OnSave(){
		if(document.Create_form.projectname.value=="")
			{
			  window.alert("please input the project name!");  
			  return false;  
			}
		if(document.Create_form.teacher.value=="")
		{
		  window.alert("please input the teacher's name!");  
		  return false;  
		}
		if(document.Create_form.school.value=="")
		{
		  window.alert("please input the school name!");  
		  return false;  
		}
		if(document.Create_form.grade.value=="")
		{
		  window.alert("please choose grade!");  
		  return false;  
		}
		
		var from = parseInt(document.Create_form.fromyear.value);
		var to = parseInt(document.Create_form.toyear.value);
		if(from > to){
			alert("the from-school-year is greater than the to-school-year, please select them again!");
			return false;
		}
		/*if(document.Create_form.group.value=="")
		{
		  window.alert("please choose group!");  
		  return false;  
		}*/	
		return true;
	}	

	function print(object) {
	  var output = '';
	  for (property in object) {
	    output += property + ': ' + object[property]+'; ';
	  }
	  console.log(output);
	}
	
	function UpdateDeleteRestoreStatus(IsProjectDeleted)
	{
	  //alert("Update delete restore called");
	  //alert(IsProjectDeleted);
	  var elem = document.getElementById("delete_restore_project");
	  //var isDeleted = IsProjectDeleted;
	  if(IsProjectDeleted == "true")
	  {
	    elem.value = '还原';
	  }
	  else
      {
	    elem.value = '删除';
	  }
	}
	
	function DeleteRestoreProject(Dbname,projectnm)
	{
	  var dbase = Dbname;
	  var pname = projectnm;
	  var elem = document.getElementById("delete_restore_project");
	  //alert(elem.value);
	  
	  var confirmation = confirm("你确定"+ elem.value +"该探究课题吗?");
	  if(confirmation)
	  {
	       if(elem.value == "删除")
	       {
		    $.ajax({
		      type: "POST",
		      async: false,
		      url: "/itm/DeleteProject",
		      data:{db:dbase, projectname:pname}
		      }).done(function( msg ) {
		     // alert( "Data Saved: " + msg );
		      }).fail(function(msg){ alert('failed');});
		      
		    //  elem.value ="Restore";
		      
	     }
	     else
	     {
	       $.ajax({
			      type: "POST",
			      async: false,
			      url: "/itm/RecoverProject",
			      data:{db:dbase, projectname:pname}
			      }).done(function( msg ) {
			     // alert( "Data Saved: " + msg );
			      }).fail(function(msg){ alert('failed');});
	     }
	       window.opener.location = "../cn/New_HomePage.jsp?database=<%=database %>";
	       window.close();
	   }   
	  else
      {
	    //do nothing. Remain on the same location.
      }
	}
</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Project Information</title>

<LINK REL=StyleSheet HREF="../css/stickyhead.css" TYPE="text/css"></LINK>
<LINK REL=StyleSheet HREF="../css/createProject.css" TYPE="text/css"></LINK>
</head>
<% 
	ProjectModel p = new ProjectModel(database);

	// Get Project information;
	String project = projectn;

	ResultSet p_data = p.getProject(database, projectn);
	p_data.next();
	
	// Get group from database
	sqls s = new sqls();
	Operatedb opdb = new Operatedb(s,database);
	
	ResultSet rs = null;
	rs = opdb.GetRecordsFromDB("group_table","title","");
	
	// Get the grade list
	List gradelist = new ArrayList();
	
	//int i=0;
	/*for(GradeSelect gradesopt:GradeSelect.values()){
		gradelist.add(gradesopt);
		//System.out.println("The "+ i + "th value of the Grade is :" + (GradeSelect)gradelist.get(i));
		//i++;
	}*/
	
	gradelist.add("K");
	for(int i=1;i<=12;i++)
	{
		gradelist.add(i);
		//System.out.println(gradelist.get(i-1));
	}
	gradelist.add("UG");
	gradelist.add("PG");
	gradelist.add("Other");
%>

<%
	// get the current year
	int curyear = Calendar.getInstance().get(Calendar.YEAR); 

	int toyear = p_data.getInt("toyear");
	int fromyear = p_data.getInt("fromyear");
%>

<body onload="updateControls('<%= canEditProjectInfo %>');UpdateDeleteRestoreStatus('<%= IsProjectDeleted %>')" >
<div id="sticky">
	<div id="header">
	<table border="0">
		<tr>
			<td><img src="../img/itmlogo100x100.gif" alt="ITM LOGO" height="100" width="100" /></td>
			<td style="background-color: #01B0F1;color:yellow;font-family:Arial, Helvetica, sans-serif;font-size:30px;" height="60" width="850">
			&nbsp;&nbsp;探究课题简介
			</td>
		</tr>
	</table>
			
</div>
</div>
<div id="wrapperu">
<div id="wrapper">
	<form name="Create_form" action="../editproject" method="post">
	<table>
		<tr>
			<td><b>探究课题名：</b></td>
			<input name="database" type="hidden" value="<%=database%>"/>
			<input name="preproject" type="hidden" value="<%=project%>"/>
			<td style="text-align:left"><%=project%></td>
			<td><b>*年级:</b></td>
			<th style="text-align:left">
<!-- 			<select name = "grade" multiple="multiple" size="4" > -->
<!--             	<option value="K">K</option> -->
<!-- 				<ul> -->
            	<% int j=0;
            	ResultSet rsGrade = p.getGrade(database,p_data.getString("idProject"));
            	
            	while(rsGrade.next()){
            		if(rsGrade.isLast())
            			out.println(rsGrade.getString("grade"));
            		else
            			out.println(rsGrade.getString("grade")+", ");
            	}
//             	
            	rsGrade.close();
            	
            	p_data = p.getProject(database, project);
            	p_data.next();
            	
            	System.out.println("ProjectID after the loop "+p_data.getString("idProject"));
            	%>	         
<!--             	</ul>      -->
<!--             </select> -->
            </th>
		</tr>
		<tr>
			<td><b>老师：</b></td>
			<td style="text-align:left"><%=p_data.getString("teacher")%></td>
			<td></td>
		</tr>
		<tr>
			<td><b>学校：</b></td>
			<td style="text-align:left"><%=p_data.getString("school")%></td>
			<td><b>知识论坛组:</b></td>
			<td style="text-align:left">
			<ul style="list-style-type: none;">
				    <% 
				    ResultSet rsGroup = p.getGroup(database,p_data.getString("idProject"));
				    
				    while(rsGroup.next()){
				        	   out.println("<li>"+rsGroup.getString("title")+"</li> ");
			           }
				    %>      
				    </ul>
        	</td>
		</tr>
		<tr>
			<td><b>学年:</b>
			<td colspan="3" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;<b>从</b>
			<%=fromyear%>
				<b>至</b>
			<%=toyear%>
			</td>
		</tr>
		<tr>
			<td></td>
			<td colspan="3"></td>
		</tr>
	</table>
	<br>
	<br>
	<center>&nbsp;&nbsp;&nbsp;<INPUT TYPE="BUTTON" VALUE="编辑" id="edit_project" ONCLICK="window.location.href='editproject.jsp?database=<%=database%>&projectname=<%=project%>'">&nbsp;&nbsp;&nbsp;
	<INPUT TYPE="BUTTON" VALUE="删除" id="delete_restore_project" ONCLICK="DeleteRestoreProject('<%= database %>','<%= project %>');return false;">&nbsp;&nbsp;&nbsp;
	<input name="save" type="button" id="create_save" value= "关闭" onclick="javascript: window.close();"/>&nbsp;&nbsp;&nbsp;</center>
	 
     </form>
</div>
</div>
</body>
</html>
<%
	//release the connection to database
	p_data.close();

	if(rs != null){
		   try{
			   rs.close();
		   }catch (SQLException e){
			   e.printStackTrace();
		   }
	}
	if(opdb != null){
		   try{
			   opdb.CloseCon();
		   }catch (Exception e){
			   e.printStackTrace();
		   }
	}
	//s.Close();
%>