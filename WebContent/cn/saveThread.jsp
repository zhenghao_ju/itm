<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.*,java.sql.*,com.knowledgeforum.k5.common.*,org.zoolib.*,org.zoolib.tuplebase.*,code.*,database.*;"%>
<%
 request.setCharacterEncoding("UTF-8");
String[] strvalue = request.getParameter("getvalue").split("@");
String project = request.getParameter("projectname");
String focus = request.getParameter("threadfocus");
String nidts = request.getParameter("nidts");

Hashtable<String,String> nid_to_timestamp = new Hashtable<String, String>();

try
{
  if(nidts != null && !nidts.isEmpty() && (nidts.compareTo("null") != 0))
  {
	  nidts = nidts.replaceAll("&#32;", " ");
	  String[] split_nidts = nidts.split(";");
	  System.out.println("[INFO : saveThread.jsp] nidts is " + nidts);
	  
	  for(int i = 0; i < split_nidts.length; ++i)
	  {
		System.out.println("[INFO : saveThread.jsp] nidts split is "+ split_nidts[i]);
		String[] key_value = split_nidts[i].split("=");
		
		System.out.println("key_value is " + key_value[0] + " " + key_value[1]);
		nid_to_timestamp.put(key_value[0], key_value[1]);	 
	  }
  }
}
catch(NullPointerException e)
{
  System.out.println("[ERROR] Parsing note ids with timestamp information.");
  e.printStackTrace();
}

sqls s = new sqls();
Operatedb opdb = new Operatedb(s,request.getParameter("database"));
ResultSet rs = opdb.getC().executeQuery("select idProject from project where projectname='"+project+"';");
rs.next();
int pid = rs.getInt(1);
opdb.getC().executeUpdate("delete from thread_note using thread_note inner join project on project.idProject = thread_note.projectid and thread_note.threadfocus='"+focus+"';");
opdb.getC().executeUpdate("delete from project_thread where project_thread.threadfocus = '"+focus+"';");
System.out.println("insert into project_thread (projectid, threadfocus) values("+pid+",'"+focus+"');");
opdb.getC().executeUpdate("insert into project_thread (projectid, threadfocus) values("+pid+",'"+focus+"');");

for (int i = 1; i < strvalue.length;i++) 
{
	if (nid_to_timestamp.size() < 0)
	{
		System.out.println("[ERROR : saveThread.jsp] Note id to timestamp information is empty");	
	}
	
	char mk = strvalue[i].charAt(strvalue[i].length() - 1);
	String tmpmk = strvalue[i].substring(0, strvalue[i].length() - 1);
	if(nid_to_timestamp.get(tmpmk).equals("null"))
	{
	  System.out.println("adding epoch time to the notes that were added before the add time feature existed");
	  System.out.println("[INFO : saveThread.jsp] insert into thread_note (projectid,threadfocus,noteid,highlight,addtime) values ("+pid+",'"+focus+"',"+tmpmk+","+ mk +","+ "1970-01-01 00:00:00" +");");
	  opdb.getC().executeUpdate("insert into thread_note (projectid,threadfocus,noteid,highlight,addtime) values ("+pid+",'"+focus+"',"+tmpmk+","+ mk +"," +"'" + "1970-01-01 00:00:00" + "'"+");");

	}
	else
	{
	  System.out.println("[INFO : saveThread.jsp] insert into thread_note (projectid,threadfocus,noteid,highlight,addtime) values ("+pid+",'"+focus+"',"+tmpmk+","+ mk +","+ nid_to_timestamp.get(tmpmk) +");");
	  opdb.getC().executeUpdate("insert into thread_note (projectid,threadfocus,noteid,highlight,addtime) values ("+pid+",'"+focus+"',"+tmpmk+","+ mk +","+ "'" + nid_to_timestamp.get(tmpmk)+"'"+");");

	}	
}

if(rs != null) {
	try{
		rs.close();
	}catch(SQLException e){
		e.printStackTrace();
	}
}

if(opdb != null){
	opdb.CloseCon();
}

if(s != null){
	//s.Close();
}
%>