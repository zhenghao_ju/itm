<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="java.util.*,java.sql.*,com.knowledgeforum.k5.common.*,org.zoolib.*,org.zoolib.tuplebase.*,code.*,database.*"%>
    
<%@ page import="itm.models.NoteModel" %>
<%@ page import="itm.controllers.NoteController" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
request.setCharacterEncoding("UTF-8"); 
/************validate the user session*******************/
String username =" ";
String usertype =" ";
	session = request.getSession(false);
        
	if(session != null){
		if (session.getAttribute("username") == null){
			response.sendRedirect("/itm/index.jsp");
		}
		else
		{
		  username = (String)session.getAttribute("username");
		  usertype = (String)session.getAttribute("usertype");
		}
	}
String projectn=session.getAttribute("project").toString();
String threadName;
	if (request.getParameter("threadfocus") == null) {
		threadName = request.getParameter("threadfocus");
	} else {
		threadName = request.getParameter("threadfocus");
	}	
String database=session.getAttribute("database").toString();
 if(session.getAttribute("par")==null){        
        ParameterObj parameterObj=new ParameterObj(database, projectn, threadName);
        session.setAttribute("par", parameterObj);
        int count=1;
        session.setAttribute("count", count);
               }  else{ 
                    int watchdog=1;
                if(session.getAttribute("count")==null)
                    watchdog=1;
                else
                    watchdog=Integer.parseInt(session.getAttribute("count").toString());
                watchdog++;
                if(watchdog>2){
                ParameterObj parameterObj=(ParameterObj)session.getAttribute("par");
                
                parameterObj.setThreadName(threadName);
                session.setAttribute("par", parameterObj);
                }
                session.setAttribute("count", watchdog);
               }          
      
/*}else{
    ParameterObj parameterObj=(ParameterObj)session.getAttribute("par");
    database=parameterObj.getDatabase();
    projectn=parameterObj.getProjectName();
    threadName=parameterObj.getThreadName();
} */              
%>
<html>
<head>
    
     <title>思维: <%=threadName%> 探究课题： <%=projectn%></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="../css/stickyhead.css" rel="stylesheet" type="text/css">
<link href="../css/thread.css" rel="stylesheet" type="text/css">
<link href="../css/screenLock.css" rel="stylesheet" type="text/css">
<link href="../css/itm_week_beta.css" rel="stylesheet" type="text/css">
<LINK REL=StyleSheet HREF="../css/btn_link.css" TYPE="text/css">

<script src="../js/jquery.js" type="text/javascript"></script>
<script language="javascript"type="text/javascript">
  <%
  String[] strid = null;
  //String database=request.getParameter("database");
  //String projectn=request.getParameter("projectname");
  Operatedb opdb_author = new Operatedb(new sqls(),database);
  AccessToProjects atp = new AccessToProjects();
  System.out.println("projectname");
  boolean canEditThread = atp.canEdit(username,usertype,projectn,database);
  String nidts_str = request.getParameter("nidts");
  if (nidts_str != null)
  {
  	nidts_str = nidts_str.replaceAll(" ", "&#32;");
  }
  %>
  var _projectname = "<%=projectn%>";
  var _threadfocus = "<%=threadName%>";
  var _database = "<%=database%>";
  var _nitds = "<%=request.getParameter("nidts") %>";
  
  /*Control thread updates or creation*/
  function UpdateThreadControl(canEditThread)
  {
  	var selt = document.getElementById("rename_thread");
  	var self = document.getElementById("find_more_notes");
  	//var selj = document.getElementById("journey_of_thinking");
  	var seld = document.getElementById("delete_thread");
  	var selr = document.getElementById("remove_note");
  	var selh = document.getElementById("hightlight_note");
  	if (canEditThread == "false")
  	{
  		console.log("[DEBUG] User does not has create new thread permissions");
  		
  		// Disable create functionality
  		selt.removeAttribute("href");
  		selt.removeAttribute("onclick");
  		selt.setAttribute("id", "rename_thread");
  		selt.style.color  = "gray";
  		
  		self.removeAttribute("href");
  		self.removeAttribute("onclick");
  		self.setAttribute("id", "find_more_notes");
  		self.style.color  = "gray";
  		
  		//selj.removeAttribute("href");
  		seld.removeAttribute("href");
  		seld.removeAttribute("onclick");
  		seld.setAttribute("id", "delete_thread");
  		seld.style.color  = "gray";
  		
  		if(selr != null)
  		{
  		   selr.removeAttribute("href");
  		   selr.removeAttribute("onclick");
  		   selr.setAttribute("id", "remove_note");
  		 selr.style.color  = "gray";
  		}
  		if(selh != null)
  		{
  		  selh.removeAttribute("href");
  		  selh.removeAttribute("onclick");
  		  selh.setAttribute("id", "highlight_note");
  		  selh.style.color  = "gray";
  		}	
  	}
  }
</script>
<script src="../js/cn/thread_update.js"></script>
<script src="../js/itm_week_beta.js"></script>
<script src="../js/cn/screenLock.js"></script>
<script  language="javascript"type="text/javascript" charset="utf-8">
if("yes"=="<%=request.getParameter("ifexist")%>"){
	alert("the thread already exists! please change the focus");
}

function SumWin()
{
	
	window.open('ThreadSum.jsp?database=<%=database%>&threadfocus=<%=request.getParameter("threadfocus")%>&projectname=<%=request.getParameter("projectname")%>','sumwin'); 
}


function onCreate(){
    if (document.getElementById("input_threadfocus").value=="")
	{
		window.alert('Please input the focus!' );
		 return false;
	}
        document.forms["createThreadForm"].submit();
}

function onCreateThread(){
    if (document.getElementById("createThreadForm").value=="")
	{
		window.alert('Please input the focus!' );
		 return false;
	}
        document.forms["createThreadForm"].submit();
}

function OnFind()
{
	if (document.getElementById("input_threadfocus").value=="")
	{
		window.alert('Please input the focus!' );
		 return false;
	}
	top.window.location="thread_action.jsp?database=<%=request.getParameter("database")%>&projectname=<%=request.getParameter("projectname")%>&threadfocus="+document.getElementById("input_threadfocus").value;
}

function OnFindMore()
{  
	OnSave();        
	top.buttomframe2.location="SearchMore.jsp?database=${par.database}&projectname=${par.projectName}&threadfocus=${par.threadName}";
	top.document.getElementById("framesetter").rows = "100px,*"
}
</script>
        <script>
            $(document).ready(function(){
                $('#linkSuggest a').click(function(){
                    $('#td_name').css({
                        'overflow-y': 'auto',
                        'height': '620px', 
                        'width': '1024px'
                    });
                    var url=$(this).attr('href');                    
                    $('#td_name').load(url);                    
                    return false;
                });//end click
                
            });
        </script>
        

            
</head>

<%
NoteModel noteobj = new NoteModel(database);
NoteController nc = new NoteController(database);

%>
<body onload="render();UpdateThreadControl('<%= canEditThread%>');">
<div id="sticky">
	<div id="header">
	<table border="0">
		<tr>
			<td><img src="../img/itmlogo100x100.gif" alt="ITM LOGO" height="60" width="60" /></td>
			<td style="background-color: #01B0F1;color:yellow;font-family:Arial, Helvetica, sans-serif;" height="60" width="870">
			<%if(request.getParameter("ifexist")==null && request.getParameter("threadfocus")==null){%>
			&nbsp;&nbsp;&nbsp;<font size="5">为探究课题“<%=request.getParameter("projectname")%>”添加思维脉络</font>
			<%}else if(request.getParameter("ifexist")!=null && request.getParameter("threadfocus")!=null){ %>
			&nbsp;&nbsp;&nbsp;<font size="5">为探究课题“<%=request.getParameter("projectname")%>”添加思维脉络</font>
			<%}else{ %>
			&nbsp;&nbsp;&nbsp;<font size="5">更新思维脉络： <%=threadName%></font><br><font size="3">&nbsp;&nbsp;&nbsp;探究课题： <%=projectn%></font>
			<%} %>			</td>
		</tr>
	</table>
	<div id=td_info>
<div id="td_name">
<%if(request.getParameter("ifexist")!= null && request.getParameter("threadfocus")!=null){%>
输入脉络名:
<form>
<input class="fields" name="thread_focus" id="input_threadfocus" type = "text" size = "25" value ="<%=request.getParameter("threadfocus") %>" />
	<a href="#" onclick="OnFind()">确定</a>
</form>
        
<%}else if(request.getParameter("ifexist")== null && request.getParameter("threadfocus")==null){%>
输入思维脉络名称（主题）:
    <form id="createThreadForm" action="../addThreadServlet?database=<%=database%>" method="post">
    <input class="fields" id="input_threadfocus" name="thread_focus"  type = "text" size = "25" value ="" onkeydown="return new_thread_searchKeyPress(event);"
        />
    <a href="#" onclick="OnFind()" class="close">确定</a>          
    <a href="New_HomePage.jsp?database=${database}" onclick="" class="close">Close</a>
    
    </form>
    <h4 id="linkSuggest">
        <a href="../GetViewServlet">Search Suggest Thread Names</a>
    </h4>
    
    
<%}else if(request.getParameter("ifexist")== null &&request.getParameter("threadfocus")!=null){%>


</div><span id="ajax_state"></span>
	<div id="button">
	<ul>
            <% 
            if(!request.getParameter("getvalue").equals("@")){ %>
	<li><a href="#" onclick="OnFindMore()">查找更多短文</a></li>
        <%
               }    %>
	<li><a href="#" onclick="SumWin()">进程反思</a></li>
	<li><a href="#" onclick="ShowRenameThread()">重命名</a></li>
        <li><a href="#" onclick="ShowDeleteThread()">删除</a></li>
	<li><a href="#" onclick="OnClose()">保存并关闭</a></li>
	</ul>
	</div>
		<%}%>
</div>
	
</div>
</div>
<div id="wrapperu">
<div id="wrapper">


<!--------------------------------display the individual thread map----------------------------------------->
	    <%
	    String getvalue = request.getParameter("getvalue");
	    System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!   "+getvalue);
	    if((getvalue != null) && !(getvalue.equals("null")) && !(getvalue.equals("@")))
	    {
	    	System.out.println(request.getParameter("getvalue"));
	    	String[] strvalue = request.getParameter("getvalue").split("&");	    	
	    	strid = strvalue[0].split("@");
	    	String strcon = "( note_table.noteid = ";
		   	String threadNoteQueryStr = "thread_note.noteid =";
	    	
	    	int[] intid = new int[1000]; //this limits the number of id is 1000 at most!
	    	int ival;
	    
	    	// Get the id of selected notes 
	    	for(int i=1; i< strid.length; i++) {  // Be careful, the index starts with "1" here!!
				intid[i] =  Integer.valueOf(strid[i]).intValue(); //change the id value from string to int
				if( i < strid.length-1){
					strcon += intid[i]+ " or note_table.noteid = ";	
				}else{
					strcon += intid[i] + ")";
				}				 
				
				// System.out.println("thread.jsp : the selected note's id is "+intid[i]);
			}
			
			for(int i = 1; i< strid.length; i++) {  // Be careful, the index starts with "1" here!!
				if( i < strid.length-1){
					//threadNoteQueryStr += intid[i]+ " and thread_note.noteid = ";	
					threadNoteQueryStr += intid[i]+ " or thread_note.noteid = ";	
				} else {
					threadNoteQueryStr += intid[i] + "";
				}
			
				// System.out.println("thread.jsp : the selected note's id is "+intid[i]);
			}
			
			//get the notes from database
			sqls s = new sqls();
			Operatedb opdb = new Operatedb(s,database);
			
			//using hashtable to store the note's relation
			ResultSet noters;
			String strtemp;
			String strtype=null;
			Hashtable<Integer,Vector<Integer>> butable = new Hashtable<Integer, Vector<Integer>>();
			Hashtable<Integer,Vector<Integer>> antable = new Hashtable<Integer, Vector<Integer>>();
			Hashtable<Integer,Vector<Integer>> retable = new Hashtable<Integer, Vector<Integer>>();
			
			for(int i=1; i<strid.length; i++)
			{
				//query the database				
				//System.out.println("the length is "+strid.length);
				//System.out.println("the i value is "+i);
				//System.out.println("the intid is "+intid[i]);
				strtemp ="fromnoteid = " + intid[i]; 
				
				//System.out.println("the strcon is "+strcon);
				noters = opdb.GetRecordsFromDB("note_note","linktype,tonoteid",strtemp);
				
				//add to hash table
				Vector<Integer> anvec = new Vector<Integer>();
				Vector<Integer> buvec = new Vector<Integer>();
				Vector<Integer> revec = new Vector<Integer>();
				
				while(noters.next()){	
					strtype = noters.getString(1);
					//System.out.println("note: "+intid[i]+"  "+ strtype+" note: "+noters.getInt(2));
					if(strtype.equals("annotates")){
						anvec.add(noters.getInt(2));
					}else if (strtype.equals("buildson")){
						buvec.add(noters.getInt(2));
					}else if (strtype.equals("references")){
						revec.add(noters.getInt(2));
					}
				}
				
				if(noters != null){
					try{
						noters.close();
					}catch(SQLException e){
						e.printStackTrace();
					}
				}
				antable.put(intid[i],anvec);
				butable.put(intid[i],buvec);
				retable.put(intid[i],revec);
				
				//System.out.println("note "+intid[i]+"'s anvec's size is "+ anvec.size());
				//System.out.println("note "+intid[i]+"'s buvec's size is "+ buvec.size());
				//System.out.println("note "+intid[i]+"'s revec's size is "+ revec.size());
			}		
	       Vector<Integer> vec;
		  
	       String[] namearray=new String[20];
	      
	      namearray[0] = "thread_note";
	      String threadFocusName = request.getParameter("threadfocus");
	      strtemp =  "thread_note.threadfocus='" + threadFocusName +"'";
	      
	      // Variables used for query
	      ResultSet  disrs = null;	
	      boolean threadIsPopulated = false;
	      int numberOfTables = 5;
	      String colNames = "note_table.noteid,notetitle,notecontent,createtime,firstname,lastname,view_table.title";
	      
	      disrs = opdb.MulGetRecordsFromDB(namearray,"projectid,threadfocus,noteid,highlight",strtemp, 1);
	      
	      System.out.println("[DEBUG] Query executed for check is: " + strtemp);
	      
	      // Store state of highlight 
	      Hashtable<Integer, Integer> idToHighlightMap = new Hashtable<Integer, Integer>();
	      
		  if(disrs != null && disrs.next())
	      {
			  int rowcount = 0;
			   
			  do
			  {
				idToHighlightMap.put(disrs.getInt(3), disrs.getInt(4));
				++rowcount;				  
			  }while(disrs.next());
			  
			  // Assuming that ids are same, we could do a better job here where we go id by id
			  if (rowcount == (strid.length - 1))
			  {
		  		System.out.println("thread_note is already populated");
		    	threadIsPopulated = true; 
			  }
	      }
		  
		  // Check if incoming ids are contained in the hashtable, if they do, do nothing, if not
		  // then insert them with highlight state being 0 since we don't know anything about them (they are new)
	      for(int i=0; i < intid.length; ++i)
	      {
	    	  if(idToHighlightMap.containsKey(intid[i]))
	    	  {
	    		  // Do nothing since we already know about the state of highlight for this note id
	    	  }
	    	  else
	    	  {
	    		  idToHighlightMap.put(intid[i], 0);
	    	  }
	      }
		  
		  namearray[0] = "note_table"; 
		  namearray[1] = "author_table";
		  namearray[2] = "author_note";
		  namearray[3] = "view_table";
		  namearray[4] = "view_note";
		  
		  //strtemp = strcon + " AND note_table.noteid=author_note.noteid AND author_note.authorid=author_table.authorid AND note_table.noteid=view_note.noteid AND view_note.viewid=view_table.idview";
		  strtemp = strcon + " AND note_table.noteid=author_note.noteid AND author_note.authorid=author_table.authorid AND note_table.noteid=view_note.noteid AND view_note.viewid=view_table.idview";
		  if (threadIsPopulated)
		  {
			 namearray[5] = "thread_note";
			 strtemp += " AND thread_note.noteid = note_table.noteid";
			 colNames += ",thread_note.highlight";
			 numberOfTables = 6;
		  }
		  strtemp += " GROUP BY note_table.noteid";
		  System.out.println("Query executed is: " + strtemp);
		  
		  // Check first if the thread_note alread contains entry entry for this note, if it does, then form a query 
		  // that includes the thread_note or else, just use the query below.
		
		  disrs = opdb.MulGetRecordsFromDB(namearray, colNames, strtemp, numberOfTables);
		  //disrs = noteobj.getNote(sql);
				  
		  %><div id="fm">
		  <div id="threadinfo"></div>
	
	<ul id="flag">
		<li id="showalltitle">&nbsp;|&nbsp;<a href="javascript:void(0)" onclick="showalltitle()">显示标题</a>&nbsp;|&nbsp;</li>
		<li id="showallauthor"><a href="javascript:void(0)" onclick="showallauthor()">显示作者</a>&nbsp;|&nbsp;</li>
		<li id="showalllink">
		<a href="javascript:void(0)" onclick="showbldlink()">显示发展关系</a>&nbsp;|&nbsp;
		<a href="javascript:void(0)" onclick="showreflink()">显示引用关系</a>&nbsp;|&nbsp;
		</li>
	</ul>

</div>
<div id="up_area">
<div id="wrapper_draw">
		  <div id="right_area">
	     <div id="draw_area" onclick="drawnoteclick(event)"></div>
	     <div id="reference_area"></div>
	     <div id="buildon_area"></div>
	     <div id="annotate_area"></div>
	     <div id="background_area"></div>
	     
	     <div id="bar_left" onclick="windowmoveleft()"></div>
	     <div id="bar_area">
	     <div id="bar"></div>
	     <div id="pointer" onmousedown="ptdown(event)" onmouseup="ptup(event)"></div>
	     </div>
	     <div id="bar_right" onclick="windowmoveright()"></div>
	     <div id="time_area"></div>
	     </div>
	     

</div>
<div id="draw_ctrl">
	<ul id="node_contral">
		<li>短文：&nbsp;&nbsp;</li>
		<li><a href="javascript:void(0)" onclick="remove_note()" id="remove_note">移除短文</a>&nbsp;|&nbsp; </li>
		<li id="showhighlight"><a href="javascript:void(0)" onclick="showhighlight()" id="hightlight_note">高亮短文</a>&nbsp;</li>
	</ul>

	<ul id="zoom">
		<li>显示范围：&nbsp;&nbsp;</li>
		<li><a href="javascript:void(0)" onclick="bar(0)">按天显示</a>&nbsp;|&nbsp; </li>
		<li><a href="javascript:void(0)" onclick="bar(1)">按周显示</a>&nbsp;|&nbsp; </li>
		<li><a href="javascript:void(0)" onclick="bar(2)">按双周显示</a>&nbsp;|&nbsp; </li>
		<li><a href="javascript:void(0)" onclick="bar(3)">按月显示</a>&nbsp;|&nbsp; </li>
		<li><a href="javascript:void(0)" onclick="bar(4)">全部显示</a> </li>
	</ul>
</div>
<div id="wrapper_ln">
<div id="list_area" onclick="listclick(event)"></div>
<div id="note_content">
<h3>请从左边选择一篇短文</h3>

</div>
</div>
</div>
		<div id="noteid_with_timestamp" value=<%=nidts_str%> ></div>
	    <div id="raw_area">
		<% 	
			System.out.println("[INFO : thread.jsp]******************" +  request.getParameter("nidts"));
		
			while(disrs.next())
			{
				//System.out.println("the note's id is " + disrs.getString(1));
				//System.out.println("the note's date is "+ disrs.getString(4));
				//System.out.println("the note's author is "+ disrs.getString(5)+"  "+disrs.getString(6));
				/*
				vec = retable.get(disrs.getInt(1));
				for(int i=0; i<vec.size();i++){
				    System.out.println("reference1:	from note "+disrs.getInt(1)+" to note " + vec.get(i));
				}
				vec.clear();
				
				vec = butable.get(disrs.getInt(1));
				for(int i=0; i<vec.size();i++){
				    System.out.println("buildson1:	from note "+disrs.getInt(1)+" to note " + vec.get(i));
				}
				vec.clear();
				
				vec = antable.get(disrs.getInt(1));
				for(int i=0; i<vec.size();i++){
				    System.out.println("annotate1:	from note "+disrs.getInt(1)+" to note " + vec.get(i));
				}*/
				
				// Default
				String highlighted = "0";
				highlighted = Integer.toString(idToHighlightMap.get(disrs.getInt(1)));
				
		%>
			<div class="rawnote" highlight="<%=highlighted %>" noteid="<%=disrs.getString(1)%>" view="<%=disrs.getString(7) %>" author="<%List authorList = opdb_author.getMultipleAuthors(disrs.getString(1));
					  for(int i=0;i<authorList.size();i++){
						  String name = (String)authorList.get(i);
                                                  name=StringUtils.AdjustChineseName(name);
                                          %>
						  <%=name %>
						  <%if(i < authorList.size()-1){
						  %>
						  ,&nbsp;
						  <%}%>	  
					  <%}%>" firstn="<%=disrs.getString(5)%>"date="<%=disrs.getString(4)%>">
			<!--display the note's relationship -->
			<div class="references">
				<%vec = retable.get(disrs.getInt(1));
				  for(int i=0; i<vec.size();i++){
				    System.out.println("reference:	from note "+disrs.getInt(1)+" to note " + vec.get(i));%>
				  	<div class="reference" target="<%=vec.get(i)%>"> 
					</div>
				<%	  
				  }
				  vec.clear();
				%>				
			</div>
			<div class="buildsons">
				<%vec = butable.get(disrs.getInt(1));
				  for(int i=0; i<vec.size();i++){
				  System.out.println("buildson:	from note "+disrs.getInt(1)+" to note " + vec.get(i));%>
				  	<div class="buildson" target="<%=vec.get(i)%>"> 
					</div>
				<%	  
				  }
				  vec.clear();
				%>	
			</div>
			<div class="annotates">
				<%vec = antable.get(disrs.getInt(1));
				  for(int i=0; i<vec.size();i++){
				  System.out.println("annotates:	from note "+disrs.getInt(1)+" to note " + vec.get(i));%>
				  	<div class="annotate" target="<%=vec.get(i)%>"> 
					</div>
				<%	  
				  }
				  vec.clear();
				%>	
			</div>
			<div class="rawtitle"><%=disrs.getString(2)%></div>
			<%
			/* String content="";
			if(disrs.getString("support")==null)
				content = disrs.getString("notecontent");
			else
				content = nc.getContent(disrs.getString("noteid"));
// 				content = nc.getOffsetContent(disrs.getString("noteid")); */
			%>
 			<div class="rawcontent"> 
				<%= nc.getContent(disrs.getString("noteid")) %> 
			</div> 
		</div>
		<% 
		}
		%>
</div>
<% 
	if(disrs != null){
		try{
			disrs.close();
		}catch(SQLException e){
			e.printStackTrace();
		}
	 }

	if(opdb != null){
		opdb.CloseCon();
	}

}%>

<!-------------------------------end of the if statement--------------------------------------->
</div>
</div>

</body>
</html>