<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page session="true"%>
<%@ page import="java.util.*" %>
<%@ page import="javax.mail.*" %>                       
<%@ page import="javax.mail.internet.*" %>           
<%@ page import="javax.activation.*" %>               
<%@ page import="java.util.*,java.io.*" %>

<%@page import="database.Operatedb"%>
<%@page import="code.*"%>
<%@ page import="java.sql.*" %>

<!DOCTYPE html>
<html>
<%
	String strsucceed = request.getParameter("ifsucceed");
	String strlogout = request.getParameter("logout");
	if (strlogout != null) {
		session.invalidate();
	}
%>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Welcome - Idea Thread Mapper</title>
<script type="text/javascript">
	var info = '<%=strsucceed%>';
	if (info == 'no') {
		alert("Wrong login information,please try again");
	}
	if (info == 'yes') {
		window.alert("Your application has been submitted, you will receive a notification after you application is aproved.");
	}
	
</script>

<script type="text/javascript"> 
function checkValidate()  
{   
  if(registration_form.fname.value==""||registration_form.fname.value==null)  
  {  	    
	  window.alert("First name cannot be empty!!!");   
	  return false;  
  }  
  
  else if(registration_form.lname.value==""||registration_form.lname.value==null)  
  {    
	  window.alert("Last name cannot be empty!!!");   
	  return false;  
  }  
  
  else if(registration_form.id.value==""||registration_form.id.value==null)  
  {   
	  window.alert("Username cannot be empty!!!");  
	  return false;  
  } 
  
  else if(registration_form.password.value==""||registration_form.password.value==null)  
  {  
	  window.alert("Password cannot be empty!!!");  
	  return false;  
  } 
  
  else if(registration_form.Email.value==""||registration_form.Email.value==null)  
  {   
	  window.alert("E-mail cannot be empty!!!");  
	  return false;  
  } 
  
  else if(registration_form.URL.value==""||registration_form.URL.value==null)  
  {  
	  
	  window.alert("URL cannot be empty!!!");    	 
	  return false;  
  } 
  
  else if(registration_form.db.value==""||registration_form.db.value==null)  
  {  
	  
	  window.alert("Database cannot be empty!!!");    	 
	  return false;  
  } 
  
  else if(registration_form.phone.value==""||registration_form.phone.value==null)  
  {   
	  window.alert("Phone number cannot be empty!!!");   
	  return false;  
  } 
  
 /* else if(registration_form.phone.value!="")
  { 
	 var reg2=/?.+\.(com|net|org|edu)$/;
     if(!reg2.test(registration_form.phone.value)) 
     { 
        alert("Please enter a valid URL"); 
        return false; 
         
     } 
  }*/
  
  else if(registration_form.port.value==""||registration_form.port.value==null)  
  {    
	  window.alert("URL port cannot be empty!!!");   
	  return false;  
  } 
  
 		registration_form.url.value=location.href;
 		//alert(registration_form.url.value);
 		window.alert("Your application has been submitted, you will receive a notification after you application is aproved.");
		return true;
}  
</script>  

<script type="text/javascript">
	/*function onSubmit(form)
	{
		form.url.value=location.href;
		return true;
	}*/

</script>

</head>
<LINK REL=StyleSheet HREF="css/default.css" TYPE="text/css"></LINK>
<body bgcolor="aliceblue">
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<center>
		<form style="border-width:1px;border-style:solid;border-color: #006600;width:50%"
			name="registration_form" action="../Register" method="post" onsubmit="return checkValidate();">
			<br />
			<h2>
				<font size="8" color="#71C2B1" face="Times New Roman"> Idea Thread Mapper </font>
			</h2>
			<br>
			<h3>Please Enter Your Knowledge Forum Information</h3>
			<br>		 
			<table>		
			    <tr>
					<td><font size="3" color="#71C2B1" face="Times New Roman"><label><b>URL</b>
						</label></font></td>
					<td><input class="fields" name="URL" id="URL"
						type="text" size="20" value="" /></td>
				</tr>
				
				<tr>
					<td><font size="3" color="#71C2B1" face="Times New Roman"><label><b>URL Port:</b>
						</label></font></td>
					<td><input class="fields" name="port" id="port"
						type="text" size="20" value="80" /></td>
				</tr>
				
				 <tr>
					<td><font size="3" color="#71C2B1" face="Times New Roman"><label><b>Database:</b>
						</label></font></td>
					<td><input class="fields" name="db" id="db"
						type="text" size="20" value="" /></td>
				</tr>
			
				<tr>
					<td><font size="3" color="#71C2B1" face="Times New Roman"><label><b>Organization:</b>
						</label></font></td>
					<td><input class="fields" name="org" id="org"
						type="text" size="20" value="" /></td>
				</tr>
				<tr>
					<td><font size="3" color="#71C2B1" face="Times New Roman"><label><b>Username:</b>
						</label></font></td>
					<td><input class="fields" name="id" id="id"
						type="text" size="20" value="" /></td>
				</tr>
				<tr>
					<td><font size="3" color="#71C2B1" face="Times New Roman"><label><b>Password:</b>
						</label></font></td>
					<td><input class="fields" name="password" id="password"
						type="password" size="20" value="" /></td>
				</tr>
				
				
								
			</table>
			</br>
			<h3>Please Enter Your Personal Information</h3>
			</br>
			<table>		
				<tr>
					<td><font size="3" color="#71C2B1" face="Times New Roman"><label><b>First Name:</b>
						</label></font></td>
					<td><input class="fields" name="fname" id="fname"
						type="text" size="20" value="" /></td>
				</tr>
				<tr>
					<td><font size="3" color="#71C2B1" face="Times New Roman"><label><b>Last Name:</b>
						</label></font></td>
					<td><input class="fields" name="lname" id="lname"
						type="text" size="20" value="" /></td>
				</tr>
				
				<tr>
					<td><font size="3" color="#71C2B1" face="Times New Roman"><label><b>E-mail:</b>
						</label></font></td>
					<td><input class="fields" name="Email" id="Email"
                                                   type="email" size="20" value="@" /></td>
				</tr>
				
				<tr>
					<td><font size="3" color="#71C2B1" face="Times New Roman"><label><b>Phone Number:</b>
						</label></font></td>
					<td><input class="fields" name="phone" id="phone"
                                                   type="tel" size="20" value="" /></td>
				</tr>
			</table>
			
			<font color="#4889EB" size="3" face="Times New Roman"><b><input
					style="width: 80px; height: 31" name="submit" id="" type="submit"
					value="submit"></b></font> 
					<br /> <br />
					<h6>Idea Thread Mapper (ITM) version 1.8, updated on February 08, 2013</h6>
					
			<input type="hidden" name="url"  id="url" />
			
		</form>
	</center>
</body>
</html>