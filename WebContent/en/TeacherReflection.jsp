<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*,java.sql.*,code.*,database.*,java.text.SimpleDateFormat;"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
/************validate the user session*******************/
String user =" ";
String usertype =" ";
	session = request.getSession(false);
	if(session != null){
		if (session.getAttribute("username") == null){
			response.sendRedirect("/itmdev/index.jsp");
		}
		else
		{
		    user = (String)session.getAttribute("username");
			usertype = (String)session.getAttribute("usertype");
		}
	}
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Teacher Reflection on This Inquiry Project</title>
<style type="text/css">
.inButtonColor {
 background-image: url(images/button.jpg);
 width:197px;
 height:72px;}
.divinput{
margin: 0px 0px 0px 10px;
width: 300px;
float: left;
}
.divinput
{
display:block;
} 
#atable {
	margin-left:auto;
	margin-right:auto;
}

#atable td {
	padding-left:40px;
	padding-right:40px;
	padding-top:10px;
	padding-bottom:10px;
}

.btable {
	margin-left:auto;
	margin-right:auto;
	border-spacing:0;
	text-align: center;
}

.btable td {
	border: 1px solid black;
	padding-left:40px;
	padding-right:40px;
	padding-top:10px;
	padding-bottom:10px;
}

</style>

<script type="text/javascript" >
	function OnHistory(){
		window.open ('History.jsp?database=<%=request.getParameter("database")%>&projectname=<%=request.getParameter("projectname")%>&threadfocus=<%=request.getParameter("threadfocus")%>'); 
	}
	
	function OnClose(){
		window.close();
	}
	
	function OnTag1(){
		document.summary_form.ProblemContent.value +="\n\n[ We want to understand:]";
	}
	
	function OnTag2(){
		document.summary_form.IdeaContent.value +="\n\n[ We used to think:]";
	}
	
	function OnTag3(){
		document.summary_form.IdeaContent.value +="\n\n[ We now understand:]";
	}
		
	function OnTag4(){
			document.summary_form.MoreContent.value +="\n\n[ We need to further understand ]";
	}
	
	function OnTag5(){
		document.summary_form.MoreContent.value +="\n\n[ We need better theories about ]";
	}
	
	function OnTag6(){
		document.summary_form.MoreContent.value +="\n\n[ We need to read more about ]";
	}
	function OnTag7(){
		document.summary_form.MoreContent.value +="\n\n[ We need evidence about ]";
	}
	function OnTag8(){
		document.summary_form.MoreContent.value +="\n\n[ We need to look at our different ideas about ]";
	}
	
</script>
<LINK REL=StyleSheet HREF="css/stickyhead.css" TYPE="text/css"></LINK>
</head>

	
<% 
	String strname = request.getParameter("projectname");
	String strfocus = request.getParameter("threadfocus");
	String strdb = request.getParameter("database");
	
	Operatedb opdb_author = new Operatedb(new sqls(),strdb);
	AccessToProjects atp = new AccessToProjects();
	
	boolean caneditjourney = atp.canEdit(user,usertype,strname,strdb);
	System.out.println("CanEditJourney is "+ caneditjourney);
	
	String strProblem = null;
	String strIdea = null;
	String strMore = null;
	String strsql;
	String strproject;
	Operatedb opdb=null;
	sqls s=new sqls();
	Statement stmt= s.Connect(strdb);
	
	if(request.getParameter("save")!=null && caneditjourney)
	{
		String strcontent = request.getParameter("ProblemContent");
		String ideacon = request.getParameter("IdeaContent");
		String morecon = request.getParameter("MoreContent");
		strcontent = strcontent.replace("'","''").replace("\\","\\\\");
		ideacon = ideacon.replace("'","''").replace("\\","\\\\");
		morecon = morecon.replace("'","''").replace("\\","\\\\");
		
		//store the three text sections into database
		String strtime;
	    java.util.Date curdate = new java.util.Date();
		opdb = new Operatedb(s,strdb);
		SimpleDateFormat spdtformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		//get the projectid
		ResultSet temprs=null;
		temprs=opdb.GetRecordsFromDB("project","idproject","projectname='"+strname+"';");//get the project id
		temprs.next();
		strproject = temprs.getString(1);
		if(temprs !=null){
			try{
				temprs.close();
			}catch(SQLException e){
				e.printStackTrace();
			}
		}
		
		//get current time
		strtime = spdtformat.format(curdate);
		
		//get the author
		String username=null;
		session = request.getSession(false);
		if(session != null){
			username = (String)session.getAttribute("username");
		}
		
		//store the problem into database
		strsql = " (`threadfocus_problem`, `createtime`,`problem`,projectid,author_problem) VALUES ( '"+strfocus+"','" + strtime+"','" + strcontent+"',"+strproject+",'"+username+"');"; 
		opdb.WritebacktoDB("thread_problem","INSERT",strsql);
		System.out.println("write into thread_problem table is :"+strsql);
		
		//store the idea  into database
		strsql = " (`threadfocus_idea`, `createtime`,`idea`,projectid,author_idea) VALUES ( '"+strfocus+"','" + strtime+"','" + ideacon+"',"+strproject+",'"+username+"');";  
		opdb.WritebacktoDB("thread_idea","INSERT",strsql);
		
		//store the "more" content into database
		strsql = " (`threadfocus_more`, `createtime`,`more`,projectid,author_more) VALUES ( '"+strfocus+"','" + strtime+"','" + morecon+"',"+strproject+",'"+username+"');"; 
		opdb.WritebacktoDB("thread_more","INSERT",strsql);
	}	
%>

<%
	//Get the latest problem from  database
	ResultSet rs_problem = null;
	strsql = "select problem from thread_problem, project WHERE threadfocus_problem='"
	                +strfocus+"' and thread_problem.projectid = project.idProject and project.projectname='"
	                +strname+"' order by createtime desc limit 1";
	rs_problem = stmt.executeQuery(strsql);
	if(rs_problem.next()){
		strProblem = rs_problem.getString(1);
		//System.out.println("rs_problem.getString(1) is : "+rs_problem.getString(1));
	}
	if(rs_problem != null){
		try{
			rs_problem.close();
		}catch(SQLException e){
			e.printStackTrace();
		}
		
	}
	//Get the latest idea from  database
	ResultSet rs_idea = null;
	strsql = "select idea from thread_idea, project WHERE threadfocus_idea='"
	                +strfocus+"' and thread_idea.projectid = project.idProject and project.projectname='"
	                +strname+"' order by createtime desc limit 1";
	rs_idea = stmt.executeQuery(strsql);
	if(rs_idea.next()){
		strIdea = rs_idea.getString(1);
	}
	if(rs_idea != null){
		try{
			rs_idea.close();
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	//Get the latest "more" from  database
	ResultSet rs_more = null;
	strsql = "select more from thread_more, project WHERE threadfocus_more='"
	                +strfocus+"' and thread_more.projectid = project.idProject and project.projectname='"
	                +strname+"' order by createtime desc limit 1";
	rs_more = stmt.executeQuery(strsql);
	if(rs_more.next()){
		strMore = rs_more.getString(1);
	}
	if(rs_more != null){
		try{
			rs_more.close();
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
%>
<body>
<div id="sticky">
	<div id="header">
	<table border="0">
		<tbody><tr>
			<td><img src="img/itmlogo100x100.gif" alt="ITM LOGO" height="100" width="100"></td>
			<td style="background-color: #01B0F1;color:yellow;font-family:Arial, Helvetica, sans-serif;font-size:30px;" height="60" width="850">
			&nbsp;&nbsp;Teacher Reflection on This Inquiry Project
			</td>
		</tr>
	</tbody></table>			
</div>
</div>
<div id="wrapperu">
<div id="wrapper">
<div>
<form name="summary_form" action="/itmdev/ThreadSum.jsp?database=<%=request.getParameter("database")%>&projectname=<%=request.getParameter("projectname")%>&threadfocus=<%=request.getParameter("threadfocus")%>" method="post">
<div>
<table id="atable">
<tr>
	<td>
	<textarea onclick=";" name="ProblemContent" id="problemcontent" rows="10" cols="30">
		</textarea>
	</td>
	<td>
	<textarea onclick=";" name="ProblemContent" id="problemcontent" rows="10" cols="30">
		</textarea>
	</td>
</tr>
<tr>
	<td>
	<textarea onclick=";" name="ProblemContent" id="problemcontent" rows="10" cols="30">
		</textarea>
	</td>
	<td>
	<textarea onclick=";" name="ProblemContent" id="problemcontent" rows="10" cols="30">
		</textarea>
	</td>
</tr>
</table>
</div> 
<div style="margin-left:auto;margin-right:auto;">
	<table class="btable"> 
		<tr>
		<td>
			<span>Upload a file</span>
		</td>
		<td>
			<span>File attached</span>
		</td>
		<td>
			<button type="Button">Upload</button>
		</td>
		</tr>
	</table>
</div>
<center>
<input type="submit" name="save" id="save" value="Save" onclick="OnSave()"/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input type="button" name="" id="" value="Close" onclick="OnClose()"/>	
</center>
	</form>
</div>
</div>
</div>
</body>
</html>
<%
	if(opdb != null){
		opdb.CloseCon();
	}
	
	if(stmt != null){
		stmt.close();
	}
	
	if(s != null){
		s.Close();
	}
%>