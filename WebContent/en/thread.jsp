<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*,java.sql.*,com.knowledgeforum.k5.common.*,org.zoolib.*,org.zoolib.tuplebase.*,code.*,database.*"%>
    
<%@ page import="itm.models.NoteModel" %>
<%@ page import="itm.controllers.NoteController" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
/************validate the user session*******************/
String username =" ";
String usertype =" ";
	session = request.getSession(false);
	if(session != null){
		if (session.getAttribute("username") == null){
			response.sendRedirect("/itmdev/index.jsp");
		}
		else
		{
		  username = (String)session.getAttribute("username");
		  usertype = (String)session.getAttribute("usertype");
		}
	}
%>
<html>
<head>
<title>Thread <%=request.getParameter("threadfocus")%> in Project <%=request.getParameter("projectname")%></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css/stickyhead.css" rel="stylesheet" type="text/css">
<link href="css/thread.css" rel="stylesheet" type="text/css">
<link href="css/screenLock.css" rel="stylesheet" type="text/css">
<link href="css/itm_week_beta.css" rel="stylesheet" type="text/css">
<script src="js/jquery.js"></script>
<script src="http://d3js.org/d3.v3.min.js" type="text/javascript"></script>
<script language="javascript"type="text/javascript">
  <%
  String[] strid = null;
  String database=request.getParameter("database");
  String projectn=request.getParameter("projectname");
  Operatedb opdb_author = new Operatedb(new sqls(),database);
  AccessToProjects atp = new AccessToProjects();
  System.out.println("projectname");
  boolean canEditThread = atp.canEdit(username,usertype,projectn,database);
  String nidts_str = request.getParameter("nidts");
  if (nidts_str != null)
  {
  	nidts_str = nidts_str.replaceAll(" ", "&#32;");
  }
  %>
  var _projectname = "<%=request.getParameter("projectname")%>";
  var _threadfocus = "<%=request.getParameter("threadfocus")%>";
  var _database = "<%=request.getParameter("database")%>";
  var _nitds = "<%=request.getParameter("nidts") %>";
  
  /*Control thread updates or creation*/
  function UpdateThreadControl(canEditThread)
  {
  	var selt = document.getElementById("rename_thread");
  	var self = document.getElementById("find_more_notes");
  	//var selj = document.getElementById("journey_of_thinking");
  	var seld = document.getElementById("delete_thread");
  	var selr = document.getElementById("remove_note");
  	var selh = document.getElementById("hightlight_note");
  	if (canEditThread == "false")
  	{
  		console.log("[DEBUG] User does not has create new thread permissions");
  		
  		// Disable create functionality
  		selt.removeAttribute("href");
  		selt.removeAttribute("onclick");
  		selt.setAttribute("id", "rename_thread");
  		selt.style.color  = "gray";
  		
  		self.removeAttribute("href");
  		self.removeAttribute("onclick");
  		self.setAttribute("id", "find_more_notes");
  		self.style.color  = "gray";
  		
  		//selj.removeAttribute("href");
  		seld.removeAttribute("href");
  		seld.removeAttribute("onclick");
  		seld.setAttribute("id", "delete_thread");
  		seld.style.color  = "gray";
  		
  		if(selr != null)
  		{
  		   selr.removeAttribute("href");
  		   selr.removeAttribute("onclick");
  		   selr.setAttribute("id", "remove_note");
  		 selr.style.color  = "gray";
  		}
  		if(selh != null)
  		{
  		  selh.removeAttribute("href");
  		  selh.removeAttribute("onclick");
  		  selh.setAttribute("id", "highlight_note");
  		  selh.style.color  = "gray";
  		}	
  	}
  }
</script>
<link href="css/drawThreadGraph.css" rel="stylesheet" type="text/css">
<script src="js/thread_update.js"></script>
<script src="js/drawThreadGraph.js"></script>
<script src="js/itm_week_beta.js"></script>
<script src="js/screenLock.js"></script>
<LINK REL=StyleSheet HREF="../css/btn_link.css" TYPE="text/css">
<script src="../js/jquery-1.6.min.js" type="text/javascript"></script>
<script  language="javascript" type="text/javascript">
if("yes"=="<%=request.getParameter("ifexist")%>"){
	alert("the thread already exists! please change the focus");
}

function SumWin()
{
	
	window.open('ThreadSum.jsp?database=<%=database%>&threadfocus=<%=request.getParameter("threadfocus")%>&projectname=<%=request.getParameter("projectname")%>','sumwin'); 
}

function OnFind()
{
	if (document.getElementById("input_threadfocus").value=="")
	{
		window.alert('Please input the focus!' );
		 return false;
	}
	top.window.location="/itmdev/thread_action.jsp?database=<%=request.getParameter("database")%>&projectname=<%=request.getParameter("projectname")%>&threadfocus="+document.getElementById("input_threadfocus").value;
}

function OnFindMore()
{  
	OnSave();
	top.buttomframe2.location="SearchMore.jsp?database=<%=request.getParameter("database")%>&projectname=<%=request.getParameter("projectname")%>&threadfocus=<%=request.getParameter("threadfocus")%>";
	top.document.getElementById("framesetter").rows = "100px,*"
}
</script>
        <script>
            $(document).ready(function(){
                $('#linkSuggest a').click(function(){
                    $('#td_name').css({
                        'overflow-y': 'auto',
                        'height': '620px', 
                        'width': '1024px'
                    });
                    var url=$(this).attr('href');                    
                    $('#td_name').load(url);                    
                    return false;
                });//end click
                
            });
        </script>
</head>

<%
NoteModel noteobj = new NoteModel(database);
NoteController nc = new NoteController(database);

%>
<body onload="render();UpdateThreadControl('<%= canEditThread%>');">
<div id="sticky">
	<div id="header">
	<table border="0">
		<tr>
			<td><img src="img/itmlogo100x100.gif" alt="ITM LOGO" height="60" width="60" /></td>
			<td style="background-color: #01B0F1;color:yellow;font-family:Arial, Helvetica, sans-serif;" height="60" width="870">
			<%if(request.getParameter("ifexist")==null && request.getParameter("threadfocus")==null){%>
			&nbsp;&nbsp;&nbsp;<font size="5">Add an Idea Thread for Project: <%=request.getParameter("projectname")%></font>
			<%}else if(request.getParameter("ifexist")!=null && request.getParameter("threadfocus")!=null){ %>
			&nbsp;&nbsp;&nbsp;<font size="5">Add an Idea Thread for Project: <%=request.getParameter("projectname")%></font>
			<%}else{ %>
			&nbsp;&nbsp;&nbsp;<font size="5">Update Idea Thread: <%=request.getParameter("threadfocus")%></font><br><font size="3">&nbsp;&nbsp;&nbsp;Project: <%=request.getParameter("projectname")%></font>
			<%} %>			</td>
		</tr>
	</table>
	<div id=td_info>
<div id="td_name">
<%if(request.getParameter("ifexist")!= null && request.getParameter("threadfocus")!=null){%>
Enter Thread Name (Focus):
<form>
<input class="fields" name="thread_focus" id="input_threadfocus" type = "text" size = "25" value ="<%=request.getParameter("threadfocus") %>" />
	<a href="#" onclick="OnFind()">OK</a>
</form>
<%}else if(request.getParameter("ifexist")== null && request.getParameter("threadfocus")==null){%>
Enter Thread Name (Focus):
<form>
<input class="fields" name="thread_focus" id="input_threadfocus" type = "text" size = "25" value ="" onkeydown="return new_thread_searchKeyPress(event);"/>
<!--Thread Sugeest        Stan-->
<a href="#" onclick="OnFind()" class="close">OK</a>
        <a href="New_HomePage.jsp?database=${database}" onclick="" class="close">Close</a>    
    </form>
    <h4 id="linkSuggest">
        <a href="../GetViewServlet">Search Suggest Thread Names</a>
    </h4>
<!--        Stan-->
<%}else if(request.getParameter("ifexist")== null &&request.getParameter("threadfocus")!=null){%>


</div><span id="ajax_state"></span>
	<div id="button">
	<ul>
	<li><a href="#" onclick="OnFindMore()" id="find_more_notes">Find More Notes</a></li>
	<li><a href="#" onclick="SumWin()" id="journey_of_thinking">Journey of Thinking</a></li>
	<li><a href="#" onclick="ShowRenameThread()" id="rename_thread">Rename Thread</a></li>
	<li><a href="#" onclick="ShowDeleteThread()" id="delete_thread">Delete Thread</a></li>
	<li><a href="#" onclick="OnClose()">Save and Close</a></li>
	</ul>
	</div>
		<%}%>
</div>
	
</div>
</div>
<div id="wrapperu">
<div id="wrapper">


<!--------------------------------display the individual thread map----------------------------------------->
	    <%
	    String getvalue = request.getParameter("getvalue");
	    System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!   "+getvalue);
	    if((getvalue != null) && !(getvalue.equals("null")) && !(getvalue.equals("@")))
	    {
	    	System.out.println(request.getParameter("getvalue"));
	    	String[] strvalue = request.getParameter("getvalue").split("&");	    	
	    	strid = strvalue[0].split("@");
	    	String strcon = "( note_table.noteid = ";
		   	String threadNoteQueryStr = "thread_note.noteid =";
	    	
	    	int[] intid = new int[1000]; //this limits the number of id is 1000 at most!
	    	int ival;
	    
	    	// Get the id of selected notes 
	    	for(int i=1; i< strid.length; i++) {  // Be careful, the index starts with "1" here!!
				intid[i] =  Integer.valueOf(strid[i]).intValue(); //change the id value from string to int
				if( i < strid.length-1){
					strcon += intid[i]+ " or note_table.noteid = ";	
				}else{
					strcon += intid[i] + ")";
				}				 
				
				// System.out.println("thread.jsp : the selected note's id is "+intid[i]);
			}
			
			for(int i = 1; i< strid.length; i++) {  // Be careful, the index starts with "1" here!!
				if( i < strid.length-1){
					//threadNoteQueryStr += intid[i]+ " and thread_note.noteid = ";	
					threadNoteQueryStr += intid[i]+ " or thread_note.noteid = ";	
				} else {
					threadNoteQueryStr += intid[i] + "";
				}
			
				// System.out.println("thread.jsp : the selected note's id is "+intid[i]);
			}
			
			//get the notes from database
			sqls s = new sqls();
			Operatedb opdb = new Operatedb(s,database);
			
			//using hashtable to store the note's relation
			ResultSet noters;
			String strtemp;
			String strtype=null;
			Hashtable<Integer,Vector<Integer>> butable = new Hashtable<Integer, Vector<Integer>>();
			Hashtable<Integer,Vector<Integer>> antable = new Hashtable<Integer, Vector<Integer>>();
			Hashtable<Integer,Vector<Integer>> retable = new Hashtable<Integer, Vector<Integer>>();
			
			for(int i=1; i<strid.length; i++)
			{
				//query the database				
				//System.out.println("the length is "+strid.length);
				//System.out.println("the i value is "+i);
				//System.out.println("the intid is "+intid[i]);
				strtemp ="fromnoteid = " + intid[i]; 
				
				//System.out.println("the strcon is "+strcon);
				noters = opdb.GetRecordsFromDB("note_note","linktype,tonoteid",strtemp);
				
				//add to hash table
				Vector<Integer> anvec = new Vector<Integer>();
				Vector<Integer> buvec = new Vector<Integer>();
				Vector<Integer> revec = new Vector<Integer>();
				
				while(noters.next()){	
					strtype = noters.getString(1);
					//System.out.println("note: "+intid[i]+"  "+ strtype+" note: "+noters.getInt(2));
					if(strtype.equals("annotates")){
						anvec.add(noters.getInt(2));
					}else if (strtype.equals("buildson")){
						buvec.add(noters.getInt(2));
					}else if (strtype.equals("references")){
						revec.add(noters.getInt(2));
					}
				}
				
				if(noters != null){
					try{
						noters.close();
					}catch(SQLException e){
						e.printStackTrace();
					}
				}
				antable.put(intid[i],anvec);
				butable.put(intid[i],buvec);
				retable.put(intid[i],revec);
				
				//System.out.println("note "+intid[i]+"'s anvec's size is "+ anvec.size());
				//System.out.println("note "+intid[i]+"'s buvec's size is "+ buvec.size());
				//System.out.println("note "+intid[i]+"'s revec's size is "+ revec.size());
			}		
	       Vector<Integer> vec;
		  
	       String[] namearray=new String[20];
	      
	      namearray[0] = "thread_note";
	      String threadFocusName = request.getParameter("threadfocus");
	      strtemp =  "thread_note.threadfocus='" + threadFocusName +"'";
	      
	      // Variables used for query
	      ResultSet  disrs = null;	
	      boolean threadIsPopulated = false;
	      int numberOfTables = 5;
	      String colNames = "note_table.noteid,notetitle,notecontent,createtime,firstname,lastname,view_table.title";
	      
	      disrs = opdb.MulGetRecordsFromDB(namearray,"projectid,threadfocus,noteid,highlight",strtemp, 1);
	      
	      System.out.println("[DEBUG] Query executed for check is: " + strtemp);
	      
	      // Store state of highlight 
	      Hashtable<Integer, Integer> idToHighlightMap = new Hashtable<Integer, Integer>();
	      
		  if(disrs != null && disrs.next())
	      {
			  int rowcount = 0;
			   
			  do
			  {
				idToHighlightMap.put(disrs.getInt(3), disrs.getInt(4));
				++rowcount;				  
			  }while(disrs.next());
			  
			  // Assuming that ids are same, we could do a better job here where we go id by id
			  if (rowcount == (strid.length - 1))
			  {
		  		System.out.println("thread_note is already populated");
		    	threadIsPopulated = true; 
			  }
	      }
		  
		  // Check if incoming ids are contained in the hashtable, if they do, do nothing, if not
		  // then insert them with highlight state being 0 since we don't know anything about them (they are new)
	      for(int i=0; i < intid.length; ++i)
	      {
	    	  if(idToHighlightMap.containsKey(intid[i]))
	    	  {
	    		  // Do nothing since we already know about the state of highlight for this note id
	    	  }
	    	  else
	    	  {
	    		  idToHighlightMap.put(intid[i], 0);
	    	  }
	      }
		  
		  namearray[0] = "note_table"; 
		  namearray[1] = "author_table";
		  namearray[2] = "author_note";
		  namearray[3] = "view_table";
		  namearray[4] = "view_note";
		  
		  //strtemp = strcon + " AND note_table.noteid=author_note.noteid AND author_note.authorid=author_table.authorid AND note_table.noteid=view_note.noteid AND view_note.viewid=view_table.idview";
		  strtemp = strcon + " AND note_table.noteid=author_note.noteid AND author_note.authorid=author_table.authorid AND note_table.noteid=view_note.noteid AND view_note.viewid=view_table.idview";
		  if (threadIsPopulated)
		  {
			 namearray[5] = "thread_note";
			 strtemp += " AND thread_note.noteid = note_table.noteid";
			 colNames += ",thread_note.highlight";
			 numberOfTables = 6;
		  }
		  strtemp += " GROUP BY note_table.noteid";
		  System.out.println("Query executed is: " + strtemp);
		  
		  // Check first if the thread_note alread contains entry entry for this note, if it does, then form a query 
		  // that includes the thread_note or else, just use the query below.
		
		  disrs = opdb.MulGetRecordsFromDB(namearray, colNames, strtemp, numberOfTables);
		  //disrs = noteobj.getNote(sql);
				  
		  %><div id="fm">
		  <div id="threadinfo"></div>
	
	<ul id="flag">
		<li id="showalltitle">&nbsp;|&nbsp;<a href="javascript:void(0)" onclick="toggleTitles(this)";>Show Title</a>&nbsp;|&nbsp;</li>
		<li id="showallauthor"><a href="javascript:void(0)" onclick="toggleAuthors(this)">Show Author</a>&nbsp;|&nbsp;</li>
		<li id="showalllink">
		<a href="javascript:void(0)" onclick="toggleLinks(this, 'buildons');">Show Build-on</a>&nbsp;|&nbsp;
		<a href="javascript:void(0)" onclick="toggleLinks(this, 'references')">Show Reference</a>&nbsp;|&nbsp;
		</li>
	</ul>

</div>
<div id="up_area">
<div id="wrapper_draw">
		  <div id="right_area">
	     <!-- <div id="draw_area" ondblclick="redraw()" onclick="drawnoteclick(event)"></div>
	     <div id="reference_area"></div>
	     <div id="buildon_area"></div>
	     <div id="annotate_area"></div> -->
	     <!--<div id="background_area"></div>
	     
	     <div id="bar_left" onclick="windowmoveleft()"></div>
	     <div id="bar_area">
	     <div id="bar"></div>
	     <div id="pointer" onmousedown="ptdown(event)" onmouseup="ptup(event)"></div>
	     </div>
	     <div id="bar_right" onclick="windowmoveright()"></div>
	     <div id="time_area"></div>-->
	     </div>
	     

</div>
<div id="draw_ctrl">
	<ul id="node_contral">
		<li>Note:&nbsp;&nbsp;</li>
		<li><a href="javascript:void(0)" onclick="remove_note()" id="remove_note">Remove Note</a>&nbsp;|&nbsp; </li>
		<li id="showhighlight"><a href="javascript:void(0)" onclick="showhighlight()" id="hightlight_note">Highlight Note</a>&nbsp;</li>
	</ul>

	<ul id="zoom">
		<li>Zoom:&nbsp;&nbsp;</li>
		<li><a href="javascript:void(0)" onclick="bar(0)">1 Day</a>&nbsp;|&nbsp; </li>
		<li><a href="javascript:void(0)" onclick="bar(1)">1 Week</a>&nbsp;|&nbsp; </li>
		<li><a href="javascript:void(0)" onclick="bar(2)">2 Weeks</a>&nbsp;|&nbsp; </li>
		<li><a href="javascript:void(0)" onclick="bar(3)">1 Month</a>&nbsp;|&nbsp; </li>
		<li><a href="javascript:void(0)" onclick="bar(4)">All</a> </li>
	</ul>
</div>
<div id="wrapper_ln">
<div id="list_area" onclick="selectListItem(event)"></div>
<div id="note_content">
<h3>Select a note to show its content here...</h3>

</div>
</div>
</div>
		<div id="noteid_with_timestamp" value=<%=nidts_str%> ></div>
	    <div id="raw_area">
		<% 	
			System.out.println("[INFO : thread.jsp]******************" +  request.getParameter("nidts"));
		
			while(disrs.next())
			{
				//System.out.println("the note's id is " + disrs.getString(1));
				//System.out.println("the note's date is "+ disrs.getString(4));
				//System.out.println("the note's author is "+ disrs.getString(5)+"  "+disrs.getString(6));
				/*
				vec = retable.get(disrs.getInt(1));
				for(int i=0; i<vec.size();i++){
				    System.out.println("reference1:	from note "+disrs.getInt(1)+" to note " + vec.get(i));
				}
				vec.clear();
				
				vec = butable.get(disrs.getInt(1));
				for(int i=0; i<vec.size();i++){
				    System.out.println("buildson1:	from note "+disrs.getInt(1)+" to note " + vec.get(i));
				}
				vec.clear();
				
				vec = antable.get(disrs.getInt(1));
				for(int i=0; i<vec.size();i++){
				    System.out.println("annotate1:	from note "+disrs.getInt(1)+" to note " + vec.get(i));
				}*/
				
				// Default
				String highlighted = "0";
				highlighted = Integer.toString(idToHighlightMap.get(disrs.getInt(1)));
				
		%>
			<div class="rawnote" highlight="<%=highlighted %>" noteid="<%=disrs.getString(1)%>" view="<%=disrs.getString(7) %>" author="<%List authorList = opdb_author.getMultipleAuthors(disrs.getString(1));
					  for(int i=0;i<authorList.size();i++){
						  String name = (String)authorList.get(i);%>
						  <%=name %>
						  <%if(i < authorList.size()-1){
						  %>
						  ,&nbsp;
						  <%}%>	  
					  <%}%>" firstn="<%=disrs.getString(5)%>"date="<%=disrs.getString(4)%>">
			<!--display the note's relationship -->
			<div class="references">
				<%vec = retable.get(disrs.getInt(1));
				  for(int i=0; i<vec.size();i++){
				    System.out.println("reference:	from note "+disrs.getInt(1)+" to note " + vec.get(i));%>
				  	<div class="reference" target="<%=vec.get(i)%>"> 
					</div>
				<%	  
				  }
				  vec.clear();
				%>				
			</div>
			<div class="buildsons">
				<%vec = butable.get(disrs.getInt(1));
				  for(int i=0; i<vec.size();i++){
				  System.out.println("buildson:	from note "+disrs.getInt(1)+" to note " + vec.get(i));%>
				  	<div class="buildson" target="<%=vec.get(i)%>"> 
					</div>
				<%	  
				  }
				  vec.clear();
				%>	
			</div>
			<div class="annotates">
				<%vec = antable.get(disrs.getInt(1));
				  for(int i=0; i<vec.size();i++){
				  System.out.println("annotates:	from note "+disrs.getInt(1)+" to note " + vec.get(i));%>
				  	<div class="annotate" target="<%=vec.get(i)%>"> 
					</div>
				<%	  
				  }
				  vec.clear();
				%>	
			</div>
			<div class="rawtitle"><%=disrs.getString(2)%></div>
			<%
			/* String content="";
			if(disrs.getString("support")==null)
				content = disrs.getString("notecontent");
			else
				content = nc.getContent(disrs.getString("noteid"));
// 				content = nc.getOffsetContent(disrs.getString("noteid")); */
			%>
 			<div class="rawcontent"> 
				<%= nc.getContent(disrs.getString("noteid")) %> 
			</div> 
		</div>
		<% 
		}
		%>
</div>
<% 
	if(disrs != null){
		try{
			disrs.close();
		}catch(SQLException e){
			e.printStackTrace();
		}
	 }

	if(opdb != null){
		opdb.CloseCon();
	}

}%>

<!-------------------------------end of the if statement--------------------------------------->
</div>
</div>

</body>
</html>