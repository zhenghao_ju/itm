//GLOBAL

var bar_width = 640;
var draw_node_width = 7;
var draw_node_margin = 4;
var draw_node = draw_node_width*2;
var draw_width =640;
var draw_width2 = draw_width-draw_node;
var draw_height = 60;
var title_flag = 0;
var ref_flag = 0;
var bld_flag = 0;
var ant_flag = 0;
var author_flag = 0;
var draw_pointer = 0;
var time_width = 680;
var tline;

function drawMap(){
	cleanUp();
	tline = initTime();
	initTLine(tline);
	initBar();    //TODO display entire area
	initthread(tline);
	bar(5);
}
function initBar(){
	document.getElementById("wrapper").style.visibility="visible";
}

function cleanUp(){
	var root_draw = document.getElementById("draw_area");
	var cnt = root_draw.children.length;
	//clean up
	for (var i = 0;i<cnt;i++){
		root_draw.removeChild(root_draw.children[0]);
	}
	root_draw = document.getElementById("time_right");
	cnt = root_draw.children.length;
	for (var i = 0;i<cnt;i++){
		root_draw.removeChild(root_draw.children[0]);
	}
}
//get start and end time
function initTime(){
	var root_raw = document.getElementById("raw_area");
	var firsttime;
	var lasttime;
	var fchildren = root_raw.children;
	var flength = fchildren.length;
	for (var k=0;k<flength;k++){
		var rchildren = fchildren[k].children;
		var rlength = rchildren.length;
		for (var i=0;i<rlength;i++){
			var currenttime = rchildren[i].getAttribute("date");
			if (firsttime == null) {
				firsttime = currenttime;
			}
			if (lasttime == null) {
				lasttime =currenttime;
			}
			if (compareDate(firsttime,currenttime) == 1){
				firsttime = currenttime;
			}
			if (compareDate(lasttime,currenttime) == -1){
				lasttime = currenttime;
			}
		}
	}
	return [firsttime,lasttime];
}
//init time line
function initTLine(e){
	var root_time = document.getElementById("time_right");
	var split = 10;
	var start = newDT(e[0]);
	var end = newDT(e[1]);
	var starttime = parseDT(e[0]);
	var endtime = parseDT(e[1]);
	var tp = (endtime-starttime)/split;
	var tnode = document.createElement("div");
	tnode.setAttribute("class","timenode");
	tnode.appendChild(document.createTextNode((start.getMonth()+1)+"/"+(start.getDate())+"/"+start.getFullYear()));
	tnode.style.left="0px";
	root_time.appendChild(tnode);
	for (var i = 1; i < split; i++){
		var nd = new Date();
		nd.setTime(0-nd.getTime());
		nd.setTime(start);
		var dateoffset = tp*i+starttime;
		nd.setTime(dateoffset);
		var ttnode = document.createElement("div");
		ttnode.setAttribute("class","timenode");
		ttnode.appendChild(document.createTextNode((nd.getMonth()+1)+"/"+(nd.getDate())));
		ttnode.style.left = bar_width/split*i + 20 + "px";
		root_time.appendChild(ttnode);
	}
	var enode = document.createElement("div");
	enode.setAttribute("class","timenode");
	enode.appendChild(document.createTextNode((end.getMonth()+1)+"/"+(end.getDate())+"/"+end.getFullYear()));
	enode.style.left=time_width + "px";
	root_time.appendChild(enode);
}
// compare two STRING date
function compareDate(str1,str2){
	var date1 = parseDT(str1);
	var date2 = parseDT(str2);
	var re;
	if ((date1 - date2) > 0 ){
		re = 1;
	}
	else if ((date1 - date2) == 0){
		re = 0;
	}
	else if ((date1 - date2) < 0){
		re = -1;
	}
	return re;
}
// parse date & time
function parseDT(str) {
	// NOTE: Since we can have months like 08 or 09
	// javascript will parse think these as octals 
	// and will return 0. We need to define base 10 explicity
	// to make sure to prevent this bevahior.
	var DT = new Date();	
	DT.setFullYear(parseInt(str.substr(0,4), 10));
	DT.setMonth(parseInt(str.substr(5,2), 10)-1);
	DT.setDate(parseInt(str.substr(8,2), 10));
	DT.setSeconds(parseInt(str.substr(17,2), 10));
	DT.setMinutes(parseInt(str.substr(14,2), 10));
	DT.setHours(parseInt(str.substr(11,2), 10));
	DT.setMilliseconds(000);
	return DT.getTime();
}

function newDT(str){
	str = parseDT(str);
	var DT = new Date();
	DT.setTime(0-DT.getTime());
	DT.setTime(str);
	return DT;
}

function initthread(e){
	var root_draw = document.getElementById("draw_area");
	var root_raw = document.getElementById("raw_area");
	for (var i = 0;i< root_raw.children.length;i++){
		//init thread left & right
		var c_raw = root_raw.children[i];
		var c_draw = document.createElement("div");
		root_draw.appendChild(c_draw);
		c_draw.setAttribute("class", "thread");
		c_draw.setAttribute("threadfocus", c_raw.getAttribute("focusname"));
		c_left = document.createElement("div");
		c_draw.appendChild(c_left);
		c_left.setAttribute("class","thread_left");
		c_right = document.createElement("div");
		c_draw.appendChild(c_right);
		c_right.setAttribute("class","thread_right")
		//init thread left
		var tmpspan = document.createElement("span");
		tmpspan.appendChild(document.createTextNode(c_raw.getAttribute("focusname")));
		c_left.appendChild(tmpspan);
		c_left.appendChild(document.createElement("br"));
		c_left.appendChild(document.createTextNode(c_raw.getAttribute("nt_num")+" 篇短文， "+ c_raw.getAttribute("au_num")+" 位作者"));
		c_left.appendChild(document.createElement("br"));
		var ahref = document.createElement("a");
		c_left.appendChild(ahref);
		ahref.setAttribute("href","ThreadSum.jsp?threadfocus="+c_raw.getAttribute("focusname")+"&projectname="+_projectname+"&database="+_database);
		ahref.setAttribute("target","_blank");
		ahref.appendChild(document.createTextNode("进展反思"));
		c_left.appendChild(document.createTextNode(" | "));
		ahref = document.createElement("a");
		c_left.appendChild(document.createTextNode(" "));
		c_left.appendChild(ahref);
		ahref.setAttribute("href","threadLoad.jsp?database="+_database+"&threadfocus="+c_raw.getAttribute("focusname")+"&projectname="+_projectname);
		ahref.setAttribute("target","_blank");
		ahref.appendChild(document.createTextNode("查看脉络"));
		//init right area
		drawright(c_raw,c_right);					
	}
}

function drawright(c_raw,c_right){
	//init link area
	var tmpdiv = document.createElement("div");
	c_right.appendChild(tmpdiv);
	tmpdiv.setAttribute("class","reference_area");
	tmpdiv = document.createElement("div");
	c_right.appendChild(tmpdiv);
	tmpdiv.setAttribute("class","buildon_area");
	tmpdiv = document.createElement("div");
	c_right.appendChild(tmpdiv);
	tmpdiv.setAttribute("class","annotate_area");
	
	
	//init background;
	var threadbg = document.createElement("div");
	threadbg.setAttribute("class","threadbg");
	c_right.appendChild(threadbg);
	var threadnotearea = document.createElement("div");
	threadnotearea.setAttribute("class","threadnote_area");
	c_right.appendChild(threadnotearea);
	var thread_TL = initthreadTLine(c_raw);
	    //tmp solution
	if (thread_TL[0]==null && thread_TL[1]==null){
		return;
	}
	
	var all_tl =tline;
	var pp=document.getElementById("pointer");
	var ll;
	if (isNaN(parseInt(pp.style.left))) {ll = 0;}
	else {ll = parseFloat(pp.style.left);}
	var rr;
	if (isNaN(parseInt(pp.style.left))) {rr = bar_width;}
	else rr=ll+parseFloat(pp.style.width);
	ll = ll/bar_width;
	rr = rr/bar_width;
	var tl_start = parseDT(all_tl[0]);
	var tl_end = parseDT(all_tl[1]);
	var tl_start2 = Math.floor(ll*(tl_end-tl_start))+tl_start;
	var tl_end2 = Math.floor(rr*(tl_end-tl_start))+tl_start;
	var td_start = parseDT(thread_TL[0]);
	var td_end = parseDT(thread_TL[1]);
	if (tl_start == tl_end){
		threadbg.style.left = draw_width2/2+30+"px";
		threadbg.style.width = draw_node+"px";
	}
	else if (td_start < tl_start2 && td_end < tl_end2){
		threadbg.style.left = "30px";
		threadbg.style.width = Math.floor((td_end - tl_start2)/(tl_end2 - tl_start2)*draw_width2)+20+"px";
		//threadbg.style.width = draw_width + "px";
	}
	else if (td_start >= tl_start2 && td_end < tl_end2){
		threadbg.style.left = Math.floor((td_start-tl_start2)/(tl_end2-tl_start2)*draw_width2)+30+"px";
		threadbg.style.width = Math.floor((td_end-td_start)/(tl_end2-tl_start2)*draw_width2)+draw_node+"px";
	}
	else if (td_start < tl_start2 && td_end >= tl_end2){
		threadbg.style.left = "30px";
		threadbg.style.width = draw_width2+draw_node+"px";
	}
	else if (td_start >= tl_start2 && td_end >=tl_end2){
		threadbg.style.left = Math.floor((td_start-tl_start2)/(tl_end2-tl_start2)*draw_width2)+30+"px";
		threadbg.style.width = Math.floor((tl_end2-td_start)/(tl_end2-tl_start2)*draw_width2)+draw_node+"px";
	}
	else alert("something is wrong");
	// draw notes
	var ch = draw_height/2;
	var craw = c_raw.children;
	var yyoffset = 0;
	var clraw = craw.length;
	if (tl_start == tl_end){
		var rnode = craw[0];
		var title = rnode.children[3].firstChild.nodeValue;
		var nnode = document.createElement("div");
		nnode.setAttribute("drawnoteid",rnode.getAttribute("noteid"));
		nnode.setAttribute("class","drawnote");
		nnode.setAttribute("title",title);
		nnode.setAttribute("author",rnode.getAttribute("author"));
		threadnotearea.appendChild(nnode);
		var xoffset = draw_width2/2+"px";
		var yoffset;
		if (yyoffset%2 == 0) yoffset = ch + yyoffset/2*draw_node + "px";
		else if(yyoffset%2 == 1) yoffset = ch - ((yyoffset+1)/2)*draw_node + "px";
		if (parseInt(yoffset) < 0 || (parseInt(yoffset)+draw_node) > draw_height){
			yoffset = ch + "px";
			yyoffset = 0;
		}
		nnode.style.left = xoffset;
		nnode.style.top = yoffset;
		yyoffset+=1;
		if(yyoffset >= draw_height/draw_node) yyoffset = 0;
	}
	else{
		for (var i = 0; i < clraw; i++){
			var rnode = craw[i];
			var ctime = parseDT(rnode.getAttribute("date"));
			var pt = (ctime-tl_start)/(tl_end-tl_start);
			if (pt <= rr && pt >= ll){
				var title = rnode.children[3].firstChild.nodeValue;
				var nnode = document.createElement("div");
				nnode.setAttribute("drawnoteid",rnode.getAttribute("noteid"));
				nnode.setAttribute("class","drawnote");
				nnode.setAttribute("title",title);
				nnode.setAttribute("author",rnode.getAttribute("author"));
				threadnotearea.appendChild(nnode);
				var xoffset = Math.floor(((ctime-tl_start)/(tl_end-tl_start)-ll)/(rr-ll)*draw_width2) + "px";
				var yoffset;
				if (yyoffset%2 == 0) yoffset = ch + yyoffset/2*draw_node + "px";
				else if(yyoffset%2 == 1) yoffset = ch - ((yyoffset+1)/2)*draw_node + "px";
				if (parseInt(yoffset) < 0 || (parseInt(yoffset)+draw_node) > draw_height){
					yoffset = ch + "px";
					yyoffset = 0;
				}
				nnode.style.left = xoffset;
				nnode.style.top = yoffset;
				yyoffset+=1;
				if(yyoffset >= draw_height/draw_node) yyoffset = 0;
			}
		}
	}
}
function windowmoveleft(){
	var pp = document.getElementById("pointer");
	if (isNaN(parseInt(pp.style.left))) {pp.style.left = "0px";}
	var ll = parseInt(pp.style.left);
	pp.style.left = ll - parseInt(pp.style.width) + "px";
	edgecheck();
	redraw();
}
function windowmoveright(){
	var pp = document.getElementById("pointer");
	if (isNaN(parseInt(pp.style.left))) {pp.style.left = "0px";}
	var ll = parseInt(pp.style.left);
	pp.style.left = ll + parseInt(pp.style.width) + "px";
	edgecheck();
	redraw();
}
function redraw(){
	var root_raw = document.getElementById("raw_area");
	var root_draw = document.getElementById("draw_area");
	for (var i = 0; i< root_draw.children.length;i++){
		var cnode = root_draw.children[i];
		for (var k=0; k< cnode.children.length;k++){
			if (cnode.children[k].getAttribute("class")=="thread_right"){
				cnode = cnode.children[k];
				while(cnode.children.length != 0){
					cnode.removeChild(cnode.children[0]);
				}
				break;
			}
		}		
		drawright(root_raw.children[i],cnode);
	}
	checkflag();
}

function initthreadTLine(e){
	var firsttime=null;
	var lasttime=null;
	var rchildren = e.children;
	var rlength = e.children.length;
	for (var i = 0; i < rlength; i++)
	{
		var currenttime = rchildren[i].getAttribute("date");
		if (firsttime == null) {
			firsttime = currenttime;
		}
		if (lasttime == null) {
			lasttime =currenttime;
		}
		if (compareDate(firsttime,currenttime) == 1){
			firsttime = currenttime;
		}
		if (compareDate(lasttime,currenttime) == -1){
			lasttime = currenttime;
		}
	}
	return [firsttime,lasttime];
}
function bar(e){
	var minutes=1000*60;
	var hours=minutes*60;
	var days=hours*24;
	var week=days*7;
	var month=days*30;
	var tm = initTime();
	var all = parseDT(tm[1])-parseDT(tm[0]);
	e = bardecide(e,all);
	var bs;
	var pp = document.getElementById("pointer");
	switch(e){
		case 0:
			bs = Math.floor(days*2/all*bar_width);
			pp.style.width = bs+"px";
			break;
		case 1:
			bs = Math.floor(week/all*bar_width);
			pp.style.width = bs+"px";
			break;
		case 2:
			bs = Math.floor(week*2/all*bar_width);
			pp.style.width = bs+"px";
			break;
		case 3:
			bs = Math.floor(month/all*bar_width);
			pp.style.width = bs+"px";
			break;
		case 4:
			bs = Math.floor(month*3/all*bar_width);
			pp.style.width = bs+"px";
			break;
		case 5:
			bs = Math.floor(days/all*bar_width);
			pp.style.width = bar_width+"px";
			break;

	}
	edgecheck();
	_blockwidth = bs;
	redraw();
}
function edgecheck(){
	var pp=document.getElementById("pointer");
	if (isNaN(parseInt(pp.style.left))) {pp.style.left = "0px";}
	var ll = parseInt(pp.style.left);
	var ww = parseInt(pp.style.width);
	var rr = ww+ll;
	if (rr > bar_width){
		pp.style.left = bar_width-parseInt(pp.style.width)+"px";
	}
	if (ll < 0){
		pp.style.left = "0px";
	}
}
function bardecide(e,all){
	var minutes=1000*60;
	var hours=minutes*60;
	var days=hours*24;
	var week=days*7;
	var month=days*30;
	var result;
	switch (e){
		case 0: 	
			if (all<days*2) result = 5;
			else result = e;
			break;
		case 1:
			if (all<week) result = 5;
			else result = e;
			break;
		case 2:
			if (all<week*2) result = 5;
			else result = e;
			break;
		case 3:
			if (all<month) result = 5;
			else result = e;
			break;
		case 4:
			if (all<month*3) result = 5;
			else result = e;
			break;
		case 5:
			result = 5;

	}
	return result
}
////////////////////////////////////////////////
//
//    LINKS
//
////////////////////////////////////////////////
//links TODO

function checkflag(){
	//clean up links
	var root_draw = document.getElementById("draw_area");
	for (var i=0;i<root_draw.children.length;i++){
		var cnode = root_draw.children[i].children[1];
		//reference_area
		var tnode = cnode.children[0];
		while(tnode.children.length != 0){
			tnode.removeChild(tnode.children[0]);
		}
		//buildon_area
		tnode = cnode.children[1];
		while(tnode.children.length != 0){
			tnode.removeChild(tnode.children[0]);
		}
		//annotate_area
		tnode = cnode.children[2];
		while(tnode.children.length != 0){
			tnode.removeChild(tnode.children[0]);
		}
	}
	
	if (title_flag == 1){
		var cnode;
		for (var i=0; i<root_draw.children.length;i++){
			cnode = root_draw.children[i].children[1].children[4];
			for (var k=0;k<cnode.children.length;k++){
				dnode = cnode.children[k];
				var dv = document.createElement("div");
				dv.setAttribute("class","drawtitle");
				dnode.appendChild(dv);
				dv.appendChild(document.createTextNode(dnode.getAttribute("title")));
			}
		}
	}
	if (author_flag == 1){
		var cnode;
		for (var i=0; i<root_draw.children.length;i++){
			cnode = root_draw.children[i].children[1].children[4];
			for (var k=0;k<cnode.children.length;k++){
				dnode = cnode.children[k];
				var dv = document.createElement("div");
				dv.setAttribute("class","drawauthor");
				dnode.appendChild(dv);
				dv.appendChild(document.createTextNode(dnode.getAttribute("author")));
			}
		}
	}	
	if (ref_flag == 1){
		var cnode;
		for (var i = 0; i < root_draw.children.length;i++){
			cnode = root_draw.children[i].children[1].children[0];
			var r_draw = root_draw.children[i].children[1].children[4];
			for (var k=0;k<r_draw.children.length;k++){
				drawsinglelink_ref(r_draw.children[k].getAttribute("drawnoteid"),cnode)
			}
		}
	}
	if (bld_flag == 1){
		var cnode;
		for (var i = 0; i < root_draw.children.length;i++){
			cnode = root_draw.children[i].children[1].children[1];
			var r_draw = root_draw.children[i].children[1].children[4];
			for (var k=0;k<r_draw.children.length;k++){
				drawsinglelink_bld(r_draw.children[k].getAttribute("drawnoteid"),cnode)
			}
		}
	}
	if (ant_flag == 1){
		var cnode;
		for (var i = 0; i < root_draw.children.length;i++){
			cnode = root_draw.children[i].children[1].children[2];
			var r_draw = root_draw.children[i].children[1].children[4];
			for (var k=0;k<r_draw.children.length;k++){
				drawsinglelink_ant(r_draw.children[k].getAttribute("drawnoteid"),cnode)
			}
		}
	}
}
function drawsinglelink_ant(id,r_draw){
	//input "noteid"
	var root_raw = document.getElementById("raw_area");
	for (var i=0;i< root_raw.children.length;i++){
		if (root_raw.children[i].getAttribute("focusname") == r_draw.parentNode.parentNode.getAttribute("threadfocus")){
			root_raw = root_raw.children[i];
			break;
		}
	}
	//fint current note
	for (var i = 0; i < root_raw.children.length; i++){
		if (root_raw.children[i].getAttribute("noteid") == id){
			cnt = i;
			break;
		}
	}
	//find referrences
	root_raw = root_raw.children[cnt];
	cnt = root_raw.children.length;
	for (var i = 0; i < cnt; i++){
		if (root_raw.children[i].getAttribute("class") == "annotates"){
			cnt = i;
			break;
		}
	}
	root_raw = root_raw.children[cnt];
	//find single reference
	var tnodes = root_raw.children;
	//check reference node if on draw_area
	cnt = tnodes.length;
	for (var i = 0; i < cnt; i++){
		var tnode = tnodes[i].getAttribute("target");
		if (checkn2d(tnode,r_draw) == 1){
			//target in draw_area
			drawlink(id,tnode,"ant",r_draw);
		}
	}
}
function drawsinglelink_bld(id,r_draw){
	//input "noteid"
	var root_raw = document.getElementById("raw_area");
	for (var i=0;i< root_raw.children.length;i++){
		if (root_raw.children[i].getAttribute("focusname") == r_draw.parentNode.parentNode.getAttribute("threadfocus")){
			root_raw = root_raw.children[i];
			break;
		}
	}
	//fint current note
	for (var i = 0; i < root_raw.children.length; i++){
		if (root_raw.children[i].getAttribute("noteid") == id){
			cnt = i;
			break;
		}
	}
	//find referrences
	root_raw = root_raw.children[cnt];
	cnt = root_raw.children.length;
	for (var i = 0; i < cnt; i++){
		if (root_raw.children[i].getAttribute("class") == "buildsons"){
			cnt = i;
			break;
		}
	}
	root_raw = root_raw.children[cnt];
	//find single reference
	var tnodes = root_raw.children;
	//check reference node if on draw_area
	cnt = tnodes.length;
	for (var i = 0; i < cnt; i++){
		var tnode = tnodes[i].getAttribute("target");
		if (checkn2d(tnode,r_draw) == 1){
			//target in draw_area
			drawlink(id,tnode,"bld",r_draw);
		}
	}
}
function drawsinglelink_ref(id,r_draw){
	//input "noteid"
	var root_raw = document.getElementById("raw_area");
	for (var i=0;i< root_raw.children.length;i++){
		if (root_raw.children[i].getAttribute("focusname") == r_draw.parentNode.parentNode.getAttribute("threadfocus")){
			root_raw = root_raw.children[i];
			break;
		}
	}
	//fint current note
	for (var i = 0; i < root_raw.children.length; i++){
		if (root_raw.children[i].getAttribute("noteid") == id){
			cnt = i;
			break;
		}
	}
	//find referrences
	root_raw = root_raw.children[cnt];
	cnt = root_raw.children.length;
	for (var i = 0; i < cnt; i++){
		if (root_raw.children[i].getAttribute("class") == "references"){
			cnt = i;
			break;
		}
	}
	root_raw = root_raw.children[cnt];
	//find single reference
	var tnodes = root_raw.children;
	//check reference node if on draw_area
	cnt = tnodes.length;
	for (var i = 0; i < cnt; i++){
		var tnode = tnodes[i].getAttribute("target");
		if (checkn2d(tnode,r_draw) == 1){
			//target in draw_area
			drawlink(id,tnode,"ref",r_draw);
		}
	}
}
function drawlink(id,tnode,type,r_draw){
	var root_draw = r_draw.parentNode.children[4];
	var cnt = root_draw.children.length;
	var srcnode = 0;
	var tgtnode = 0;
	for (var i = 0; i < cnt; i++){
		if ( root_draw.children[i].getAttribute("drawnoteid") == tnode){
			srcnode = root_draw.children[i];
		}
		if ( root_draw.children[i].getAttribute("drawnoteid") == id){
			tgtnode = root_draw.children[i];
		}
		if ( srcnode != 0 && tgtnode != 0 ) break;
	}
	//init none 0px var
	var srcnodex,srcnodey,tgtnodex,tgtnodey;
	if (isNaN(parseInt(srcnode.style.left))) {srcnode.style.left = "0px";}
	if (isNaN(parseInt(srcnode.style.top))) {srcnode.style.top = "0px";}
	if (isNaN(parseInt(tgtnode.style.left))) {tgtnode.style.left = "0px";}
	if (isNaN(parseInt(tgtnode.style.top))) {tgtnode.style.top = "0px";}
	//input src_xy,tgt_xy
	srcnodex = parseInt(srcnode.style.left);
	srcnodey = parseInt(srcnode.style.top);
	tgtnodex = parseInt(tgtnode.style.left);
	tgtnodey = parseInt(tgtnode.style.top);
	switch (type){
		case "ref":
			drawlink_ref([srcnodex,srcnodey],[tgtnodex,tgtnodey],r_draw);
			break;
		case "bld":
			drawlink_bld([srcnodex,srcnodey],[tgtnodex,tgtnodey],r_draw);
			break;
		case "ant":
			drawlink_ant([srcnodex,srcnodey],[tgtnodex,tgtnodey],r_draw);
			break;
		default:
			alert("none available link_area" + type);
	}
}

function drawlink_ant(srcnode,tgtnode,r_draw){
	var layer = r_draw;
	if (srcnode[1] == tgtnode[1]){
		//on same y level, straight line
		if (((tgtnode[0] - srcnode[0])>draw_node_width)&&((tgtnode[0]-srcnode[0])<=(draw_node_width+draw_node_margin+2))){
			var line = document.createElement("div");
			line.setAttribute("class","ant_line");
			layer.appendChild(line);
			line.style.width = tgtnode[0]-(srcnode[0]+draw_node_width)+"px";
			line.style.height = "1px";
			line.style.top = srcnode[1]+7+draw_node_margin+"px";
			line.style.left = srcnode[0]+draw_node_width+draw_node_margin+"px";
		}
		//on same y level, need top space
		else if ((tgtnode[0] - srcnode[0])>(draw_node_width+draw_node_margin)){
			//end to srcnode '-'
			var line = document.createElement("div");
			line.setAttribute("class","ant_line");
			layer.appendChild(line);
			line.style.width = "6px";
			line.style.height = "1px";
			line.style.top = srcnode[1]+7+draw_node_margin+"px";
			line.style.left = srcnode[0]+draw_node_width+draw_node_margin+"px";
			//I to end "I to -"
			line = document.createElement("div");
			line.setAttribute("class","ant_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = "10px";
			line.style.top = srcnode[1]-2+draw_node_margin+"px";
			line.style.left = srcnode[0]+draw_node_width+6+draw_node_margin+"px";
			//long line "I-I"
			line = document.createElement("div");
			line.setAttribute("class","ant_line");
			layer.appendChild(line);
			line.style.width = tgtnode[0]-srcnode[0]-draw_node_width-4-1-1-3+"px";
			line.style.height = "1px";
			line.style.top = srcnode[1]-2+draw_node_margin+"px";
			line.style.left = srcnode[0]+draw_node_width+6+1+draw_node_margin+"px";
			//I to start "I to -"
			line = document.createElement("div");
			line.setAttribute("class","ant_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = "10px";
			line.style.top = srcnode[1]-2+draw_node_margin+"px";
			line.style.left = tgtnode[0]-1-1+draw_node_margin+"px";
			//start to tgtnode
			line = document.createElement("div");
			line.setAttribute("class","ant_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = "1px";
			line.style.top = tgtnode[1]+7+draw_node_margin+"px";
			line.style.left = tgtnode[0]-1+draw_node_margin+"px";
		}
	}
	if (srcnode[1] < tgtnode[1]){
		//2 shape
		if (tgtnode[0]-srcnode[0]< draw_node_width*2){
			//end to srcnode
			var line = document.createElement("div");
			line.setAttribute("class","ant_line");
			layer.appendChild(line);
			line.style.width = "6px";
			line.style.height = "1px";
			line.style.top = srcnode[1]+7+draw_node_margin+"px";
			line.style.left = srcnode[0]+draw_node_width+draw_node_margin+"px";
			//I to end
			line = document.createElement("div");
			line.setAttribute("class","ant_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = (tgtnode[1]-1)-(srcnode[1]+7)+"px";
			line.style.top = srcnode[1]+7+draw_node_margin+"px";
			line.style.left = srcnode[0]+6+draw_node_width+draw_node_margin+"px";
			//long line "I-I"
			line = document.createElement("div");
			line.setAttribute("class","ant_line");
			layer.appendChild(line);
			line.style.width = (srcnode[0]+draw_node_width+6)-(tgtnode[0]-1)+"px";
			line.style.height = "1px";
			line.style.top = tgtnode[1]-2+draw_node_margin+"px";
			line.style.left = tgtnode[0]-1+draw_node_margin+"px";
			//I to start "I to -"
			line = document.createElement("div");
			line.setAttribute("class","ant_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = "10px";
			line.style.top = tgtnode[1]-2+draw_node_margin+"px";
			line.style.left = tgtnode[0]-1-1+draw_node_margin+"px";
			//start to tgtnode
			line = document.createElement("div");
			line.setAttribute("class","ant_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = "1px";
			line.style.top = tgtnode[1]+7+draw_node_margin+"px";
			line.style.left = tgtnode[0]-1+draw_node_margin+"px";
		}
		//straight down
		else if (tgtnode[0]-srcnode[0] == draw_node_width*2){
			//end to srcnode
			var line = document.createElement("div");
			line.setAttribute("class","ant_line");
			layer.appendChild(line);
			line.style.width = "6px";
			line.style.height = "1px";
			line.style.top = srcnode[1]+7+draw_node_margin+"px";
			line.style.left = srcnode[0]+draw_node_width+draw_node_margin+"px";
			//I to end
			line = document.createElement("div");
			line.setAttribute("class","ant_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = (tgtnode[1]+8)-(srcnode[1]+7)+"px";
			line.style.top = srcnode[1]+7+draw_node_margin+"px";
			line.style.left = srcnode[0]+6+draw_node_width+draw_node_margin+"px";
			//start to tgtnode
			line = document.createElement("div");
			line.setAttribute("class","ant_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = "1px";
			line.style.top = tgtnode[1]+7+draw_node_margin+"px";
			line.style.left = tgtnode[0]-1+draw_node_margin+"px";
		}
		//z shape
		else if (tgtnode[0]-srcnode[0] > draw_node_width*2){
			//end to srcnode
			var line = document.createElement("div");
			line.setAttribute("class","ant_line");
			layer.appendChild(line);
			line.style.width = "6px";
			line.style.height = "1px";
			line.style.top = srcnode[1]+7+draw_node_margin+"px";
			line.style.left = srcnode[0]+draw_node_width+draw_node_margin+"px";
			//I to end
			line = document.createElement("div");
			line.setAttribute("class","ant_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = (tgtnode[1]-1)-(srcnode[1]+7)+"px";
			line.style.top = srcnode[1]+7+draw_node_margin+"px";
			line.style.left = srcnode[0]+6+draw_node_width+draw_node_margin+"px";
			//long line "I-I"
			line = document.createElement("div");
			line.setAttribute("class","ant_line");
			layer.appendChild(line);
			line.style.width = (tgtnode[0]-1-1)-(srcnode[0]+6+1)-draw_node_width+"px";
			line.style.height = "1px";
			line.style.top = tgtnode[1]-2+draw_node_margin+"px";
			line.style.left = srcnode[0]+draw_node_width+6+1+draw_node_margin+"px";
			//I to start "I to -"
			line = document.createElement("div");
			line.setAttribute("class","ant_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = "10px";
			line.style.top = tgtnode[1]-2+draw_node_margin+"px";
			line.style.left = tgtnode[0]-1-1+draw_node_margin+"px";
			//start to tgtnode
			line = document.createElement("div");
			line.setAttribute("class","ant_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = "1px";
			line.style.top = tgtnode[1]+7+draw_node_margin+"px";
			line.style.left = tgtnode[0]-1+draw_node_margin+"px";
		}
	}
	if (srcnode[1] > tgtnode[1]){
		//s shape
		if (tgtnode[0]-srcnode[0]< draw_node_width*2){
			//end to srcnode
			var line = document.createElement("div");
			line.setAttribute("class","ant_line");
			layer.appendChild(line);
			line.style.width = "6px";
			line.style.height = "1px";
			line.style.top = srcnode[1]+7+draw_node_margin+"px";
			line.style.left = srcnode[0]+draw_node_width+draw_node_margin+"px";
			//I to end
			line = document.createElement("div");
			line.setAttribute("class","ant_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = (srcnode[1]+7+1)-(tgtnode[1]+draw_node_width+6)+"px";
			line.style.top = tgtnode[1]+draw_node_width+6+draw_node_margin+"px";
			line.style.left = srcnode[0]+6+draw_node_width+draw_node_margin+"px";
			//long line "I-I"
			line = document.createElement("div");
			line.setAttribute("class","ant_line");
			layer.appendChild(line);
			line.style.width = (srcnode[0]+draw_node_width+6)-(tgtnode[0]-1)+"px";
			line.style.height = "1px";
			line.style.top = tgtnode[1]+draw_node_width+6+draw_node_margin+"px";
			line.style.left = tgtnode[0]-1+draw_node_margin+"px";
			//I to start "I to -"
			line = document.createElement("div");
			line.setAttribute("class","ant_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = "10px";
			line.style.top = tgtnode[1]+7+draw_node_margin+"px";
			line.style.left = tgtnode[0]-1-1+draw_node_margin+"px";
			//start to tgtnode
			line = document.createElement("div");
			line.setAttribute("class","ant_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = "1px";
			line.style.top = tgtnode[1]+7+draw_node_margin+"px";
			line.style.left = tgtnode[0]-1+draw_node_margin+"px";
		}
		//straight down
		else if (tgtnode[0]-srcnode[0] == draw_node_width*2){
			//end to srcnode
			var line = document.createElement("div");
			line.setAttribute("class","ant_line");
			layer.appendChild(line);
			line.style.width = "6px";
			line.style.height = "1px";
			line.style.top = srcnode[1]+6+draw_node_margin+"px";
			line.style.left = srcnode[0]+draw_node_width+draw_node_margin+"px";
			//I to end
			line = document.createElement("div");
			line.setAttribute("class","ant_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = (srcnode[1]+7)-(tgtnode[1]+6)+"px";
			line.style.top = tgtnode[1]+6+draw_node_margin+"px";
			line.style.left = srcnode[0]+6+draw_node_width+draw_node_margin+"px";
			//start to tgtnode
			line = document.createElement("div");
			line.setAttribute("class","ant_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = "1px";
			line.style.top = tgtnode[1]+6+draw_node_margin+"px";
			line.style.left = tgtnode[0]-1+draw_node_margin+"px";
		}
		//reversed 3 shape
		else if (tgtnode[0]-srcnode[0] > draw_node_width*2){
			//end to srcnode
			var line = document.createElement("div");
			line.setAttribute("class","ant_line");
			layer.appendChild(line);
			line.style.width = "6px";
			line.style.height = "1px";
			line.style.top = srcnode[1]+7+draw_node_margin+"px";
			line.style.left = srcnode[0]+draw_node_width+draw_node_margin+"px";
			//I to end
			line = document.createElement("div");
			line.setAttribute("class","ant_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = (srcnode[1]+8)-(tgtnode[1]+draw_node_width+6)+"px";
			line.style.top = tgtnode[1]+draw_node_width+6+draw_node_margin+"px";
			line.style.left = srcnode[0]+6+draw_node_width+draw_node_margin+"px";
			//long line "I-I"
			line = document.createElement("div");
			line.setAttribute("class","ant_line");
			layer.appendChild(line);
			line.style.width = (tgtnode[0]-3-1)-(srcnode[0]+4+1)-draw_node_width+"px";
			line.style.height = "1px";
			line.style.top = tgtnode[1]+draw_node_width+6+draw_node_margin+"px";
			line.style.left = srcnode[0]+6+1+draw_node_width+draw_node_margin+"px";
			//I to start "I to -"
			line = document.createElement("div");
			line.setAttribute("class","ant_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = "10px";
			line.style.top = tgtnode[1]+7+draw_node_margin+"px";
			line.style.left = tgtnode[0]-1-1+draw_node_margin+"px";
			//start to tgtnode
			line = document.createElement("div");
			line.setAttribute("class","ant_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = "1px";
			line.style.top = tgtnode[1]+7+draw_node_margin+"px";
			line.style.left = tgtnode[0]-1+draw_node_margin+"px";
		}
	}
}
function drawlink_bld(srcnode,tgtnode,r_draw){
	var layer = r_draw;
	if (srcnode[1] == tgtnode[1]){
		//on same y level, straight line
		if (((tgtnode[0] - srcnode[0])>draw_node_width)&&((tgtnode[0]-srcnode[0])<=(draw_node_width+draw_node_margin+2))){
			var line = document.createElement("div");
			line.setAttribute("class","bld_line");
			layer.appendChild(line);
			line.style.width = tgtnode[0]-(srcnode[0]+draw_node_width)+"px";
			line.style.height = "1px";
			line.style.top = srcnode[1]+3+draw_node_margin+"px";
			line.style.left = srcnode[0]+draw_node_width+draw_node_margin+"px";
		}
		//on same y level, need top space
		else if ((tgtnode[0] - srcnode[0])>(draw_node_width+draw_node_margin)){
			//end to srcnode '-'
			var line = document.createElement("div");
			line.setAttribute("class","bld_line");
			layer.appendChild(line);
			line.style.width = "3px";
			line.style.height = "1px";
			line.style.top = srcnode[1]+3+draw_node_margin+"px";
			line.style.left = srcnode[0]+draw_node_width+draw_node_margin+"px";
			//I to end "I to -"
			line = document.createElement("div");
			line.setAttribute("class","bld_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = "8px";
			line.style.top = srcnode[1]-4+draw_node_margin+"px";
			line.style.left = srcnode[0]+draw_node_width+3+draw_node_margin+"px";
			//long line "I-I"
			line = document.createElement("div");
			line.setAttribute("class","bld_line");
			layer.appendChild(line);
			line.style.width = tgtnode[0]-srcnode[0]-draw_node_width-3-1-1-3+"px";
			line.style.height = "1px";
			line.style.top = srcnode[1]-4+draw_node_margin+"px";
			line.style.left = srcnode[0]+draw_node_width+3+1+draw_node_margin+"px";
			//I to start "I to -"
			line = document.createElement("div");
			line.setAttribute("class","bld_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = "8px";
			line.style.top = srcnode[1]-4+draw_node_margin+"px";
			line.style.left = tgtnode[0]-3-1+draw_node_margin+"px";
			//start to tgtnode
			line = document.createElement("div");
			line.setAttribute("class","bld_line");
			layer.appendChild(line);
			line.style.width = "3px";
			line.style.height = "1px";
			line.style.top = tgtnode[1]+3+draw_node_margin+"px";
			line.style.left = tgtnode[0]-3+draw_node_margin+"px";
		}
	}
	if (srcnode[1] < tgtnode[1]){
		//2 shape
		if (tgtnode[0]-srcnode[0]< draw_node_width*2){
			//end to srcnode
			var line = document.createElement("div");
			line.setAttribute("class","bld_line");
			layer.appendChild(line);
			line.style.width = "3px";
			line.style.height = "1px";
			line.style.top = srcnode[1]+3+draw_node_margin+"px";
			line.style.left = srcnode[0]+draw_node_width+draw_node_margin+"px";
			//I to end
			line = document.createElement("div");
			line.setAttribute("class","bld_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = (tgtnode[1]-3)-(srcnode[1]+3)+"px";
			line.style.top = srcnode[1]+3+draw_node_margin+"px";
			line.style.left = srcnode[0]+3+draw_node_width+draw_node_margin+"px";
			//long line "I-I"
			line = document.createElement("div");
			line.setAttribute("class","bld_line");
			layer.appendChild(line);
			line.style.width = (srcnode[0]+draw_node_width+3)-(tgtnode[0]-3)+"px";
			line.style.height = "1px";
			line.style.top = tgtnode[1]-4+draw_node_margin+"px";
			line.style.left = tgtnode[0]-3+draw_node_margin+"px";
			//I to start "I to -"
			line = document.createElement("div");
			line.setAttribute("class","bld_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = "8px";
			line.style.top = tgtnode[1]-4+draw_node_margin+"px";
			line.style.left = tgtnode[0]-3-1+draw_node_margin+"px";
			//start to tgtnode
			line = document.createElement("div");
			line.setAttribute("class","bld_line");
			layer.appendChild(line);
			line.style.width = "3px";
			line.style.height = "1px";
			line.style.top = tgtnode[1]+3+draw_node_margin+"px";
			line.style.left = tgtnode[0]-3+draw_node_margin+"px";
		}
		//straight down
		else if (tgtnode[0]-srcnode[0] == draw_node_width*2){
			//end to srcnode
			var line = document.createElement("div");
			line.setAttribute("class","bld_line");
			layer.appendChild(line);
			line.style.width = "3px";
			line.style.height = "1px";
			line.style.top = srcnode[1]+3+draw_node_margin+"px";
			line.style.left = srcnode[0]+draw_node_width+draw_node_margin+"px";
			//I to end
			line = document.createElement("div");
			line.setAttribute("class","bld_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = (tgtnode[1]+4)-(srcnode[1]+3)+"px";
			line.style.top = srcnode[1]+3+draw_node_margin+"px";
			line.style.left = srcnode[0]+3+draw_node_width+draw_node_margin+"px";
			//start to tgtnode
			line = document.createElement("div");
			line.setAttribute("class","bld_line");
			layer.appendChild(line);
			line.style.width = "3px";
			line.style.height = "1px";
			line.style.top = tgtnode[1]+3+draw_node_margin+"px";
			line.style.left = tgtnode[0]-3+draw_node_margin+"px";
		}
		//z shape
		else if (tgtnode[0]-srcnode[0] > draw_node_width*2){
			//end to srcnode
			var line = document.createElement("div");
			line.setAttribute("class","bld_line");
			layer.appendChild(line);
			line.style.width = "3px";
			line.style.height = "1px";
			line.style.top = srcnode[1]+3+draw_node_margin+"px";
			line.style.left = srcnode[0]+draw_node_width+draw_node_margin+"px";
			//I to end
			line = document.createElement("div");
			line.setAttribute("class","bld_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = (tgtnode[1]-3)-(srcnode[1]+3)+"px";
			line.style.top = srcnode[1]+3+draw_node_margin+"px";
			line.style.left = srcnode[0]+3+draw_node_width+draw_node_margin+"px";
			//long line "I-I"
			line = document.createElement("div");
			line.setAttribute("class","bld_line");
			layer.appendChild(line);
			line.style.width = (tgtnode[0]-3-1)-(srcnode[0]+3+1)-draw_node_width+"px";
			line.style.height = "1px";
			line.style.top = tgtnode[1]-4+draw_node_margin+"px";
			line.style.left = srcnode[0]+draw_node_width+3+1+draw_node_margin+"px";
			//I to start "I to -"
			line = document.createElement("div");
			line.setAttribute("class","bld_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = "8px";
			line.style.top = tgtnode[1]-4+draw_node_margin+"px";
			line.style.left = tgtnode[0]-3-1+draw_node_margin+"px";
			//start to tgtnode
			line = document.createElement("div");
			line.setAttribute("class","bld_line");
			layer.appendChild(line);
			line.style.width = "3px";
			line.style.height = "1px";
			line.style.top = tgtnode[1]+3+draw_node_margin+"px";
			line.style.left = tgtnode[0]-3+draw_node_margin+"px";
		}
	}
	if (srcnode[1] > tgtnode[1]){
		//s shape
		if (tgtnode[0]-srcnode[0]< draw_node_width*2){
			//end to srcnode
			var line = document.createElement("div");
			line.setAttribute("class","bld_line");
			layer.appendChild(line);
			line.style.width = "3px";
			line.style.height = "1px";
			line.style.top = srcnode[1]+3+draw_node_margin+"px";
			line.style.left = srcnode[0]+draw_node_width+draw_node_margin+"px";
			//I to end
			line = document.createElement("div");
			line.setAttribute("class","bld_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = (srcnode[1]+4)-(tgtnode[1]+draw_node_width+3)+"px";
			line.style.top = tgtnode[1]+draw_node_width+3+draw_node_margin+"px";
			line.style.left = srcnode[0]+3+draw_node_width+draw_node_margin+"px";
			//long line "I-I"
			line = document.createElement("div");
			line.setAttribute("class","bld_line");
			layer.appendChild(line);
			line.style.width = (srcnode[0]+draw_node_width+3)-(tgtnode[0]-3)+"px";
			line.style.height = "1px";
			line.style.top = tgtnode[1]+draw_node_width+3+draw_node_margin+"px";
			line.style.left = tgtnode[0]-3+draw_node_margin+"px";
			//I to start "I to -"
			line = document.createElement("div");
			line.setAttribute("class","bld_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = "8px";
			line.style.top = tgtnode[1]+3+draw_node_margin+"px";
			line.style.left = tgtnode[0]-3-1+draw_node_margin+"px";
			//start to tgtnode
			line = document.createElement("div");
			line.setAttribute("class","bld_line");
			layer.appendChild(line);
			line.style.width = "3px";
			line.style.height = "1px";
			line.style.top = tgtnode[1]+3+draw_node_margin+"px";
			line.style.left = tgtnode[0]-3+draw_node_margin+"px";
		}
		//straight down
		else if (tgtnode[0]-srcnode[0] == draw_node_width*2){
			//end to srcnode
			var line = document.createElement("div");
			line.setAttribute("class","bld_line");
			layer.appendChild(line);
			line.style.width = "3px";
			line.style.height = "1px";
			line.style.top = srcnode[1]+3+draw_node_margin+"px";
			line.style.left = srcnode[0]+draw_node_width+draw_node_margin+"px";
			//I to end
			line = document.createElement("div");
			line.setAttribute("class","bld_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = (srcnode[1]+4)-(tgtnode[1]+3)+"px";
			line.style.top = tgtnode[1]+3+draw_node_margin+"px";
			line.style.left = srcnode[0]+3+draw_node_width+draw_node_margin+"px";
			//start to tgtnode
			line = document.createElement("div");
			line.setAttribute("class","bld_line");
			layer.appendChild(line);
			line.style.width = "3px";
			line.style.height = "1px";
			line.style.top = tgtnode[1]+3+draw_node_margin+"px";
			line.style.left = tgtnode[0]-3+draw_node_margin+"px";
		}
		//reversed 3 shape
		else if (tgtnode[0]-srcnode[0] > draw_node_width*2){
			//end to srcnode
			var line = document.createElement("div");
			line.setAttribute("class","bld_line");
			layer.appendChild(line);
			line.style.width = "3px";
			line.style.height = "1px";
			line.style.top = srcnode[1]+3+draw_node_margin+"px";
			line.style.left = srcnode[0]+draw_node_width+draw_node_margin+"px";
			//I to end
			line = document.createElement("div");
			line.setAttribute("class","bld_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = (srcnode[1]+4)-(tgtnode[1]+draw_node_width+3)+"px";
			line.style.top = tgtnode[1]+draw_node_width+3+draw_node_margin+"px";
			line.style.left = srcnode[0]+3+draw_node_width+draw_node_margin+"px";
			//long line "I-I"
			line = document.createElement("div");
			line.setAttribute("class","bld_line");
			layer.appendChild(line);
			line.style.width = (tgtnode[0]-3-1)-(srcnode[0]+3+1)-draw_node_width+"px";
			line.style.height = "1px";
			line.style.top = tgtnode[1]+draw_node_width+3+draw_node_margin+"px";
			line.style.left = srcnode[0]+3+1+draw_node_width+draw_node_margin+"px";
			//I to start "I to -"
			line = document.createElement("div");
			line.setAttribute("class","bld_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = "8px";
			line.style.top = tgtnode[1]+3+draw_node_margin+"px";
			line.style.left = tgtnode[0]-3-1+draw_node_margin+"px";
			//start to tgtnode
			line = document.createElement("div");
			line.setAttribute("class","bld_line");
			layer.appendChild(line);
			line.style.width = "3px";
			line.style.height = "1px";
			line.style.top = tgtnode[1]+3+draw_node_margin+"px";
			line.style.left = tgtnode[0]-3+draw_node_margin+"px";
		}
	}
}

function drawlink_ref(srcnode,tgtnode,r_draw){
	var layer = r_draw;
	if (srcnode[1] == tgtnode[1]){
		//on same y level, straight line
		if (((tgtnode[0] - srcnode[0])>draw_node_width)&&((tgtnode[0]-srcnode[0])<=(draw_node_width+draw_node_margin+2))){
			var line = document.createElement("div");
			line.setAttribute("class","ref_line");
			layer.appendChild(line);
			line.style.width = tgtnode[0]-(srcnode[0]+draw_node_width)+"px";
			line.style.height = "1px";
			line.style.top = srcnode[1]+draw_node_width+1+"px";
			line.style.left = srcnode[0]+draw_node_width+"px";
		}
		//on same y level, need top space
		else if ((tgtnode[0] - srcnode[0])>(draw_node_width+draw_node_margin)){
			//end to srcnode '-'
			var line = document.createElement("div");
			line.setAttribute("class","ref_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = "1px";
			line.style.top = srcnode[1]+1+draw_node_margin+"px";
			line.style.left = srcnode[0]+draw_node_width+draw_node_margin+"px";
			//I to end "I to -"
			line = document.createElement("div");
			line.setAttribute("class","ref_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = "8px";
			line.style.top = srcnode[1]-6+draw_node_margin+"px";
			line.style.left = srcnode[0]+draw_node_width+1+draw_node_margin+"px";
			//long line "I-I"
			line = document.createElement("div");
			line.setAttribute("class","ref_line");
			layer.appendChild(line);
			line.style.width = tgtnode[0]-srcnode[0]-draw_node_width-1-1-1-5+"px";
			line.style.height = "1px";
			line.style.top = srcnode[1]-6+draw_node_margin+"px";
			line.style.left = srcnode[0]+draw_node_width+1+1+draw_node_margin+"px";
			//I to start "I to -"
			line = document.createElement("div");
			line.setAttribute("class","ref_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = "8px";
			line.style.top = srcnode[1]-6+draw_node_margin+"px";
			line.style.left = tgtnode[0]-5-1+draw_node_margin+"px";
			//start to tgtnode
			line = document.createElement("div");
			line.setAttribute("class","ref_line");
			layer.appendChild(line);
			line.style.width = "5px";
			line.style.height = "1px";
			line.style.top = tgtnode[1]+1+draw_node_margin+"px";
			line.style.left = tgtnode[0]-5+draw_node_margin+"px";
		}
	}
	if (srcnode[1] < tgtnode[1]){
		//2 shape
		if (tgtnode[0]-srcnode[0]< draw_node_width*2){
			//end to srcnode
			var line = document.createElement("div");
			line.setAttribute("class","ref_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = "1px";
			line.style.top = srcnode[1]+1+draw_node_margin+"px";
			line.style.left = srcnode[0]+draw_node_width+draw_node_margin+"px";
			//I to end
			line = document.createElement("div");
			line.setAttribute("class","ref_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = (tgtnode[1]-5)-(srcnode[1]+1)+"px";
			line.style.top = srcnode[1]+1+draw_node_margin+"px";
			line.style.left = srcnode[0]+1+draw_node_width+draw_node_margin+"px";
			//long line "I-I"
			line = document.createElement("div");
			line.setAttribute("class","ref_line");
			layer.appendChild(line);
			line.style.width = (srcnode[0]+draw_node_width+1)-(tgtnode[0]-5)+"px";
			line.style.height = "1px";
			line.style.top = tgtnode[1]-6+draw_node_margin+"px";
			line.style.left = tgtnode[0]-5+draw_node_margin+"px";
			//I to start "I to -"
			line = document.createElement("div");
			line.setAttribute("class","ref_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = "8px";
			line.style.top = tgtnode[1]-6+draw_node_margin+"px";
			line.style.left = tgtnode[0]-5-1+draw_node_margin+"px";
			//start to tgtnode
			line = document.createElement("div");
			line.setAttribute("class","ref_line");
			layer.appendChild(line);
			line.style.width = "5px";
			line.style.height = "1px";
			line.style.top = tgtnode[1]+1+draw_node_margin+"px";
			line.style.left = tgtnode[0]-5+draw_node_margin+"px";
		}
		//straight down
		else if (tgtnode[0]-srcnode[0] == draw_node_width*2){
			//end to srcnode
			var line = document.createElement("div");
			line.setAttribute("class","ref_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = "1px";
			line.style.top = srcnode[1]+1+draw_node_margin+"px";
			line.style.left = srcnode[0]+draw_node_width+draw_node_margin+"px";
			//I to end
			line = document.createElement("div");
			line.setAttribute("class","ref_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = (tgtnode[1]+2)-(srcnode[1]+1)+"px";
			line.style.top = srcnode[1]+1+draw_node_margin+"px";
			line.style.left = srcnode[0]+1+draw_node_width+draw_node_margin+"px";
			//start to tgtnode
			line = document.createElement("div");
			line.setAttribute("class","ref_line");
			layer.appendChild(line);
			line.style.width = "5px";
			line.style.height = "1px";
			line.style.top = tgtnode[1]+1+draw_node_margin+"px";
			line.style.left = tgtnode[0]-5+draw_node_margin+"px";
		}
		//z shape
		else if (tgtnode[0]-srcnode[0] > draw_node_width*2){
			//end to srcnode
			var line = document.createElement("div");
			line.setAttribute("class","ref_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = "1px";
			line.style.top = srcnode[1]+1+draw_node_margin+"px";
			line.style.left = srcnode[0]+draw_node_width+draw_node_margin+"px";
			//I to end
			line = document.createElement("div");
			line.setAttribute("class","ref_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = (tgtnode[1]-5)-(srcnode[1]+1)+"px";
			line.style.top = srcnode[1]+1+draw_node_margin+"px";
			line.style.left = srcnode[0]+1+draw_node_width+draw_node_margin+"px";
			//long line "I-I"
			line = document.createElement("div");
			line.setAttribute("class","ref_line");
			layer.appendChild(line);
			line.style.width = (tgtnode[0]-5-1)-(srcnode[0]+1+1)-draw_node_width+"px";
			line.style.height = "1px";
			line.style.top = tgtnode[1]-6+draw_node_margin+"px";
			line.style.left = srcnode[0]+draw_node_width+1+1+draw_node_margin+"px";
			//I to start "I to -"
			line = document.createElement("div");
			line.setAttribute("class","ref_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = "8px";
			line.style.top = tgtnode[1]-6+draw_node_margin+"px";
			line.style.left = tgtnode[0]-5-1+draw_node_margin+"px";
			//start to tgtnode
			line = document.createElement("div");
			line.setAttribute("class","ref_line");
			layer.appendChild(line);
			line.style.width = "5px";
			line.style.height = "1px";
			line.style.top = tgtnode[1]+1+draw_node_margin+"px";
			line.style.left = tgtnode[0]-5+draw_node_margin+"px";
		}
	}
	if (srcnode[1] > tgtnode[1]){
		//s shape
		if (tgtnode[0]-srcnode[0]< draw_node_width*2){
			//end to srcnode
			var line = document.createElement("div");
			line.setAttribute("class","ref_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = "1px";
			line.style.top = srcnode[1]+1+draw_node_margin+"px";
			line.style.left = srcnode[0]+draw_node_width+draw_node_margin+"px";
			//I to end
			line = document.createElement("div");
			line.setAttribute("class","ref_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = (srcnode[1]+2)-(tgtnode[1]+draw_node_width+1)+"px";
			line.style.top = tgtnode[1]+draw_node_width+1+draw_node_margin+"px";
			line.style.left = srcnode[0]+1+draw_node_width+draw_node_margin+"px";
			//long line "I-I"
			line = document.createElement("div");
			line.setAttribute("class","ref_line");
			layer.appendChild(line);
			line.style.width = (srcnode[0]+draw_node_width+1)-(tgtnode[0]-5)+"px";
			line.style.height = "1px";
			line.style.top = tgtnode[1]+draw_node_width+1+draw_node_margin+"px";
			line.style.left = tgtnode[0]-5+draw_node_margin+"px";
			//I to start "I to -"
			line = document.createElement("div");
			line.setAttribute("class","ref_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = "8px";
			line.style.top = tgtnode[1]+1+draw_node_margin+"px";
			line.style.left = tgtnode[0]-5-1+draw_node_margin+"px";
			//start to tgtnode
			line = document.createElement("div");
			line.setAttribute("class","ref_line");
			layer.appendChild(line);
			line.style.width = "5px";
			line.style.height = "1px";
			line.style.top = tgtnode[1]+1+draw_node_margin+"px";
			line.style.left = tgtnode[0]-5+draw_node_margin+"px";
		}
		//straight down
		else if (tgtnode[0]-srcnode[0] == draw_node_width*2){
			//end to srcnode
			var line = document.createElement("div");
			line.setAttribute("class","ref_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = "1px";
			line.style.top = srcnode[1]+1+draw_node_margin+"px";
			line.style.left = srcnode[0]+draw_node_width+draw_node_margin+"px";
			//I to end
			line = document.createElement("div");
			line.setAttribute("class","ref_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = (srcnode[1]+2)-(tgtnode[1]+1)+"px";
			line.style.top = tgtnode[1]+1+draw_node_margin+"px";
			line.style.left = srcnode[0]+1+draw_node_width+draw_node_margin+"px";
			//start to tgtnode
			line = document.createElement("div");
			line.setAttribute("class","ref_line");
			layer.appendChild(line);
			line.style.width = "5px";
			line.style.height = "1px";
			line.style.top = tgtnode[1]+1+draw_node_margin+"px";
			line.style.left = tgtnode[0]-5+draw_node_margin+"px";
		}
		//reversed 3 shape
		else if (tgtnode[0]-srcnode[0] > draw_node_width*2){
			//end to srcnode
			var line = document.createElement("div");
			line.setAttribute("class","ref_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = "1px";
			line.style.top = srcnode[1]+1+draw_node_margin+"px";
			line.style.left = srcnode[0]+draw_node_width+draw_node_margin+"px";
			//I to end
			line = document.createElement("div");
			line.setAttribute("class","ref_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = (srcnode[1]+2)-(tgtnode[1]+draw_node_width+1)+"px";
			line.style.top = tgtnode[1]+draw_node_width+1+draw_node_margin+"px";
			line.style.left = srcnode[0]+1+draw_node_width+draw_node_margin+"px";
			//long line "I-I"
			line = document.createElement("div");
			line.setAttribute("class","ref_line");
			layer.appendChild(line);
			line.style.width = (tgtnode[0]-5-1)-(srcnode[0]+1+1)-draw_node_width+"px";
			line.style.height = "1px";
			line.style.top = tgtnode[1]+draw_node_width+1+draw_node_margin+"px";
			line.style.left = srcnode[0]+draw_node_width+1+1+draw_node_margin+"px";
			//I to start "I to -"
			line = document.createElement("div");
			line.setAttribute("class","ref_line");
			layer.appendChild(line);
			line.style.width = "1px";
			line.style.height = "8px";
			line.style.top = tgtnode[1]+1+draw_node_margin+"px";
			line.style.left = tgtnode[0]-5-1+draw_node_margin+"px";
			//start to tgtnode
			line = document.createElement("div");
			line.setAttribute("class","ref_line");
			layer.appendChild(line);
			line.style.width = "5px";
			line.style.height = "1px";
			line.style.top = tgtnode[1]+1+draw_node_margin+"px";
			line.style.left = tgtnode[0]-5+draw_node_margin+"px";
		}
	}
}
function checkn2d(tnode,r_draw){
	var root_draw = r_draw.parentNode.children[4];
	var cnt = root_draw.children.length;
	var result = 0;
	for (var i = 0; i < cnt; i++){
		if ( root_draw.children[i].getAttribute("drawnoteid") == tnode){
			result = 1;
			break;
		}
	}
	return result;
}
















//bar pointer move
var _startX = 0;            // mouse starting positions
var _offsetX = 0;           // current element offset
var _dragElement;           // pass the element
var _moving = 0;
var _blockwidth = bar_width;

function ptdown(e){
	var pp=document.getElementById("pointer");
	_blockwidth = parseInt(pp.style.width);
	var pt;
	e = e ? e : window.event;
	if (e.target) pt = e.target;
	if (_moving == 0 ){
		if (isNaN(parseInt(pt.style.left))) {pt.style.left = "0px";}
		_offsetX = parseInt(pt.style.left);
		_startX = e.clientX;
		_dragElement = pt;
		_moving = 1;
		document.onmousemove = ptmove;
	}
	else if (_moving == 1){
		ptup(e);
	}
}
function ptmove(e){
	e = e ? e : window.event;
	_dragElement.style.left = (_offsetX + e.clientX - _startX) + 'px';
	if (parseInt(_dragElement.style.left) < 0) {_dragElement.style.left = "0px";}
	if (parseInt(_dragElement.style.left) > (bar_width - _blockwidth)) {_dragElement.style.left = (bar_width - _blockwidth)+"px";}
}
function ptup(e){
	e = e ? e : window.event;
	document.onmousemove = null;
	document.onmouseup = null;
	document.onselectstart = null;
	_dragElement = null;
	_moving = 0;
	redraw();
}


////////////////////////////////
//
//     triggers
//
////////////////////////////////
function showallauthor(){
	var pp = document.getElementById("showallauthor");
	pp = pp.children[0];
	if (pp.firstChild.nodeValue == "显示作者"){
		pp.firstChild.nodeValue = "隐藏作者";
		author_flag = 1;
	}
	else if (pp.firstChild.nodeValue == "隐藏作者"){
		pp.firstChild.nodeValue = "显示作者";
		author_flag = 0;
	}
	else alert("error");
	redraw();
}
function showalltitle(){
	var pp = document.getElementById("showalltitle");
	pp = pp.children[0];
	if (pp.firstChild.nodeValue == "显示标题"){
		pp.firstChild.nodeValue = "隐藏标题";
		title_flag = 1;
	}
	else if (pp.firstChild.nodeValue == "隐藏标题"){
		pp.firstChild.nodeValue = "显示标题";
		title_flag = 0;
	}
	else alert("error");
	redraw();
}
function showreflink(){
	var pp = document.getElementById("showalllink");
	pp = pp.children[1];
	if (pp.firstChild.nodeValue == "显示引用关系"){
		pp.firstChild.nodeValue = "隐藏引用关系";
		ref_flag = 1;
	}
	else if (pp.firstChild.nodeValue == "隐藏引用关系"){
		pp.firstChild.nodeValue = "显示引用关系";
		ref_flag = 0;
	}
	else alert("error");
	redraw();
}
function showbldlink(){
	var pp = document.getElementById("showalllink");
	pp = pp.children[0];
	if (pp.firstChild.nodeValue == "显示发展关系"){
		pp.firstChild.nodeValue = "隐藏发展关系";
		bld_flag = 1;
	}
	else if (pp.firstChild.nodeValue == "隐藏发展关系"){
		pp.firstChild.nodeValue = "显示发展关系";
		bld_flag = 0;
	}
	else alert("error");
	redraw();
}