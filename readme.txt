#######################################
Local Database connection in the following files -
#######################################

CreateDatabase.java
sqls.java

#######################################
Rename project
#######################################

1. Rename project in eclipse
2. Replace all text /ITM/ with new name using search 
3. Change project name in build.xml and build.properties



#######################################
Setup project
#######################################

1.Deploy war or build files
2.Create itm database
3.Remove other old databases
4.Login with given database name and corresponding database will be created in mysql