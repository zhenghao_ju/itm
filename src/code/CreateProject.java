package code;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import code.*;
import database.*;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpSession;


/**
 * Servlet implementation class CreateProject
 */
@WebServlet("/CreateProject")
public class CreateProject extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateProject() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
             request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=UTF-8");
            // TODO Auto-generated method stub
		String strproject = request.getParameter("projectname");
		String strteacher = request.getParameter("teacher");
		String strschool = request.getParameter("school");
		String[] arraygroup = request.getParameterValues("group");
		String[] arraygrade = request.getParameterValues("grade");
		String database = request.getParameter("database");
		String fromyear = request.getParameter("fromyear");
		String toyear = request.getParameter("toyear");
		String username = request.getParameter("username");
		int deleted = Integer.parseInt(request.getParameter("deleted"));
		System.out.println("username is"+username);
		System.out.println("Deleted is"+deleted);
		sqls s = new sqls();
		Operatedb dbobject = new Operatedb(s,database);
		int ires = dbobject.WriteNewProject(strproject,strteacher,strschool,arraygrade, arraygroup,fromyear,toyear,username,deleted);
                RequestDispatcher rd=null;
                 HttpSession session=request.getSession();
                                        short isCN=Short.parseShort(session.getAttribute("lan").toString());
                                         String url ="";
                                               
		
		if(ires == 1){
                    if(isCN==1)
                        url =response.encodeURL("/cn/New_CreateProject.jsp?ifsucceed=no&database="+database);
                     else
                        response.encodeURL("/en/New_CreateProject.jsp?ifsucceed=no&database="+database);
                    response.sendRedirect(url);
		}else{
                     if(isCN==1)
                       url ="/cn/New_CreateProject.jsp?database="+database+"&ifsucceed=yes&projectname="+strproject;
                     else
                       url ="/en/New_CreateProject.jsp?database="+database+"&ifsucceed=yes&projectname="+strproject; 
                    rd=getServletContext().getRequestDispatcher(url);
		}
		
		if(dbobject!= null){
				dbobject.CloseCon();
			
		}
                rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request,response);// TODO Auto-generated method stub
	}

}
