package code;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.zoolib.tuplebase.ZTB;

import com.knowledgeforum.k5.common.K5TBConnector;

import database.*;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.zoolib.*;
import org.zoolib.tuplebase.*;

import com.knowledgeforum.k5.common.K5TBConnector;
import javax.servlet.http.HttpSession;
/**
 * Servlet implementation class RefreshDB
 */
@WebServlet("/RefreshDB")
public class RefreshDB extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RefreshDB() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String strdb = request.getParameter("database");
		String host=null,db=null, username=null,password=null;
		int port=0;
		sqls s = new sqls();
		Operatedb pqdb = new Operatedb(s,"itm");
		String strcolum = "URL,kfdb,username,password,port";
		String strcon = "localdb='"+strdb+"'";
		ResultSet rs=null;
		try{
			rs = pqdb.GetRecordsFromDB("connection_table", strcolum, strcon);
			if(rs.next()){
				host = rs.getString(1);
				db = rs.getString(2);
				username = rs.getString(3);
				password = rs.getString(4);
				port = rs.getInt(5);
			}
					
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			if(rs != null){
				try{
					rs.close();
				}catch(SQLException e){
					e.printStackTrace();
				}
			}
			if(pqdb != null){
				pqdb.CloseCon();
			}
			if(s != null){
				//s.Close();
			}
			
		}
		ZTB tb = K5TBConnector.sGetTB_HTTP_UserName(new K5TBConnector.HostInfo(host, port, db),null, username, password,null);
		if(tb != null){
			UpdateViews(tb,strdb); 
			UpdateNote(tb,strdb); 
			UpdateNoteViewLink(tb,strdb); 
			Updateauthor(tb,strdb);
			UpdateAuthorNoteLink(tb,strdb);
			UpdateNoteNoteLink(tb,strdb);
			UpdateGroups(tb,strdb);
			UpdateGroupAuthorLink(tb,strdb);
			UpdateRefreshTime(strdb);
			tb.close();
		}
		HttpSession session=request.getSession();
                Boolean isCN=(Boolean)session.getAttribute("isCN");
                String strpage="";
		if(request.getParameter("projectname").equals("null"))
		{
                    if(isCN){
			 strpage = "/itm/cn/New_HomePage.jsp?database=" +request.getParameter("database"); 
                    } else
                        strpage = "/itm/en/New_HomePage.jsp?database=" +request.getParameter("database");  
		  String url =response.encodeURL(strpage);
		  response.sendRedirect(url);
		}
		else
		{                    
                    if(isCN){
                        strpage = "/itm/cn/New_HomePage.jsp?database=" +request.getParameter("database")+"&projectname="+request.getParameter("projectname"); 
                    } else {
                        strpage = "/itm/en/New_HomePage.jsp?database=" +request.getParameter("database")+"&projectname="+request.getParameter("projectname"); 
                    }
		  
		  String url =response.encodeURL(strpage);
		  response.sendRedirect(url);
		}
	}
	/**
	 * update the refresh time
	 */
	private void UpdateRefreshTime(String strdb){
		
		sqls s = new sqls();
		String strtime=null;
		String strsql = null;
		
		//get current time
	    java.util.Date curdate = new java.util.Date();
	    SimpleDateFormat spdtformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		strtime = spdtformat.format(curdate);
		
		Statement stmt = s.Connect("itm");
		String strcon = "dbname='"+strdb+"'";
		
		strsql = "UPDATE db_table SET refreshtime='"+strtime+"' WHERE dbname='"+strdb+"'";
		System.out.println(strsql);
		try{
			stmt.executeUpdate(strsql);
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			if(stmt != null){
				try{
					stmt.close();
				}catch(SQLException e){
					e.printStackTrace();
				}
				
			}
			if(s != null){
				s.Close();
			}
		}
		return;
	}
	/**
	 * update view_table
	 * @param tb
	 * @param strdb
	 */
	private void UpdateViews(ZTB tb,String strdb){
		System.out.println("updating views from KF");
		ViewObject viewobj;
		int i=0,num=0;
		String sqlvalue=null,strcon=null;
		long iid;
		ResultSet rs=null;
		Retrieve rtrv=new Retrieve();
		//sqls s = new sqls();
		Operatedb dbobject = new Operatedb(new sqls(),strdb); //get one database operation object
		/*********************write the view objects into database********************/
		List viewList = rtrv.GetViewObject(tb);//get all views from KF
		 
		//update views
		for(i=0; i<viewList.size();i++)
		{
			  viewobj = (ViewObject)viewList.get(i);
			  iid = viewobj.GetID();
			  strcon = "idview="+iid;
			  try{
				  rs = dbobject.GetRecordsFromDB("view_table", "count(*)", strcon);
				  if(rs.next()){
					  num = rs.getInt(1); 
				  }  
			  }catch(SQLException e){
				  e.printStackTrace();
			  }
			  
			  if(num==0){
				  sqlvalue = " (`idview`, `title`) VALUES ("+viewobj.GetID()+","+"'" +viewobj.GetTitle()+"');";
				 // System.out.println("the sqlvalue is "+sqlvalue);
				  dbobject.WritebacktoDB("view_table","INSERT",sqlvalue);	
			  }
		} 
		if(rs != null){
			 try{
				 rs.close();
			 }catch(SQLException e){
				 e.printStackTrace();
			 }
		 }
		if(dbobject != null){
			dbobject.CloseCon();
		}
		return;
		
	}
	
	/**
	 * update note_table
	 */
	private void UpdateNote(ZTB tb,String strdb){
		System.out.println("updating the note_table");
		NoteObject noteobj;
		String sqlvalue, strcretime,strcon;
		Retrieve rtrv=new Retrieve();
		ResultSet rs = null;
		long iid;
		int num = 0;
		Operatedb dbobject = new Operatedb(new sqls(),strdb); //get one database operation object
		SimpleDateFormat spdtformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		List noteList = rtrv.GetNoteObject(tb);//get all notes from KF
		   
		//add all view objects into view_table
		 for(int i=0; i<noteList.size();i++)
		  {
			  noteobj = (NoteObject)noteList.get(i);
			  iid = noteobj.GetID();
			  strcon = "noteid="+iid;
			  try{
				  rs = dbobject.GetRecordsFromDB("note_table", "count(*)", strcon);
				  if(rs.next()){
					  num = rs.getInt(1); 
				  } 
				  
				  if(num==0){
					  strcretime = spdtformat.format(noteobj.GetCretime());
					  sqlvalue = " (`noteid`, `notetitle`,`notecontent`,`createtime`) VALUES ("+noteobj.GetID()+","+"'" 
					  +noteobj.GetTitle()+"','"+noteobj.GetContent()+"','"+strcretime+"');";
					  dbobject.WritebacktoDB("note_table","INSERT",sqlvalue);	   
				  }
			  }catch(SQLException e){
				  e.printStackTrace();
			  }
			  
		  }
		 if(rs != null){
			 try{
				 rs.close();
			 }catch(SQLException e){
				 e.printStackTrace();
			 }
		 }
		 if(dbobject != null){
			 dbobject.CloseCon();
		 }
		 
		 return;
		
}
	/**
	 *update note_view table 
	 */
	private void UpdateNoteViewLink(ZTB tb,String strdb){
		
		System.out.println("update the view_note table ");
		Links linkobj;
		int i,num=0;
		long noteid,viewid;
		String sqlvalue,strcon;
		ResultSet rs = null;
		Retrieve rtrv=new Retrieve();
		Operatedb dbobject = new Operatedb(new sqls(),strdb); //get one database operation object
		
	    //get all view_note link from KF
		List linkList = rtrv.GetViewNoteLink(tb);
		 
		//add all view_note links into view_note table
		for(i=0; i<linkList.size();i++)
		{
			  linkobj = (Links)linkList.get(i);
			  viewid = linkobj.GetFromID(); //view id
			  noteid = linkobj.GetToID();//note id
			  strcon = "viewid="+viewid+" and noteid="+noteid;
			  try{
				  rs = dbobject.GetRecordsFromDB("view_note", "count(*)", strcon);
				  if(rs.next()){
					  num = rs.getInt(1); 
				  }
				  
				  if(num==0){
					  sqlvalue = " (`noteid`, `viewid`) VALUES ("+linkobj.GetToID()+","+ +linkobj.GetFromID()+");";
					  //System.out.println("the sqlvalue is "+sqlvalue);
					  dbobject.WritebacktoDB("view_note","INSERT",sqlvalue); 
				  }
				  
			  }catch(SQLException e){
				  e.printStackTrace();				  
			  }
			  	  
		} 
		if(rs != null){
			 try{
				 rs.close();
			 }catch(SQLException e){
				 e.printStackTrace();
			 }
		 }
		 if(dbobject != null){
			 dbobject.CloseCon();
		 }
		
		return;
		
	}
	
	/**
	 * update note_note table
	 * @param tb
	 * @param strdb
	 */
	private void UpdateNoteNoteLink(ZTB tb,String strdb){
		System.out.println("updating the note_to_note table");
		Links linkobj;
		int i,num=0;
		long fromid, toid;
		String sqlvalue,strcon,strtype;
		ResultSet rs = null;
		Retrieve rtrv=new Retrieve();
		Operatedb dbobject = new Operatedb(new sqls(),strdb); //get one database operation object
		
	    //get all note_to_note link from KF
		List linkList = rtrv.GetNoteNoteLink(tb);
		 
		//add all view_note links into view_note table
		for(i=0; i<linkList.size();i++)
		{
			  linkobj = (Links)linkList.get(i);
			  fromid = linkobj.GetFromID();
			  toid = linkobj.GetToID();
			  strtype = linkobj.GetLink();
			  strcon = "fromnoteid="+fromid+" and tonoteid="+toid+" and linktype='"+strtype+"'";
			  
			  try{
				  rs = dbobject.GetRecordsFromDB("note_note", "count(*)", strcon);
				  if(rs.next()){
					  num = rs.getInt(1); 
				  }
				  
				  if(num==0){
					  sqlvalue = " (`fromnoteid`, `tonoteid`,`linktype`) VALUES ("+linkobj.GetFromID()+","+linkobj.GetToID()+",'" + linkobj.GetLink()+"');";
					  dbobject.WritebacktoDB("note_note","INSERT",sqlvalue);
				  }
				  
			  }catch(SQLException e){
				  e.printStackTrace();
			  }			  	  
		} 
		if(rs != null){
			 try{
				 rs.close();
			 }catch(SQLException e){
				 e.printStackTrace();
			 }
		 }
		 if(dbobject != null){
			 dbobject.CloseCon();
		 }
		return;
	}
	
	/**
	 * update author_note table
	 * @param tb
	 * @param strdb
	 */
	private void UpdateAuthorNoteLink(ZTB tb,String strdb){
		
		System.out.println("updating author_note table");
		Links linkobj;
		int i,num=0;
		long noteid, authorid;
		String sqlvalue,strcon;
		Retrieve rtrv=new Retrieve();
		ResultSet rs = null;
		//sqls s = new sqls();
		Operatedb dbobject = new Operatedb(new sqls(),strdb); //get one database operation object
		
	    //get all author_note link from KF
		List linkList = rtrv.GetAuthorNoteLink(tb);
		 
		//add all view_note links into view_note table
		for(i=0; i<linkList.size();i++)
		{
			  linkobj = (Links)linkList.get(i);
			  noteid = linkobj.GetToID();
			  authorid = linkobj.GetFromID();
			  strcon = "noteid="+noteid+" and authorid="+authorid;
			  try{
				  rs = dbobject.GetRecordsFromDB("author_note", "count(*)", strcon);
				  if(rs.next()){
					  num = rs.getInt(1); 
				  }
				  
				  if(num==0){
					  sqlvalue = " (`noteid`, `authorid`) VALUES ("+linkobj.GetToID()+","+ +linkobj.GetFromID()+");";
					  dbobject.WritebacktoDB("author_note","INSERT",sqlvalue);
				  }  
			  }catch(SQLException e){
				  e.printStackTrace();
			  }	  
		} 
		
		if(rs != null){
			 try{
				 rs.close();
			 }catch(SQLException e){
				 e.printStackTrace();
			 }
		 }
		 if(dbobject != null){
			 dbobject.CloseCon();
		 }
		 
		 return;
		
	}
	
	/**
	 * update author_table
	 * @param tb
	 * @param strdb
	 */
	private void Updateauthor(ZTB tb,String strdb){
		
		System.out.println("updating author_table");
		AuthorObject authorobj;
		String sqlvalue, strcon;
		ResultSet rs = null;
		int num = 0;
		Retrieve rtrv=new Retrieve();
		Operatedb dbobject = new Operatedb(new sqls(),strdb); //get one database operation object
				
		List authorList = rtrv.GetAuthorObject(tb);//get author from KF
		   
		//add author objects into author_table
		 for(int i=0; i<authorList.size();i++)
		  {
			  authorobj = (AuthorObject)authorList.get(i);
			  strcon = "authorid="+authorobj.GetID();
			  try{
				  rs = dbobject.GetRecordsFromDB("author_table", "count(*)", strcon);
				  if(rs.next()){
					  num = rs.getInt(1); 
				  } 
				  
				  if(num==0){
					  sqlvalue = " (`authorid`, `firstname`,`lastname`,`username`,`type`) VALUES ("+authorobj.GetID()+","+"'" 
					  +authorobj.GetFstname()+"','"+authorobj.GetLstname()+"','"+authorobj.GetUsname()+"','"+authorobj.GetType()+"');";
					  dbobject.WritebacktoDB("author_table","INSERT",sqlvalue);   
				  }
			  }catch(SQLException e){
				  e.printStackTrace();
			  }
			  	  
		  } 
		 if(rs != null){
			 try{
				 rs.close();
			 }catch(SQLException e){
				 e.printStackTrace();
			 }
		 }
		 if(dbobject != null){
			 dbobject.CloseCon();
		 }
		 return;
	}
	
	/**
	 * update group_table
	 * @param tb
	 * @param strdb
	 */
	private void UpdateGroups(ZTB tb,String strdb){
		
		System.out.println("updating group_table");
		GroupObject groupobj;
		String sqlvalue,strcon;
		ResultSet rs = null;
		long id;
		int num = 0;
		Operatedb dbobject = new Operatedb(new sqls(),strdb); //get one database operation object
		List Gtitlelist = new ArrayList();
		Retrieve rtrv=new Retrieve();
		List groupList = rtrv.GetGroupObject(tb); //get groups from KF

		for(int i=0; i<groupList.size();i++)
		{
			  groupobj = (GroupObject)groupList.get(i);
			  Gtitlelist.add(groupobj.GetTitle());
			  id = groupobj.GetID();
			  strcon = "idgroup="+id;
			  try{
				  rs = dbobject.GetRecordsFromDB("group_table", "count(*)", strcon);
				  if(rs.next()){
					  num = rs.getInt(1); 
				  } 
				  
				  if(num==0){
					//System.out.println("the "+i+"th"+" group title is: "+groupobj.GetTitle());
					  sqlvalue = " (`idgroup`, `title`) VALUES ("+groupobj.GetID()+","+"'" +groupobj.GetTitle()+"');";
					  //System.out.println("the sqlvalue is "+sqlvalue);
					  dbobject.WritebacktoDB("group_table","INSERT",sqlvalue);  
				  }
				  
			  }catch(SQLException e){
				  e.printStackTrace();
			  }	  
		}
		
		if(rs != null){
			 try{
				 rs.close();
			 }catch(SQLException e){
				 e.printStackTrace();
			 }
		 }
		 if(dbobject != null){
			 dbobject.CloseCon();
		 }
		 
		return;	
	}
	
	/**
	 * update author_note table
	 * @param tb
	 * @param strdb
	 */
	private void UpdateGroupAuthorLink(ZTB tb,String strdb){
		
		System.out.println("updating group_author table");
		Links linkobj;
		int i,num=0;
		long author_id,group_id;
		String sqlvalue,strcon;
		Retrieve rtrv=new Retrieve();
		ResultSet rs = null;
		//sqls s = new sqls();
		Operatedb dbobject = new Operatedb(new sqls(),strdb); //get one database operation object
		
	    //get all group_author link from KF
		List linkList = rtrv.GetGroupAuthorLink(tb);
		 
		//add all group_author links into group_author table
		for(i=0; i<linkList.size();i++)
		{
			  linkobj = (Links)linkList.get(i);
			  author_id = linkobj.GetToID();
			  group_id = linkobj.GetFromID();
			  strcon = "author_id="+author_id+" and group_id="+group_id;
			  try{
				  rs = dbobject.GetRecordsFromDB("group_author", "count(*)", strcon);
				  if(rs.next()){
					  num = rs.getInt(1); 
				  }
				  
				  if(num==0){
					  sqlvalue = " (`author_id`, `group_id`) VALUES ("+linkobj.GetToID()+","+ +linkobj.GetFromID()+");";
					  dbobject.WritebacktoDB("group_author","INSERT",sqlvalue);
				  }  
			  }catch(SQLException e){
				  e.printStackTrace();
			  }	  
		} 
		
		if(rs != null){
			 try{
				 rs.close();
			 }catch(SQLException e){
				 e.printStackTrace();
			 }
		 }
		 if(dbobject != null){
			 dbobject.CloseCon();
		 }
		 
		 return;
		
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request,response);// TODO Auto-generated method stub
	}

}
