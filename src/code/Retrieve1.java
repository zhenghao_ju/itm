package code;
import java.util.*;

import org.zoolib.*;
import org.zoolib.tuplebase.*;

import java.sql.*;

import org.zoolib.tuplebase.ZTB;
import database.*;

public class Retrieve1 
{
/**
 * get the information of the views object from KF 
 * @param tb
 * 		an tuplebase connection 
 * @return
 * 		list of view objects
 */
public List GetViewObject(ZTB tb)
{
	HandleStr handleobj = new HandleStr();
	List viewList = null;
	String strtitle;
	if(tb != null)
	{
		while(tb !=null && true)
		{
			//create a new transaction
			ZTxn txn = new ZTxn();
			//create an object to encapsulate the tuplebase and the transaction
			ZTBTxn tbTxn = new ZTBTxn(txn,tb);
			
			ZTBQuery viewsQuery = new ZTBQuery(ZTBSpec.sEquals("Object", "view"));
			viewList = new ArrayList();
			
			// Create an iterator to loop over the list of tuples that were found
		    for (ZTBIter viewsIter = new ZTBIter(tbTxn, viewsQuery); viewsIter.hasValue(); viewsIter.advance())
		    {
		    	// get the current tuple in the list
		        ZTuple viewTuple = viewsIter.get(); 
		        
		       //create the class instance responding to the view table
		    	ViewObject viewObj = new ViewObject();
		    	
		        viewObj.SetID(viewsIter.getZID().longValue());
		        strtitle = viewTuple.getString("titl");
		      //replace one single quotation character with two single quotation characters
		        strtitle = strtitle.replace("'", "''");
		        viewObj.SetTitle(strtitle);
		        
		        viewList.add(viewObj);
		    }
		    // break out of the loop if the transaction was committed successfully
		    if (txn.commit())
		      break;					
		}		
	}
	else
	{
		System.out.println("the tuplebase connection is invalid when getting view object from KF");
	}
	
	return viewList;
	
}


/**
 * get the information of the note objects from KF 
 * @param tb
 * 		an tuplebase connection 
 * @return
 * 		list of note objects
 */
public List GetNoteObject(ZTB tb)
{
	List noteList = null;
	String strtemp;
	if(tb != null)
	{
		while(tb != null && true)
		{
			//create a new transaction
			ZTxn txn = new ZTxn();
			//create an object to encapsulate the tuplebase and the transaction
			ZTBTxn tbTxn = new ZTBTxn(txn,tb);
			
			ZTBQuery notesQuery = new ZTBQuery(ZTBSpec.sEquals("Object", "note"));
			noteList = new ArrayList();
			
			// Create an iterator to loop over the list of tuples that were found
		    for (ZTBIter notesIter = new ZTBIter(tbTxn, notesQuery); notesIter.hasValue(); notesIter.advance())
		    {
		    	// get the current tuple in the list
		        ZTuple noteTuple = notesIter.get(); 
		        
		       //create the class instance responding to the view table
		    	NoteObject noteObj = new NoteObject();
		    	
		    	//get id
		        noteObj.SetID(notesIter.getZID().longValue());
		        
		        //get title
		        strtemp = noteTuple.getString("titl");
		      //replace one single quotation character with two single quotation characters
		        strtemp = strtemp.replace("'", "''");
		        noteObj.SetTitle(strtemp);
		        
		        //get content
		        strtemp = noteTuple.getString("text");
		      //replace one single quotation character with two single quotation characters
		        strtemp = strtemp.replace("'", "''");
		        noteObj.SetContent(strtemp);
		        
		        //get create time
		        noteObj.SetCretime(noteTuple.getTime("crea"));
		        	        
		        noteList.add(noteObj);
		    }
		    // break out of the loop if the transaction was committed successfully
		    if (txn.commit())
		      break;					
		}		
	}
	else
	{
		System.out.println("the tuplebase connection is invalid when getting view object from KF");
	}
	
	return noteList;
	
}

/**
 * get the author objects from KF 
 * @param tb
 * 		an tuplebase connection 
 * @return
 * 		list of author objects
 */
public List GetAuthorObject(ZTB tb)
{
	List authorList = null;
	String strtemp;
	if(tb != null)
	{
		while(tb != null && true)
		{
			//create a new transaction
			ZTxn txn = new ZTxn();
			//create an object to encapsulate the tuplebase and the transaction
			ZTBTxn tbTxn = new ZTBTxn(txn,tb);
			
			ZTBQuery authorQuery = new ZTBQuery(ZTBSpec.sEquals("Object", "author"));
			authorList = new ArrayList();
			
			// Create an iterator to loop over the list of tuples that were found
		    for (ZTBIter authorIter = new ZTBIter(tbTxn, authorQuery); authorIter.hasValue(); authorIter.advance())
		    {
		    	// get the current tuple in the list
		        ZTuple authorTuple = authorIter.get(); 
		        
		       //create the class instance responding to the view table
		    	AuthorObject authorObj = new AuthorObject();
		    	
		    	//get id
		        authorObj.SetID(authorIter.getZID().longValue());
		        
		        //get first name
		        strtemp = authorTuple.getString("fnam");
		        strtemp = strtemp.replace("'", "''");
		        authorObj.SetFstname(strtemp);
		        
		        //get last name
		        strtemp = authorTuple.getString("lnam");
		        strtemp = strtemp.replace("'", "''");
		        authorObj.SetLstname(strtemp);
		        
		        //get user name
		        strtemp = authorTuple.getString("unam");
		        strtemp = strtemp.replace("'", "''");
		        authorObj.SetUsname(strtemp);
		        
		        //get type
		        strtemp = authorTuple.getString("type");
		        authorObj.SetType(strtemp);
		            	        
		        authorList.add(authorObj);
		    }
		    // break out of the loop if the transaction was committed successfully
		    if (txn.commit())
		      break;					
		}		
	}
	else
	{
		System.out.println("the tuplebase connection is invalid when getting author object from KF");
	}
	
	return authorList;
	
}

/**
 * get the support objects from KF 
 * @param tb
 * 		an tuplebase connection 
 * @return
 * 		list of support objects
 */
public List getSupports(ZTB tb)
{
	List supportList = null;
	String strtemp;
	if(tb != null)
	{
		while(tb != null && true)
		{
			//create a new transaction
			ZTxn txn = new ZTxn();
			//create an object to encapsulate the tuplebase and the transaction
			ZTBTxn tbTxn = new ZTBTxn(txn,tb);
			
			ZTBQuery supportQuery = new ZTBQuery(ZTBSpec.sEquals("Object", "support"));
			supportList = new ArrayList();
			
			// Create an iterator to loop over the list of tuples that were found
		    for (ZTBIter supportIter = new ZTBIter(tbTxn, supportQuery); supportIter.hasValue(); supportIter.advance())
		    {
		    	// get the current tuple in the list
		        ZTuple supportTuple = supportIter.get(); 
		        
		       //create the class instance responding to the support table
		    	SupportObject supportObj = new SupportObject();
		    	
		    	//get id
		        supportObj.setID(supportIter.getZID().longValue());
		        
		        //get the text
		        supportObj.setText(supportTuple.getString("text"));
		        //System.out.println(supportTuple.getString("text"));
		        	        
		        supportList.add(supportObj);
		    }
		    // break out of the loop if the transaction was committed successfully
		    if (txn.commit())
		      break;					
		}		
	}
	else
	{
		System.out.println("the tuplebase connection is invalid when getting support object from KF");
	}
	
	return supportList;
	
}


/**
 * get the support-note link from KF 
 * @param tb
 * 		an tuplebase connection 
 * @return
 * 		list of link objects
 */
public List getSupportNoteLink(ZTB tb)
{
	List linkList = null;
	
	if(tb != null)
	{
		//create a new transaction
		ZTxn txn = new ZTxn();
		//create an object to encapsulate the tuplebase and the transaction
		ZTBTxn tbTxn = new ZTBTxn(txn,tb);
		linkList = new ArrayList();
		
		ZTBQuery supportsLinkQuery = new ZTBQuery(ZTBSpec.sEquals("Link","supports"));
		for (ZTBIter it2 = new ZTBIter(txn,tb,supportsLinkQuery); it2.hasValue(); it2.advance()) {
			ZTuple link = it2.get();
			long supportID = link.getID("from");
			long noteID = link.getID("to");
			String arroundText = link.getString("text");
			Links linkobj = new Links();
        	linkobj.SetFromID(supportID);
        	linkobj.SetToID(noteID);
        	linkobj.SetText(arroundText.replace("'", "''"));
      
        	linkList.add(linkobj);
		}
		
		
	}else{
		System.out.println("the tuplebase connection is invalid when getting group object from KF");
	}
	
	return linkList;

}

/**
 * get the group object from KF 
 * @param tb
 * 		an tuplebase connection 
 * @return
 * 		list of group objects
 */
public List GetGroupObject(ZTB tb)
{
	List groupList = null;
	if(tb != null)
	{
		while(tb != null && true)
		{
			//create a new transaction
			ZTxn txn = new ZTxn();
			//create an object to encapsulate the tuplebase and the transaction
			ZTBTxn tbTxn = new ZTBTxn(txn,tb);
			
			ZTBQuery groupQuery = new ZTBQuery(ZTBSpec.sEquals("Object", "group"));
			groupList = new ArrayList();
			
			// Create an iterator to loop over the list of tuples that were found
		    for (ZTBIter groupIter = new ZTBIter(tbTxn, groupQuery); groupIter.hasValue(); groupIter.advance())
		    {
		    	// get the current tuple in the list
		        ZTuple groupTuple = groupIter.get(); 
		        
		       //create the class instance responding to the view table
		    	GroupObject groupObj = new GroupObject();
		    	
		        groupObj.SetID(groupIter.getZID().longValue());
		        groupObj.SetTitle(groupTuple.getString("titl").replace("'","''"));
		       // System.out.println("the group title is:" + groupTuple.getString("titl"));
		        
		        groupList.add(groupObj);
		    }
		    // break out of the loop if the transaction was committed successfully
		    if (txn.commit())
		      break;					
		}		
	}
	else
	{
		System.out.println("the tuplebase connection is invalid when getting group object from KF");
	}
	
	return groupList;
	
}

/**
 * get the author_note link from KF 
 * @param tb
 * 		an tuplebase connection 
 * @return
 * 		list of Links object
 */
public List GetAuthorNoteLink(ZTB tb)
{
	List linkList = null;
	
	if(tb != null)
	{
		//create a new transaction
		ZTxn txn = new ZTxn();
		//create an object to encapsulate the tuplebase and the transaction
		ZTBTxn tbTxn = new ZTBTxn(txn,tb);
		ZTBQuery allNotes = new ZTBQuery(ZTBSpec.sEquals("Object", "note"));
		ZTBQuery allAuthor = new ZTBQuery(ZTBSpec.sEquals("Object", "author"));
		
		linkList = new ArrayList();
		
		
		for (ZTBIter authorIter = new ZTBIter(tbTxn, allAuthor); authorIter.hasValue(); authorIter.advance())
		{
	        ZID authorid = authorIter.getZID();
	        /* 
	        //for each author, get the notes he/she created
	        ZTBQuery a_nLinks= new ZTBQuery(ZTBSpec.sEquals("Link","created").and(ZTBSpec.sEquals("from",authorid)).and(new ZTBQuery("to",allNotes)));
	        ZTBQuery createNotes = new ZTBQuery(a_nLinks,"to");
	        for (ZTBIter noteLinksIter = new ZTBIter(tbTxn, createNotes); noteLinksIter.hasValue(); noteLinksIter.advance())
	        {
	        	Links linkobj = new Links();
	        	linkobj.SetFromID(authorid.longValue());
	        	linkobj.SetToID(noteLinksIter.getZID().longValue());
	        	
	        	linkList.add(linkobj);
	        	
	        }*/ 
	        
	        //get the owns note for each author
	        ZTBQuery a_nLinks= new ZTBQuery(ZTBSpec.sEquals("Link","owns").and(ZTBSpec.sEquals("from",authorid)).and(new ZTBQuery("to",allNotes)));
	        ZTBQuery ownsNotes = new ZTBQuery(a_nLinks,"to");
	        for (ZTBIter noteLinksIter = new ZTBIter(tbTxn, ownsNotes); noteLinksIter.hasValue(); noteLinksIter.advance())
	        {
	        	Links linkobj = new Links();
	        	linkobj.SetFromID(authorid.longValue());
	        	linkobj.SetToID(noteLinksIter.getZID().longValue());
	        	
	        	linkList.add(linkobj);
	        	//System.out.println("                       the author id is "+authorid.longValue());
	        	//System.out.println("                       the note id is "+noteLinksIter.getZID().longValue());
	        	
	        }
			
		}
		
	}
	else{
		
		System.out.println("GetAuthorNoteLink error");
	}
	
	return linkList;

}

/**
 * get the note_to_note link from KF 
 * @param tb
 * 		an tuplebase connection 
 * @return
 * 		list of Links object
 */
public List GetNoteNoteLink(ZTB tb)
{
	System.out.println("got into GetNoteNoteLink Function");
	List linkList = null;
	
	if(tb != null)
	{
		//create a new transaction
		ZTxn txn = new ZTxn();
		//create an object to encapsulate the tuplebase and the transaction
		ZTBTxn tbTxn = new ZTBTxn(txn,tb);
		ZTBQuery anNotes = new ZTBQuery(ZTBSpec.sEquals("Link", "annotates"));
		ZTBQuery buNotes = new ZTBQuery(ZTBSpec.sEquals("Link", "buildson"));
		ZTBQuery reNotes = new ZTBQuery(ZTBSpec.sEquals("Link", "references"));
		
		linkList = new ArrayList();
		 
		for (ZTBIter annoteIter = new ZTBIter(tbTxn, anNotes); annoteIter.hasValue(); annoteIter.advance())
		{	
			// get the current tuple in the list
	        ZTuple annoteTuple = annoteIter.get(); 
	        
	        Links linkObj = new Links();
	    	
	        linkObj.SetLink("annotates"); 
	        linkObj.SetFromID(annoteTuple.getID("from"));
	        linkObj.SetToID(annoteTuple.getID("to"));
	        
	        linkList.add(linkObj);
	        
		}
		
		
		for (ZTBIter bunoteIter = new ZTBIter(tbTxn, buNotes); bunoteIter.hasValue(); bunoteIter.advance())
		{	
			// get the current tuple in the list
	        ZTuple bunoteTuple = bunoteIter.get(); 
	        
	        Links linkObj = new Links();
	    	
	        linkObj.SetLink("buildson"); 
	        
	        linkObj.SetFromID(bunoteTuple.getID("from"));
	        
	        linkObj.SetToID(bunoteTuple.getID("to"));
	        
	        linkList.add(linkObj);
	        
		}
		
		for (ZTBIter renoteIter = new ZTBIter(tbTxn, reNotes); renoteIter.hasValue(); renoteIter.advance())
		{	
			// get the current tuple in the list
	        ZTuple renoteTuple = renoteIter.get(); 
	        
	        Links linkObj = new Links();
	    	
	        linkObj.SetLink("references"); 
	        
	        linkObj.SetFromID(renoteTuple.getID("from"));
	        
	        linkObj.SetToID(renoteTuple.getID("to"));
	        
	        linkList.add(linkObj);
	        
		}
		
	}
	else{
		
		System.out.println("GetNoteNoteLink error");
	}
	
	return linkList;

}


/**
 * get the group_to_author link from KF 
 * @param tb
 * 		an tuplebase connection 
 * @return
 * 		list of Links object
 */
public List GetGroupAuthorLink(ZTB tb)
{
	System.out.println("got into GetGroupAuthorLink Function");
    List linkList = null;
	
	if(tb != null)
	{
		//create a new transaction
		ZTxn txn = new ZTxn();
		//create an object to encapsulate the tuplebase and the transaction
		ZTBTxn tbTxn = new ZTBTxn(txn,tb);
		ZTBQuery allGroup = new ZTBQuery(ZTBSpec.sEquals("Object", "group"));
		ZTBQuery allAuthor = new ZTBQuery(ZTBSpec.sEquals("Object", "author"));
		
		linkList = new ArrayList();
		
		
		for (ZTBIter groupIter = new ZTBIter(tbTxn, allGroup); groupIter.hasValue(); groupIter.advance())
		{
	        ZID groupid = groupIter.getZID();
	        
	        //get the contains author for each group
	        ZTBQuery g_aLinks= new ZTBQuery(ZTBSpec.sEquals("Link","contains").and(ZTBSpec.sEquals("from",groupid)).and(new ZTBQuery("to",allAuthor)));
	        ZTBQuery containsAuthor = new ZTBQuery(g_aLinks,"to");
	        for (ZTBIter authorLinksIter = new ZTBIter(tbTxn, containsAuthor); authorLinksIter.hasValue(); authorLinksIter.advance())
	        {
	        	Links linkobj = new Links();
	        	linkobj.SetFromID(groupid.longValue());
	        	linkobj.SetToID(authorLinksIter.getZID().longValue());
	        	
	        	linkList.add(linkobj);
	        }
			
		}
		
	}
	else{
		
		System.out.println("GetGroupAuthorLink error");
	}
	
	return linkList;

}

/**
 * get the view_note link from KF 
 * @param tb
 * 		an tuplebase connection 
 * @return
 * 		list of Links object
 */
public List GetViewNoteLink(ZTB tb)
{
	List linkList = null;
	
	if(tb != null)
	{
		//create a new transaction
		ZTxn txn = new ZTxn();
		//create an object to encapsulate the tuplebase and the transaction
		ZTBTxn tbTxn = new ZTBTxn(txn,tb);
		ZTBQuery allNotes = new ZTBQuery(ZTBSpec.sEquals("Object", "note"));
		ZTBQuery allViews = new ZTBQuery(ZTBSpec.sEquals("Object", "view"));
		
		linkList = new ArrayList();
		
		//for each view, get the notes it contains
		for (ZTBIter viewIter = new ZTBIter(tbTxn, allViews); viewIter.hasValue(); viewIter.advance())
		{
	        ZID viewid = viewIter.getZID();
	        ZTBQuery vnLinks= new ZTBQuery(ZTBSpec.sEquals("Link","contains").and(ZTBSpec.sEquals("from",viewid)).and(new ZTBQuery("to",allNotes)));
	        ZTBQuery visibleNotes = new ZTBQuery(vnLinks,"to");
	        for (ZTBIter noteLinksIter = new ZTBIter(tbTxn, visibleNotes); noteLinksIter.hasValue(); noteLinksIter.advance())
	        {
	        	Links linkobj = new Links();
	        	linkobj.SetFromID(viewid.longValue());
	        	linkobj.SetToID(noteLinksIter.getZID().longValue());
	        	
	        	linkList.add(linkobj);
	        	
	        } 
			
		}
		
	}
	else{
		
		System.out.println("GetViewNoteLink error");
	}
	
	return linkList;

}



/**
 * get the attachment objects from KF 
 * @param tb
 * 		an tuplebase connection 
 * @return
 * 		list of attachment objects
 */
public List getAttachments(ZTB tb)
{
	System.out.println("Baibhav downloading Attachments from KF");
	List attachmentList = null;
	String strtemp;
	if(tb != null)
	{
		while(tb != null && true)
		{
			//create a new transaction
			ZTxn txn = new ZTxn();
			//create an object to encapsulate the tuplebase and the transaction
			ZTBTxn tbTxn = new ZTBTxn(txn,tb);
			
			ZTBQuery attachmentQuery = new ZTBQuery(ZTBSpec.sEquals("Object", "attachment"));
			attachmentList = new ArrayList();
			
			// Create an iterator to loop over the list of tuples that were found
		    for (ZTBIter attachmentIter = new ZTBIter(tbTxn, attachmentQuery); attachmentIter.hasValue(); attachmentIter.advance())
		    {
		    	// get the current tuple in the list
		        ZTuple attachmentTuple = attachmentIter.get(); 
		        
		       //create the class instance responding to the support table
		        AttachmentObject attachmentObj = new AttachmentObject();
		    	
		    	//get id
		        attachmentObj.SetID(attachmentIter.getZID().longValue());
		        
		        //get the title
		        attachmentObj.SetTitle(attachmentTuple.getString("titl"));
		        System.out.println(attachmentTuple.getString("titl"));
		        
		        //get the file
		        attachmentObj.SetFile(attachmentTuple.getString("file"));
		        System.out.println(attachmentTuple.getString("file"));
		        
		        //get the creation date
		        attachmentObj.SetCretime(attachmentTuple.getTime("crea"));
		        System.out.println(attachmentTuple.getString("crea"));
		        
		        attachmentList.add(attachmentObj);
		    }
		    // break out of the loop if the transaction was committed successfully
		    if (txn.commit())
		      break;					
		}		
	}
	else
	{
		System.out.println("the tuplebase connection is invalid when getting support object from KF");
	}
	
	return attachmentList;
	
}



public List GetAttachmentNoteLink(ZTB tb)
{
	List linkList = null;
	
	if(tb != null)
	{
		//create a new transaction
		ZTxn txn = new ZTxn();
		//create an object to encapsulate the tuplebase and the transaction
		ZTBTxn tbTxn = new ZTBTxn(txn,tb);
		ZTBQuery allNotes = new ZTBQuery(ZTBSpec.sEquals("Object", "note"));
		ZTBQuery allAttachment = new ZTBQuery(ZTBSpec.sEquals("Object", "attachment"));
		
		linkList = new ArrayList();
		
		//for each view, get the notes it contains
		for (ZTBIter noteIter = new ZTBIter(tbTxn, allNotes); noteIter.hasValue(); noteIter.advance())
		{
	        ZID noteid = noteIter.getZID();
	        ZTBQuery vaLinks= new ZTBQuery(ZTBSpec.sEquals("Link","contains").and(ZTBSpec.sEquals("from",noteid)).and(new ZTBQuery("to",allAttachment)));
	        ZTBQuery noteattachments = new ZTBQuery(vaLinks,"to");
	        for (ZTBIter noteLinksIter = new ZTBIter(tbTxn, noteattachments); noteLinksIter.hasValue(); noteLinksIter.advance())
	        {
	        	Links linkobj = new Links();
	        	linkobj.SetFromID(noteid.longValue());
	        	linkobj.SetToID(noteLinksIter.getZID().longValue());
	        	
	        	linkList.add(linkobj);
	        	
	        } 
			
		}
		
	}
	else{
		
		System.out.println("GetAttachmentNoteLink error");
	}
	
	return linkList;

}




}


