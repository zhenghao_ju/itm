package code;

public class StringUtils {
	
	public static boolean CheckIfDevelopment(String url)
	{
		boolean IsDevelopment = false;
		/* 
		 * Check if the given substring exists in the url.If yes then set the value of IsDevelopment to true since its development env 
		 * otherwise return false since its deployment env 
		 * 
		 * */
		IsDevelopment = url.matches("(?i).*itm[a-zA-Z]*dev.*");
		System.out.println("Is Development" + IsDevelopment);
		// = url.matches("(?i)itm[a-zA-Z]*dev");
		return IsDevelopment;
	}
        
        /**
         * Combine db's lastname and firstname together
         * @param rawName
         * @return Chinese fullname
         * @Author Stan
         */
        public static String AdjustChineseName(String rawName){
            if(rawName.contains(" ")){
             String[] fullname=rawName.split(" ");
             rawName=fullname[1]+fullname[0];
            }
            return rawName;
        }

}
