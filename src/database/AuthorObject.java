package database;
//import java.util.*;

public class AuthorObject {
	private long ID;
	private String firstname;
	private String lastname;
	private String username;
	private String type;
		
	public void SetID(long ID)
	{
		this.ID = ID;
	}
	
	public long GetID()
	{
		return this.ID;
	}
	
	public void SetFstname(String Fstname)
	{
		this.firstname = Fstname;
	}
	
	public String GetFstname()
	{
		return this.firstname;
	}
	public void SetLstname(String Lstname)
	{
		this.lastname = Lstname;
	}
	
	public String GetLstname()
	{
		return this.lastname;
	}
	public void SetUsname(String Usname)
	{
		this.username = Usname;
	}
	
	public String GetUsname()
	{
		return this.username;
	}
	
	public void SetType(String strtype)
	{
		this.type = strtype;
	}
	
	public String GetType()
	{
		return this.type;
	}	

}
