package database;

import java.sql.*;

import code.sqls;
import java.text.SimpleDateFormat;

public class CreateDatabase {
	private String dbname=null; //the database need to be created
	private Statement DBConn = null; //connection to itm
	private sqls sobj = null;
	
	/**
	 * 
	 * @param strname
	 * database name
	 * @param sobj
	 * a connection to itm database 
	 */
	public CreateDatabase(String strname){
		this.sobj = new sqls();
		dbname = strname;
		DBConn = this.sobj.Connect("itm"); //return a connection to database of itm
	}
	
	public void CloseCon(){
		this.sobj.Close();
	}
	
	private Statement GetNewCon(String dbname){
		this.sobj = new sqls();
		return this.sobj.Connect(dbname);
	}
	
	/**
	 * Check if the DATABASE is already exists
	 * @param strdb
	 * database name
	 * @return
	 * true if the database is already exists, otherwise false
	 */
	public boolean CheckDB(){
        String strsql = "select refreshtime from itm.db_table where dbname='"+this.dbname+"'";
        ResultSet rs = null;
        //int num=0;
        Date date = null;
        try{
            Statement test=this.sobj.Connect(dbname);
            if(test==null)
                return false;
            
            rs = this.DBConn.executeQuery(strsql);
            if(rs.next()){
                //num = rs.getInt(1);
                date = rs.getDate(1);
            }
            
        }catch(SQLException e){
            e.printStackTrace();            
        }finally{
            if(rs != null){
                try{
                    rs.close();
                }catch(SQLException e){
                    e.printStackTrace();
                }
            }
        }
        Date date2 = new Date(70, 0, 1);
        
        System.out.println(date.compareTo(date2));
        if(date.compareTo(date2) == 0) {
            return false;
        }
    
        return true;
    }
	/**
	 * Create New Database
	 * @return
	 */
	public boolean CreateDB(){
		Connection con = null;
        String url = "jdbc:mysql://localhost:3306/";
        String driver = "com.mysql.jdbc.Driver";
        String user = "root";
        //String pass = "root";
        String pass = "051633b$";
        String strsql = null;
        Statement stmt = null;
        try {
        	System.out.println("Test Database creation");
        	//create database
            Class.forName(driver); 
            con = DriverManager.getConnection(url, user, pass);
            stmt = con.createStatement();
            strsql = "drop schema if exists "+this.dbname;
            stmt.executeUpdate(strsql);
            strsql = "create database if not exists "+this.dbname;
            stmt.executeUpdate(strsql);
            
            //write the database name into db_table
            this.RecordDB();
            
            //create tables
            this.CreateTables(this.dbname);
        }
        catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
            return false;
        }finally{
        	if(stmt != null){
        		try{
        			stmt.close();
        		}catch(SQLException e){
        			e.printStackTrace();
        		}
        	}
        	
        	if(con != null){
        		try{
        			con.close();
        		}catch(SQLException e){
        			e.printStackTrace();
        		}
        	}
        }
        
        return true;
	}
	/**
	 * Write the created time into itm.db_table
	 */
	private void RecordDB(){
		String strsql=null;
		String strtime=null;
		
	    java.util.Date curdate = new java.util.Date();	
		SimpleDateFormat spdtformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		strtime = spdtformat.format(curdate);
		//strsql = "insert itm.db_table (dbname, refreshtime) values('"+this.dbname+"','"+strtime+"')";
                   strsql = "update itm.db_table set refreshtime='"+strtime+"' where dbname='"+this.dbname+"'";
		System.out.println(strsql);
		try{
			this.DBConn.executeUpdate(strsql);
		}catch(Exception e){
			e.printStackTrace();
			
		}
		
	}
	
	/**
	 * Store the connection information of KF database into itm.connection_table
	 * @param URL
	 * @param strdb
	 * @param struser
	 * user name
	 * @param strpsw
	 * password
	 * @param port
	 */
	public void RecordConInfor(String URL, String strdb,String struser,String strpsw,String localdb,int port){
		String strsql = "insert connection_table (kfdb,URL,username,password,localdb,port) values('"+strdb+
		"','"+URL+"','"+struser+"','"+strpsw+"','"+localdb+"',"+port+")";
		System.out.println(strsql);
		try{
			this.DBConn.executeUpdate(strsql);
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Get the database's last refresh time
	 * @return
	 * refresh time
	 */
	public String GetRefreshTime(){
		String strsql = "select refreshtime from itm.db_table where dbname='"+this.dbname+"'";
		ResultSet rs = null;
		try{
			rs = this.DBConn.executeQuery(strsql);
			if(rs.next()){
				return rs.getString(1);		
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return null;
	}
	 /**
     * Create empty tables;
     * @return
     */
    private int CreateTables(String dbname){
        
        Statement st = null;
        String strsql = null;
        try{
            st = this.GetNewCon(dbname);
            
            //create "project" table
            strsql = "DROP TABLE IF EXISTS `project`";    
            st.executeUpdate(strsql);
            strsql = "CREATE TABLE `project` (" +
                    "`idProject` int(11) NOT NULL AUTO_INCREMENT," +
                    "`projectname` varchar(200) DEFAULT NULL," +
                    "`teacher` varchar(200) DEFAULT NULL," +
                    "`school` varchar(200) DEFAULT NULL," +
                    "`fromyear` varchar(20) DEFAULT NULL," +
                    "`toyear` varchar(20) DEFAULT NULL," +
                    "`projectowner` varchar(200) DEFAULT NULL,"+
                    "`deleted` int(11),"+
                    "PRIMARY KEY (`idProject`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
            st.executeUpdate(strsql);
            
            //create project_thread table
            strsql = "DROP TABLE IF EXISTS `project_thread`";
            st.executeUpdate(strsql);
            strsql = "CREATE TABLE `project_thread` " +
                    "(`projectid` int(11) NOT NULL," +
                    "`threadfocus` varchar(100) NOT NULL," +
                    "`author` varchar(100) DEFAULT NULL," +
                    "KEY `project_thread_fk` (`projectid`)," +
                    "KEY `project_thread_fk1` (`projectid`)," +
                    "CONSTRAINT `project_thread_fk1` " +
                    "FOREIGN KEY (`projectid`) REFERENCES `project` (`idProject`) " +
                    "ON DELETE CASCADE ON UPDATE CASCADE) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
            st.executeUpdate(strsql);
            
            //create note_note table
            strsql = "DROP TABLE IF EXISTS `note_note`";
            st.executeUpdate(strsql);
            strsql = "CREATE TABLE `note_note` " +
                    "(`fromnoteid` int(11) DEFAULT NULL," +
                    "`tonoteid` int(11) DEFAULT NULL," +
                    "`linktype` varchar(45) DEFAULT NULL," +
                    "KEY `fromid_FK` (`fromnoteid`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
            st.executeUpdate(strsql);
            
            //create note_table
            strsql = "DROP TABLE IF EXISTS `note_table`";
            st.executeUpdate(strsql);
            strsql = "CREATE TABLE `note_table` " +
                    "(`noteid` int(11) NOT NULL," +
                    "`notetitle` varchar(200) DEFAULT NULL," +
                    "`notecontent` mediumtext," +
                    "`offset` varchar(200) DEFAULT NULL," +
                    "`createtime` datetime DEFAULT NULL," +
                    "PRIMARY KEY (`noteid`)) ENGINE=InnoDB DEFAULT CHARSET=utf8";
            st.executeUpdate(strsql);
            
            //create thread_note
            strsql = "DROP TABLE IF EXISTS `thread_note`";
            st.executeUpdate(strsql);
            strsql = "CREATE TABLE `thread_note` " +
                    "(`projectid` int(11) NOT NULL," +
                    "`threadfocus` varchar(100) NOT NULL," +
                    "`noteid` int(11) NOT NULL," +
                    "`highlight` int(11) NOT NULL DEFAULT '0'," +
                    "`addtime` datetime NOT NULL,"+
                    "KEY `thread_note_fk` (`projectid`)," +
                    "KEY `thread_note_fk2` (`noteid`)," +
                    "CONSTRAINT `thread_note_fk1` " +
                    "FOREIGN KEY (`projectid`) REFERENCES `project` (`idProject`) " +
                    "ON DELETE CASCADE ON UPDATE CASCADE," +
                    "CONSTRAINT `thread_note_fk2` FOREIGN KEY (`noteid`) " +
                    "REFERENCES `note_table` (`noteid`) ON DELETE CASCADE ON UPDATE CASCADE)" +
                    " ENGINE=InnoDB DEFAULT CHARSET=utf8;";
            st.executeUpdate(strsql);
            
            //create thread_more table
            strsql = "DROP TABLE IF EXISTS `thread_more`";
            st.executeUpdate(strsql);
            strsql = "CREATE TABLE `thread_more` " +
                    "(`projectid` int(11) NOT NULL," +
                    "`threadfocus_more` varchar(100) NOT NULL," +
                    "`createtime` datetime NOT NULL," +
                    "`more` text,`author_more` varchar(45) DEFAULT NULL," +
                    "KEY `thread_more_fk` (`projectid`)," +
                    "CONSTRAINT `thread_more_fk` FOREIGN KEY (`projectid`) REFERENCES `project` (`idProject`) ON DELETE CASCADE ON UPDATE CASCADE) " +
                    "ENGINE=InnoDB DEFAULT CHARSET=utf8;";
            st.executeUpdate(strsql);
            
            //create project_grade
            strsql = "DROP TABLE IF EXISTS `project_grade`";
            st.executeUpdate(strsql);
            strsql = "CREATE TABLE `project_grade` " +
                    "(`projectid` int(11) DEFAULT NULL," +
                    "`grade` varchar(45) DEFAULT NULL," +
                    "KEY `proid1_fk` (`projectid`)," +
                    "CONSTRAINT `proid1_fk` FOREIGN KEY (`projectid`) REFERENCES `project` (`idProject`) ON DELETE CASCADE ON UPDATE CASCADE) " +
                    "ENGINE=InnoDB DEFAULT CHARSET=utf8;";
            st.executeUpdate(strsql);
            
            //create view_table
            strsql = "DROP TABLE IF EXISTS `view_table`";
            st.executeUpdate(strsql);
            strsql = "CREATE TABLE `view_table` " +
                    "(`idview` int(11) NOT NULL," +
                    "`title` varchar(450) DEFAULT NULL," +
                    "PRIMARY KEY (`idview`)) ENGINE=InnoDB DEFAULT CHARSET=utf8";
            st.executeUpdate(strsql);
            
            //create view_note table
            strsql = "DROP TABLE IF EXISTS `view_note`";
            st.executeUpdate(strsql);
            strsql = "CREATE TABLE `view_note` " +
                    "(`noteid` int(11) DEFAULT NULL," +
                    "`viewid` int(11) DEFAULT NULL," +
                    "KEY `noteid_FK` (`noteid`)," +
                    "KEY `viewid_FK` (`viewid`)," +
                    "CONSTRAINT `noteid_FK` FOREIGN KEY (`noteid`) REFERENCES `note_table` (`noteid`) ON DELETE CASCADE ON UPDATE CASCADE," +
                    "CONSTRAINT `viewid_FK` FOREIGN KEY (`viewid`) REFERENCES `view_table` (`idview`) ON DELETE CASCADE ON UPDATE CASCADE) " +
                    "ENGINE=InnoDB DEFAULT CHARSET=utf8;";
            st.executeUpdate(strsql);
            
            //create thread_problem table
            strsql = "DROP TABLE IF EXISTS `thread_problem`";
            st.executeUpdate(strsql);
            strsql = "CREATE TABLE `thread_problem` " +
                    "(`projectid` int(11) NOT NULL," +
                    "`threadfocus_problem` varchar(100) NOT NULL," +
                    "`createtime` datetime NOT NULL," +
                    "`problem` text," +
                    "`author_problem` varchar(45) DEFAULT NULL," +
                    "KEY `thread_problem_fk` (`projectid`)) ENGINE=InnoDB DEFAULT CHARSET=utf8";
            st.executeUpdate(strsql);
            
            //create group_table
            strsql = "DROP TABLE IF EXISTS `group_table`";
            st.executeUpdate(strsql);
            strsql = "CREATE TABLE `group_table` " +
                    "(`idgroup` int(11) NOT NULL," +
                    "`title` varchar(45) DEFAULT NULL,PRIMARY KEY (`idgroup`)) ENGINE=InnoDB DEFAULT CHARSET=utf8";
            st.executeUpdate(strsql);
            
            //create author_table
            strsql = "DROP TABLE IF EXISTS `author_table`";
            st.executeUpdate(strsql);
            strsql = "CREATE TABLE `author_table` " +
                    "(`authorid` int(11) NOT NULL," +
                    "`firstname` varchar(45) DEFAULT NULL," +
                    "`lastname` varchar(45) DEFAULT NULL," +
                    "`username` varchar(45) DEFAULT NULL," +
                    "`type` varchar(45) DEFAULT NULL," +
                    "PRIMARY KEY (`authorid`)) ENGINE=InnoDB DEFAULT CHARSET=utf8";
            st.executeUpdate(strsql);
            
            //create project_group
            strsql = "DROP TABLE IF EXISTS `project_group`";
            st.executeUpdate(strsql);
            strsql ="CREATE TABLE `project_group` " +
                    "(`projectid` int(11) DEFAULT NULL," +
                    "`groupid` int(11) DEFAULT NULL," +
                    "KEY `proid_fk` (`projectid`)," +
                    "KEY `group_fk` (`groupid`)," +
                    "KEY `project_fk` (`projectid`)," +
                    "CONSTRAINT `group_fk` FOREIGN KEY (`groupid`) REFERENCES `group_table` (`idgroup`) ON DELETE CASCADE ON UPDATE CASCADE," +
                    "CONSTRAINT `project_fk` FOREIGN KEY (`projectid`) REFERENCES `project` (`idProject`) ON DELETE CASCADE ON UPDATE CASCADE) " +
                    "ENGINE=InnoDB DEFAULT CHARSET=utf8";
            st.executeUpdate(strsql);
            
            //create author_note table
            strsql = "DROP TABLE IF EXISTS `author_note`";
            st.executeUpdate(strsql);
            strsql = "CREATE TABLE `author_note` " +
                    "(`authorid` int(11) DEFAULT NULL," +
                    "`noteid` int(11) DEFAULT NULL," +
                    "KEY `authorid_FK` (`authorid`)," +
                    "KEY `notedid_FK` (`noteid`)," +
                    "CONSTRAINT `authorid_FK` FOREIGN KEY (`authorid`) REFERENCES `author_table` (`authorid`) ON DELETE CASCADE ON UPDATE CASCADE," +
                    "CONSTRAINT `notedid_FK` FOREIGN KEY (`noteid`) REFERENCES `note_table` (`noteid`) ON DELETE CASCADE ON UPDATE CASCADE) " +
                    "ENGINE=InnoDB DEFAULT CHARSET=utf8";
            st.executeUpdate(strsql);
            
            //create group_author table
            strsql = "DROP TABLE IF EXISTS `group_author`";
            st.executeUpdate(strsql);
            strsql = "CREATE TABLE `group_author` " +
                    "(`group_id` int(11) DEFAULT NULL," +
                    "`author_id` int(11) DEFAULT NULL," +
                    "KEY `groupid_FK` (`group_id`)," +
                    "KEY `authorid_FK` (`author_id`)," +
                    "CONSTRAINT `group_id_FK` FOREIGN KEY (`group_id`) REFERENCES `group_table` (`idgroup`) ON DELETE CASCADE ON UPDATE CASCADE," +
                    "CONSTRAINT `author_id_FK` FOREIGN KEY (`author_id`) REFERENCES `author_table` (`authorid`) ON DELETE CASCADE ON UPDATE CASCADE) " +
                    "ENGINE=InnoDB DEFAULT CHARSET=utf8";
            st.executeUpdate(strsql);
            
            //create thread_idea
            strsql = "DROP TABLE IF EXISTS `thread_idea`";
            st.executeUpdate(strsql);
            strsql="CREATE TABLE `thread_idea` " +
                    "(`projectid` int(11) NOT NULL," +
                    "`threadfocus_idea` varchar(100) NOT NULL," +
                    "`createtime` datetime NOT NULL," +
                    "`idea` text," +
                    "`author_idea` varchar(45) DEFAULT NULL," +
                    "KEY `thread_idea_fk` (`projectid`)) ENGINE=InnoDB DEFAULT CHARSET=utf8";
            st.executeUpdate(strsql);
            
            //create thread_table
            strsql = "DROP TABLE IF EXISTS `thread_table`";
            st.executeUpdate(strsql);
            strsql = "CREATE TABLE `thread_table` " +
                    "(`threadfocus` varchar(45) NOT NULL," +
                    "PRIMARY KEY (`threadfocus`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
            st.executeUpdate(strsql);
            
            //create support_table
            strsql = "DROP TABLE IF EXISTS `support_table`";
            st.executeUpdate(strsql);
            strsql = "CREATE  TABLE `support_table` " +
                    "(`supportid` INT NOT NULL ," +
                    "`text` VARCHAR(200) NULL ,PRIMARY KEY (`supportid`) )ENGINE=InnoDB DEFAULT CHARSET=utf8;";
            st.executeUpdate(strsql);
            
            //create the support_note table
            strsql = "DROP TABLE IF EXISTS `support_note`";
            st.executeUpdate(strsql);
            strsql = "CREATE TABLE `support_note` " +
                    "(`supportid` int(11) NOT NULL," +
                    "`noteid` int(11) NOT NULL, " +
                    "`text` text,"+
                    "KEY `supportid_fk1` (`supportid`)," +
                    "KEY `noteid_fk2` (`noteid`)," +
                    "CONSTRAINT `supportid_fk1` FOREIGN KEY (`supportid`) REFERENCES `support_table` (`supportid`) " +
                    "ON DELETE CASCADE ON UPDATE CASCADE," +
                    "CONSTRAINT `noteid_fk2` FOREIGN KEY (`noteid`) REFERENCES `note_table` (`noteid`) " +
                    "ON DELETE CASCADE ON UPDATE CASCADE) ENGINE=InnoDB DEFAULT CHARSET=utf8";
            st.executeUpdate(strsql);
            
            //create attachment_table
            strsql = "DROP TABLE IF EXISTS `attachment_table`";
            st.executeUpdate(strsql);
            strsql = "CREATE TABLE `attachment_table` " +
                    "(`attachmentid` int(11) NOT NULL," +
                    "`attachmenttitle` varchar(200) DEFAULT NULL," +
                    "`attachmentfile` varchar(200) DEFAULT NULL," +
                    "`createtime` datetime DEFAULT NULL," +
                    "PRIMARY KEY (`attachmentid`)) ENGINE=InnoDB DEFAULT CHARSET=utf8";
            st.executeUpdate(strsql);
            
            //create attachment_note table
            strsql = "DROP TABLE IF EXISTS `attachment_note`";
            st.executeUpdate(strsql);
            strsql = "CREATE TABLE `attachment_note` " +
                    "(`noteid` int(11) DEFAULT NULL," +
                    "`attachmentid` int(11) DEFAULT NULL) " +
                    "ENGINE=InnoDB DEFAULT CHARSET=utf8;";
            st.executeUpdate(strsql);
            
            
        
        }catch(Exception e){
            e.printStackTrace();
            return 1;
        }finally{
            if(st != null){
                try{
                    st.close();
                    this.CloseCon();
                }catch(SQLException e){
                    e.printStackTrace();
                }
            }
        }
        return 0;
    }
}
