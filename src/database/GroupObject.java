package database;

public class GroupObject {
	private long ID;
	private String title;
	
	public void SetID(long ID)
	{
		this.ID = ID;
	}
	
	public long GetID()
	{
		return this.ID;
	}
	
	public void SetTitle(String title)
	{
		this.title = title;
	}
	
	public String GetTitle()
	{
		return this.title;
	}

}
