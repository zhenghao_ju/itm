package database;
import itm.controllers.Stemmer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import code.*;

import java.io.*;

import javax.sql.rowset.CachedRowSet;

import com.sun.rowset.CachedRowSetImpl;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Operatedb  {
	
	private Statement DBConn = null;
	private String curdb = null;
	private sqls conobj = null;
	
	public Statement getC(){
		return this.DBConn;
	}
	
	public Operatedb(sqls sobj,String strname)
	{
		this.curdb = strname;
		DBConn = sobj.Connect(strname);
		this.conobj = sobj;
	}
	
	public void CloseCon(){
		try{
			if(this.DBConn != null){
				this.DBConn.close();
			}
			if(this.conobj!=null){
				this.conobj.Close();
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	/**
	 * Get Records from single table in database
	 * @param strtable
	 * 		  table name
	 * @param strcolumn
	 * 		  column name
	 * @param strconstraint
	 * 		  constraint
	 * @return	
	 * 		  record set
	 */
	public ResultSet GetRecordsFromDB(String strtable,String strcolumn, String strconstraint)
	{
		ResultSet rs = null;
		String strsql;
		
		if(strconstraint.equals("")) {
			strsql = "SELECT " + strcolumn +" FROM "+ "`"+strtable+"`" ;
		}else {
			strsql = "SELECT " + strcolumn + " FROM "+ "`"+strtable+"`"+"WHERE " +strconstraint;		
		}
		
		try{
			// System.out.println(strsql);
			rs = this.DBConn.executeQuery(strsql);
			System.out.println("Resultset"+rs);
		}catch(SQLException e){
			System.out.println("GetRecordsFromDB error");
			e.printStackTrace();		
		}
		
		return rs;	
	}
	
	
	/**
	 * Get multiple Records from single table in database
	 * @param strtable
	 * 		  table name
	 * @param String[] 
	 *        column names
	 * @param strconstraint
	 * 		  constraint
	 * @return	
	 * 		  record set
	 */
	public ResultSet GetMultipleRecordsFromDB(String strtable,String strcolumns[], String strconstraint)
	{
		ResultSet rs = null;
		String strsql;
		String str = "";
		
		for(int i = 0; i < strcolumns.length; i++)
		{
			if(i == (strcolumns.length - 1) )
			{
				str += strcolumns[i];
			}
			else
			{
		    str += strcolumns[i] +",";
			}
		}
		
		if(strconstraint.equals("")) 
		{
			strsql = "SELECT " + str + " FROM " + "`" + strtable + "`" ;
			  
		}else 
		{
			strsql = "SELECT " + str + " FROM "+ "`"+strtable+"`"+"WHERE " + strconstraint;
		}
		
		try
		{
			rs = this.DBConn.executeQuery(strsql);
		}
		catch(SQLException e)
		{
			System.out.println("GetRecordsFromDB error");
			e.printStackTrace();		
		}
		
		return rs;
	}
	
	/**
	 * Get Records from multiple tables in database
	 * @param strtable
	 * 		  table names
	 * @param strcolumn
	 * 		  column name
	 * @param strconstraint
	 * 		  constraint
	 * @param num
	 * 		  table number
	 * @return	
	 * 		  record set
	 */
	public ResultSet MulGetRecordsFromDB(String[] strtable,String strcolumn, String strconstraint,int num)
	{
		ResultSet rs = null;
		String strsql;
		String strname = null;
		strname = "`"+strtable[0]+"`";
		//get the sql
		for(int i=1; i<num; i++){
			if(i<num-1){
				strname = strname + ", " + "`" + strtable[i] + "` ";
			}else if(i==num-1){
				strname = strname + ", " + "`" + strtable[i] + "` ";
			}
		}
		if(strconstraint == null)
		{
			strsql = "SELECT " + strcolumn +" FROM "+ strname ;
		}else{
			strsql = "SELECT " + strcolumn + " FROM "+ strname+" WHERE " +strconstraint;
		}
		
		try{
			//System.out.println(strsql);
			rs = this.DBConn.executeQuery(strsql);
		}catch(SQLException e){
			System.out.println("GetRecordsFromDB error");
			e.printStackTrace();		
		}
		
		return rs;
		

	}
	
	/*
	 * Write records into database
	 * @param strtable
	 * 		table name
	 * @param stropt
	 * 		operation string, could be "DELETE","INSERT" and "UPDATE"
	 * @param strvalue
	 * 		the corresponding field and values need to be changed 
	 * */
	public void WritebacktoDB(String strtable, String stropt, String strvalue){
		String strsql = null;
		
		if(stropt.compareTo("INSERT")== 0)
		{
			strsql = "INSERT INTO " + "`"+strtable+"`"+strvalue;
		}
		else if(stropt.compareTo("UPDATE")== 0)
		{
			
		}
		
		try{
			//System.out.println("the sql is :" + strsql);
			this.DBConn.executeUpdate(strsql);
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
	
	/**
	 * add new project into database
	 * @param proname
	 * 		project name
	 * @param strteacher
	 * 		teacher name
	 * @param strschool
	 * 		school name
	 * @param arraygrade
	 * 		grade array
	 * @param arraygroup
	 * 		group array
	 * @param fromyear
	 * 		from school year
	 * @param toyear
	 * 		to school year
	 * @return 
	 * 		0: successfully
	 * 		1: duplicate project name
	 * 		5: do not get the project id successfully   
	 */
	public int WriteNewProject(String proname, String strteacher, String strschool,String[] arraygrade, String[]arraygroup,String fromyear, String toyear,String username,int deleted){
		String strvalue = null;
		ResultSet rs = null;
		int proid = 0;
		int groupid = 0;
		int i;
		//System.out.println("**************************going into write new project process**********************");
		//check if have duplicate project name
		
		try{
			rs = this.DBConn.executeQuery("SELECT * from project where projectname ='"+proname+"';");
			
			rs.last();
			if(rs.getRow()>0){
		//System.out.println("******************going out from write new project process with error code 1***********************");
				return 1;
			}
		}catch(SQLException e){
			System.out.println("error1");
			e.printStackTrace();	
			e.getErrorCode();
		}finally{
			if(rs != null){
				try{
					rs.close();
				}catch(SQLException e){
					e.printStackTrace();
				}
				
			}
		}
		
		//write the relevant information into project table
		strvalue = " (`projectname`, `teacher`,`school`,`fromyear`,`toyear`,`projectowner`,`deleted`) VALUES ('"+proname+"' ,"+"'" +strteacher+"'," + "'"+strschool+"',"+"'"+fromyear+"',"+"'"+toyear+"',"+"'"+username+"',"+"'"+deleted+"');";
		this.WritebacktoDB("project", "INSERT", strvalue);
		
		//get the project id from the project table
		try{
			rs = this.DBConn.executeQuery("SELECT idproject from project where projectname ='"+proname+"';");
			if(rs.next()){
				proid = rs.getInt(1);
				//System.out.println("the proid is "+proid);
			}else{
				//System.out.println("******************going out from write new project process with error code 5***********************");
				return 5;
			}
		}catch(SQLException e){
			System.out.println("error2");
			e.printStackTrace();		
		}finally{
			if(rs != null){
				try{
					rs.close();
				}catch(SQLException e){
					e.printStackTrace();
				}
				
			}
		}
		
		//write the records into project_grade table
		for(i=0; i< arraygrade.length;i++)
		{
			//System.out.println("user choosed "+arraygrade.length+" grades");
			//System.out.println("the "+i+"th grade is "+arraygrade[i]);
			strvalue = "(`projectid`, `grade`) VALUES ("+proid+" ,"+"'" +arraygrade[i]+"');";
			//System.out.println(strvalue);
			this.WritebacktoDB("project_grade", "INSERT", strvalue);
			
			
			
		}
		
		//write the records into project_group table
		for(i=0; i<arraygroup.length;i++)
		{
			//get the group id from the group table
			try{
			rs = this.DBConn.executeQuery("SELECT idgroup from `group_table` where title ='"+arraygroup[i]+"';");
			//System.out.println("SELECT idgroup from `itm`.`group_table` where title ='"+arraygroup[i]+"';");
			if(rs.next())
			{
				groupid = rs.getInt(1);
			}			
			strvalue = "(`projectid`, `groupid`) VALUES ("+proid+" ," +groupid+");";
			
			this.WritebacktoDB("project_group", "INSERT", strvalue);
			
			}catch(SQLException e){
				System.out.println("error3");
				e.printStackTrace();		
			}finally{
				if(rs != null){
					try{
						rs.close();
					}catch(SQLException e){
						e.printStackTrace();
					}
					
				}
			} 
			
		}
		
		//System.out.println("******************going out from write new project process successfully***********************");
		return 0;
		
	}
	
	/**
	 * store thread note into database
	 * @param proname
	 * 		project name
	 * @param strthread
	 * 		thread focus
	 * @param arrayid
	 * 		note id array
	 * @return 
	 * 		0: successfully
	 * 		1: error 
	 */
	public int WriteThreadNote(String proname, String strthread, String[] arrayid){
		String strvalue = null;
		String strproid = null;
		ResultSet temprs = null;
		ResultSet rs1=null;
		int iResult;
		int i;
		//System.out.println("**************************going into store thread notes process**********************");
		//System.out.println();
		//check if the thread(containing some notes) is already in the thread_note table, 
		//if so, delete the thread in thread_note table
		try{
			
			temprs=this.DBConn.executeQuery("SELECT idproject FROM project WHERE projectname='"+proname+"';");
			if (temprs.next())
			{
				strproid = temprs.getString(1);
			}
			
			strvalue = "SELECT * from thread_note where thread_note.projectid = " +strproid+ " and thread_note.threadfocus = '"+ strthread+"';";
			//System.out.println(strvalue);
			rs1 = this.DBConn.executeQuery(strvalue);
			rs1.next();
			//System.out.println("the rs.getRow is "+rs1.getRow());
			
			if(rs1.getRow()>0){
			//System.out.println("******************the thread is already in the thread_note table***********************");
			//delete all notes containing in the thread
			strvalue ="DELETE FROM thread_note WHERE threadfocus = '"+strthread+"' AND projectid="+strproid+";";
			//System.out.println(strvalue);
		    iResult = this.DBConn.executeUpdate(strvalue);
			}
			
			for(i=0; i< arrayid.length;i++){
					strvalue = "(projectid,`threadfocus`, `noteid`) VALUES ("+strproid+",'"+strthread+"' ,"+arrayid[i]+");";
					this.WritebacktoDB("thread_note", "INSERT", strvalue);						
			}	
		}catch(SQLException e){
			e.printStackTrace();	
			e.getErrorCode();
		}
		return 0;
		
	}
	
	/**
	 * Get all notes contained in view chosen by user
	 * @param strfrtime
	 * 		  from time
	 * @param strtotime
	 * 		  to time 
	 * @param strview
	 * 		  view title
	 * @param strword1
	 * 		  key word in content
	 * @param strword2
	 * 		  key word in title
	 * @param strrange
	 * 		  key word range
	 * @return
	 * 		  record set
	 */
	public ResultSet GetNotes(String thread, String strfrtime, String strtotime, String[] strtitle, String strword1,String strword2,String strrange,String match)
	{
		//Get the notes which are contained by the view user chosen
		ResultSet rs = null;
		try{
		//sqls s=new sqls();
		//Statement stmt=s.Connect(this.curdb);
		String strsql=null;
		String viewsql = null, keysql="", timesql=null;
		boolean ifall=false;
		String[] viewtitle= new String[strtitle.length];
		
		viewsql = "(view_table.title='";
		for(int i=0; i<strtitle.length;i++){
			viewtitle[i]=strtitle[i].replace("'", "''");
			if(viewtitle[i].equals("所有视窗")){
				ifall = true;
				break;
			}
			if(i<strtitle.length-1){
				viewsql = viewsql + viewtitle[i]+"' or view_table.title= '";
			}else{
				viewsql = viewsql + viewtitle[i]+"'";
			}
			
		}
		
		viewsql += ")";
		
		if(match.contentEquals("exact")){
			if(strrange.indexOf("任何地方")!=-1){
				keysql = " AND (note_table.notecontent LIKE '%" +strword1+"%'" +"OR note_table.notetitle LIKE '%"+strword2+"%')";
			}else if(strrange.equals("短文标题")){
				keysql = " AND note_table.notetitle LIKE '%"+strword2+"%'" ;
			}else if(strrange.equals("短文内容")){
				keysql = " AND note_table.notecontent LIKE '%" +strword1+"%'";
			}
		}
		
				
		if(!strfrtime.equals("earliest")&& !strtotime.equals("latest")){
			timesql = " AND note_table.createtime >'"+strfrtime+"'"
			+" AND note_table.createtime < '"+strtotime+"'";
		}else if(strfrtime.equals("earliest") && strtotime.equals("latest")){
			timesql =" " ;
		}else if(strfrtime.equals("earliest")&& !strtotime.equals("latest")){
			timesql = " AND note_table.createtime <'"+strtotime+"'";
		}else if(!strfrtime.equals("earliest")&& strtotime.equals("latest")){
			timesql =" AND note_table.createtime > '"+strfrtime+"'";
		}
		/*
		if(!strfrtime.equals("earliest")&& !strtotime.equals("latest")&&!ifall){
			strsql = "SELECT note_table.notetitle,view_table.title,DATE(note_table.createtime), note_table.noteid, author_table.firstname,author_table.lastname,SUBSTRING_INDEX(note_table.notecontent, ' ', 20),note_table.notecontent FROM `note_table` ,view_note, view_table, author_table, author_note WHERE "
				+viewsql+" AND view_table.idview=view_note.viewid AND note_table.noteid=view_note.noteid AND note_table.noteid=`author_note`.`noteid` AND author_table.authorid = author_note.authorid AND note_table.createtime >'"+strfrtime+"'"
				+" AND note_table.createtime < '"+strtotime+"'" + keysql +";" ;
		
		}else if(!strfrtime.equals("earliest")&&!strtotime.equals("latest")&& ifall){
			
			strsql = "SELECT note_table.notetitle,view_table.title,DATE(note_table.createtime), note_table.noteid, author_table.firstname,author_table.lastname,SUBSTRING_INDEX(note_table.notecontent, ' ', 20),note_table.notecontent "+
					"FROM note_table ,view_table,author_table, view_note,author_note" +
					" WHERE note_table.noteid=author_note.noteid AND author_table.authorid = author_note.authorid AND view_table.idview = view_note.viewid AND note_table.noteid = view_note.noteid AND note_table.createtime >'"
				+strfrtime+"'"+" AND note_table.createtime < '"+strtotime+"'" + keysql +";" ;
			
		}else if(strfrtime.equals("earliest")&& strtotime.equals("latest")&& ifall){
			strsql = "SELECT note_table.notetitle,view_table.title,DATE(note_table.createtime), note_table.noteid, author_table.firstname,author_table.lastname,SUBSTRING_INDEX(note_table.notecontent, ' ', 20),note_table.notecontent " +
					"FROM `note_table` ,author_table,view_table,view_note, author_note WHERE view_table.idview = view_note.viewid and note_table.noteid=`author_note`.`noteid` AND author_table.authorid = author_note.authorid AND view_note.noteid=note_table.noteid"+keysql+";" ;
			
		
		}else if(strfrtime.equals("earliest")&& strtotime.equals("latest")&& !ifall){
			strsql = "SELECT note_table.notetitle,view_table.title,DATE(note_table.createtime), note_table.noteid, author_table.firstname,author_table.lastname,SUBSTRING_INDEX(note_table.notecontent, ' ', 20),note_table.notecontent FROM `note_table` ,view_note, view_table, author_table, author_note WHERE "+viewsql+" AND view_table.idview=view_note.viewid AND note_table.noteid=view_note.noteid AND note_table.noteid=`author_note`.`noteid` AND author_table.authorid = author_note.authorid"+keysql+";" ;
			
		}*/
		
		if(!ifall){
//			strsql = "SELECT note_table.notetitle,view_table.title,DATE(note_table.createtime), note_table.noteid,SUBSTRING_INDEX(note_table.notecontent, ' ', 20),note_table.notecontent FROM `note_table` ,view_note, view_table WHERE "
//				+viewsql+" AND view_table.idview=view_note.viewid AND note_table.noteid=view_note.noteid"+ timesql + keysql +";" ;
                        strsql= "SELECT  note_table.notetitle,view_table.title,DATE(note_table.createtime), note_table.noteid,note_table.notecontent from note_table,view_note, view_table "
                                + "WHERE NOT EXISTS (SELECT * FROM thread_note WHERE thread_note.threadfocus='"
                                +thread+"' and note_table.noteid = thread_note.noteid) and "
                                + viewsql
                                + "AND view_table.idview=view_note.viewid "
                                + "AND note_table.noteid=view_note.noteid  "
                                + timesql+ keysql+";";       
  
		}else if(ifall){
			
			strsql = "SELECT note_table.notetitle,view_table.title,DATE(note_table.createtime), note_table.noteid,SUBSTRING_INDEX(note_table.notecontent, ' ', 20),note_table.notecontent "+
					"FROM note_table ,view_table,view_note" +
					" WHERE view_table.idview = view_note.viewid AND note_table.noteid = view_note.noteid"+ timesql + keysql +";" ;
			
		}	
		
		System.out.println(strsql);
	    rs = this.DBConn.executeQuery(strsql);
	   	
		}catch(SQLException e){
			System.out.println("error:when get the notes which are contained by the view user chosen");
			e.printStackTrace();			
		}
		
		return rs;
		
	}
	
	
	public CachedRowSet GetStemmedNotes(String strfrtime, String strtotime, String[] strtitle, String strword1,String strword2,String strrange) throws SQLException
	{
		//Get the notes which are contained by the view user chosen
		ResultSet rs = null;
		try{
		//sqls s=new sqls();
		//Statement stmt=s.Connect(this.curdb);
		String strsql=null;
		String viewsql = null, keysql="", timesql=null;
		boolean ifall=false;
		String[] viewtitle= new String[strtitle.length];
		
		viewsql = "(view_table.title='";
		for(int i=0; i<strtitle.length;i++){
			viewtitle[i]=strtitle[i].replace("'", "''");
			if(viewtitle[i].equals("所有视窗")){
				ifall = true;
				break;
			}
			if(i<strtitle.length-1){
				viewsql = viewsql + viewtitle[i]+"' or view_table.title= '";
			}else{
				viewsql = viewsql + viewtitle[i]+"'";
			}
			
		}
		
		viewsql += ")";
		
//		if(strrange.equals("anywhere")){
//			keysql = " AND (note_table.notecontent LIKE '%" +strword1+"%'" +"OR note_table.notetitle LIKE '%"+strword2+"%')";
//		}else if(strrange.equals("note title")){
//			keysql = " AND note_table.notetitle LIKE '%"+strword2+"%'" ;
//		}else if(strrange.equals("note content")){
//			keysql = " AND note_table.notecontent LIKE '%" +strword1+"%'";
//		}
		
		
		if(!strfrtime.equals("earliest")&& !strtotime.equals("latest")){
			timesql = " AND note_table.createtime >'"+strfrtime+"'"
			+" AND note_table.createtime < '"+strtotime+"'";
		}else if(strfrtime.equals("earliest") && strtotime.equals("latest")){
			timesql =" " ;
		}else if(strfrtime.equals("earliest")&& !strtotime.equals("latest")){
			timesql = " AND note_table.createtime <'"+strtotime+"'";
		}else if(!strfrtime.equals("earliest")&& strtotime.equals("latest")){
			timesql =" AND note_table.createtime > '"+strfrtime+"'";
		}
				
		if(!ifall){
			strsql = "SELECT note_table.notetitle,view_table.title,DATE(note_table.createtime), note_table.noteid,SUBSTRING_INDEX(note_table.notecontent, ' ', 20),note_table.notecontent FROM `note_table` ,view_note, view_table WHERE "
				+viewsql+" AND view_table.idview=view_note.viewid AND note_table.noteid=view_note.noteid"+ timesql + keysql +";" ;
		
		}else if(ifall){
			
			strsql = "SELECT note_table.notetitle,view_table.title,DATE(note_table.createtime), note_table.noteid,SUBSTRING_INDEX(note_table.notecontent, ' ', 20),note_table.notecontent "+
					"FROM note_table ,view_table,view_note" +
					" WHERE view_table.idview = view_note.viewid AND note_table.noteid = view_note.noteid"+ timesql + keysql +";" ;
			
		}	
		
		System.out.println(strsql);
	    rs = this.DBConn.executeQuery(strsql);
	   	
		}catch(SQLException e){
			System.out.println("error:when get the notes which are contained by the view user chosen");
			e.printStackTrace();			
		}
		
		//Create CacheRowset of Resultset to filter and stem data
		CachedRowSet crs=new CachedRowSetImpl();
		crs.populate(rs);
//        rs.close();

//        Stemmer stemmer = new Stemmer();
        if(crs!=null){
        while(crs.next()){
        	System.out.println("Inside loop"+crs.getString(1));
			//    		if(strrange.equals("anywhere")){
			//			keysql = " AND (note_table.notecontent LIKE '%" +strword1+"%'" +"OR note_table.notetitle LIKE '%"+strword2+"%')";
			//		}else if(strrange.equals("note title")){
			//			keysql = " AND note_table.notetitle LIKE '%"+strword2+"%'" ;
			//		}else if(strrange.equals("note content")){
			//			keysql = " AND note_table.notecontent LIKE '%" +strword1+"%'";
			//		}
        	
//        	if(crs.isFirst())
//        		crs.deleteRow();
        	}
		}
		return crs;
		
	}
	
	/**
	 * 
	 */
	public List getMultipleAuthors(String noteid){
		
		List authorList = new ArrayList();
		ResultSet rs = null;
		String strsql = "select firstname,lastname from author_note,author_table where noteid="+noteid+" and author_note.authorid=author_table.authorid";
		System.out.println(strsql);
		try{
			rs = this.DBConn.executeQuery(strsql);
			
			while(rs.next()){
				String name = rs.getString(1) + " " + rs.getString(2);
				authorList.add(name);
				System.out.println("the author's name is "+rs.getString(1) + " " + rs.getString(2));
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		
		
		return authorList;
	}
}
