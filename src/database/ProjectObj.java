package database;

public class ProjectObj {
	private String[] groups;
	private String name;
	private String teacher;
	private String school;
	private String[] grades;
	
	public void SetName(String strname){
		this.name=strname;
	}
	
	public String GetName(){
		return this.name;
	}
	
	public void SetTeacher(String strteacher){
		this.teacher=strteacher;
	}
	
	public String GetTeacher(){
		return this.teacher;
	}
	
	public void SetSchool(String strschool){
		this.school = strschool;
	}
	
	public String GetSchool(){
		return this.school;
	}
	
	public void SetGroups(String[] strgroup){
		for(int i=0;i<strgroup.length;i++){
			this.groups[i]=strgroup[i];
		}
	}
	
	public String[] GetGroups(){
		return this.groups;
	}
	
	public void SetGrades(String[] strgrades){
		for(int i=0;i<strgrades.length;i++){
			this.grades[i]=strgrades[i];
		}
		
	}
	
	public String[] GetGrades(){
		return this.grades;
	}

}
