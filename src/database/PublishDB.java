/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import code.sqls;
import itm.controllers.PublishProject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * publish project DB
 * @author Stan
 */
public class PublishDB {
    public  static int InsertSavePublish(String db,PublishProject pp){
        sqls sql=new sqls();
        Connection conn=sql.getConn(db);
        PreparedStatement ps=null;
        String query="insert into publish (project_name,showID,threads_name,date) values (?,?,?,NOW());";
        String threadsName="";
        String[] threads=pp.getThreadsName();
        if(threads.length>1){
           for(String t:threads){
            threadsName+=t+".and^";
         } 
           threadsName=threadsName.substring(0, threadsName.length()-5);
        }else
            threadsName=threads[0];
        
        try{
            ps=conn.prepareStatement(query);
            ps.setString(1, pp.getProjectName());
            ps.setBoolean(2, pp.getShowID());
            ps.setString(3, threadsName);
            return ps.executeUpdate();
        } catch(SQLException e){
            return 0;
        }finally{
            sql.Close();
        }
    }
}
