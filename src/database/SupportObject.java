package database;

public class SupportObject {
	private long ID;
	private String text;
	
	public void setID(long id){
		this.ID = id;
	}
	
	public long getID(){
		return this.ID;
	}
	
	public void setText(String strtext){
		this.text = strtext;
		return;
	}
	
	public String getText(){
		return this.text;
	}

}
