package itm.controllers;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BaseController{

	public String query;
	public java.sql.Statement st;
	public java.sql.Connection conn;
	public String table;
	
	public static final String nl = System.getProperty("line.separator");
	
	public BaseController() 
			throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
		
	}
	
	public void dbClose(ResultSet rs) throws SQLException{
		if(null!=rs && !rs.isClosed()){
			st = rs.getStatement();
			conn = st.getConnection();
			
			rs.close();
			st.close();
			conn.close();
		}
	}
	
	
}
