package itm.controllers;

import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

import javax.servlet.http.HttpServletRequest;

import itm.models.*;
import java.io.UnsupportedEncodingException;


public class ProjectController extends BaseController {
	private ProjectModel p;
	public ProjectController(String dbname) 
			throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		super();
		p = new ProjectModel(dbname);
	}
	public void updateproject(HttpServletRequest req) throws SQLException, UnsupportedEncodingException{
		p.updateproject(req);
	}
	
	public void deleteproject(HttpServletRequest req) throws SQLException{
		p.DeleteProj(req);
	}
	
	public void recoverproject(HttpServletRequest req) throws SQLException{
		p.RecoverProj(req);
	}
	
}
