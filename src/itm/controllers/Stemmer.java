package itm.controllers;

import java.io.IOException;
import java.io.StringReader;
import java.util.HashSet;
import java.util.Set;

import org.apache.lucene.analysis.PorterStemFilter;
import org.apache.lucene.analysis.StopFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.tokenattributes.TermAttribute;
import org.apache.lucene.util.Version;

public class Stemmer {
	
	public String removeStopWordsAndStem(String input) throws IOException {
	    Set<String> stopWords = new HashSet<String>();
	    stopWords.add("a");
	    stopWords.add("I");
	    stopWords.add("the");

	    TokenStream tokenStream = new StandardTokenizer(
	            Version.LUCENE_30, new StringReader(input));
	    tokenStream = new StopFilter(true, tokenStream, stopWords);
	    tokenStream = new PorterStemFilter(tokenStream);

	    StringBuilder sb = new StringBuilder();
	    TermAttribute termAttr = tokenStream.getAttribute(TermAttribute.class);
	    while (tokenStream.incrementToken()) {
	        if (sb.length() > 0) {
	            sb.append(" ");
	        }
	        sb.append(termAttr.term());
	    }
	    return sb.toString();
	}

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		String one = "I decided buy something from the shop.";
	    String two = "Nevertheless I decidedly bought something from a shop.";
	    String three = "I deciding buying something from the shopping.";
//	    System.out.println(removeStopWordsAndStem(one));
//	    System.out.println(removeStopWordsAndStem(two));
//	    System.out.println(removeStopWordsAndStem(three));
	}

}
