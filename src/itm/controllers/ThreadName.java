/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package itm.controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * SuggestThread.jsp obj
 * @author Stan
 */
public class ThreadName {
    private String threadName="";
    private String keyWord="";
    private String title="";
    private String author="";    
    private String noteContent="";
    
    public ThreadName(String tn,String kw,String t,String a,String n){
        this.threadName=tn;
        this.keyWord=kw;
        this.title=t;
        this.author=a;
        this.noteContent=n;
    }    
   
    public ThreadName(){
        
    }
    public String getThreadName(){
        return threadName;
    }
    public void setThreadName(String threadname){
        this.threadName=threadname;
    }
    
    public String getKeyWord(){
        return keyWord;
    }
    public void setKeyWord(String keyword){
        this.keyWord=keyword;
    }
    
    public String getTitle(){
        return title;
    }
    public void setTitle(String title){
        this.title=title;
    }
    
    public String getAuthor(){
        return author;
    }
    public void setAuthor(String author){
        this.author=author;
    }
    
    public String getContent(){
        return noteContent;
    }
    public void setContent(String content){
        this.noteContent=content;
    }
    
}
