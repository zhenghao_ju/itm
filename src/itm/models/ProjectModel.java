package itm.models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import code.sqls;
import database.Operatedb;
import java.io.UnsupportedEncodingException;

public class ProjectModel extends BaseModel{

	public ProjectModel(String dbname) throws InstantiationException,
		IllegalAccessException, ClassNotFoundException, SQLException {
		super(dbname);
		table = "project";
	// TODO Auto-generated constructor stub
	}
	
	public ResultSet getProject(String database, String project) throws SQLException{
		sql = "SELECT * FROM " + database + ".project "+
			  "WHERE `projectname`='" + project+"'";
		return querySelect(sql);
	}
	
	public ResultSet getGrade(String database, String projectId) throws SQLException{
		sql = "SELECT * FROM " + database + ".project_grade "+
				  "WHERE `projectid`='"+projectId+"'";
			System.out.println(sql);
			return querySelect(sql);
	}

	public ResultSet getGroup(String database, String projectId) throws SQLException{
		sql = "SELECT * FROM " + database + ".project_group " +
			  "INNER JOIN group_table ON groupid = idgroup "+
			  "WHERE `projectid`='"+projectId+"'";
			System.out.println(sql);
			return querySelect(sql);
	}
	
	/*
 	 * Check the status of project if it is deleted status =1 ,if it exists status =0
 	 * */
 	 public boolean IsDeleted(String projectname)
 	 {
 			ResultSet rs = null;
 			int deleted = 0;
 		  //get the deleted status from the project table
 			String sql ="SELECT deleted from project where projectname ='"+ projectname+"'";
 			System.out.println(sql);
 			
 			try{
 				// System.out.println(strsql);
 				rs = querySelect(sql);
 				System.out.println("Resultset"+rs);
 				while(rs.next())
 				{
 					deleted = rs.getInt("deleted");
 				}
 				if(deleted == 1)
 				{
 					return true;
 				}
 				else
 				{
 					return false;
 				}
 			}catch(SQLException e){
 				System.out.println("GetDeleted status error");
 				e.printStackTrace();		
 			}
 			return false;
 	 }
	
	public String[] project_list(String id) throws SQLException{
		sql = "SELECT * FROM project where `part_id`='"+id+"'";
//		System.out.println(query);
		
		rs = querySelect(sql);
		ArrayList<String> manuList = new ArrayList();
		
		while(rs.next())
			manuList.add(rs.getString(3));
		
		manuList.add("None");
				
		String[] manuArray = manuList.toArray(new String[manuList.size()]);
		
		return manuArray;
	}
	
	public void DeleteProj(HttpServletRequest req) throws SQLException
	{
		String projectname = req.getParameter("projectname");
		//String database = req.getParameter("database");
		ResultSet rs = null;
		int proid = 0;
		int deleted = 1;
		
	  //get the project id from the project table
		sql = "SELECT * from project where projectname ='"+ projectname+"'";
		System.out.println(sql);
		rs = querySelect(sql);
		
		while(rs.next()){
			proid = rs.getInt("idProject");
		}
		
	  //change the project status from 0 to 1 in the project table
			sql = "UPDATE project " +
					"SET " +
					"deleted = '"+ deleted+"'"+
				    "WHERE idProject = "+proid;
			
			queryUpdate(sql);
	}
	
	public void RecoverProj(HttpServletRequest req) throws SQLException
	{
		String projectname = req.getParameter("projectname");
		//String database = req.getParameter("database");
		ResultSet rs = null;
		int proid = 0;
		int deleted = 0;
		
	  //get the project id from the project table
		sql = "SELECT * from project where projectname ='"+ projectname+"'";
		System.out.println(sql);
		rs = querySelect(sql);
		
		while(rs.next()){
			proid = rs.getInt("idProject");
		}
		
	  //change the project status from 1 to 0 in the project table
			sql = "UPDATE project " +
					"SET " +
					"deleted = '"+ deleted+"'"+
				    "WHERE idProject = "+proid;
			
			queryUpdate(sql);
	}
		
	public void updateproject(HttpServletRequest req) throws SQLException, UnsupportedEncodingException{
            req.setCharacterEncoding("utf-8");
		String preproject = req.getParameter("preproject");
		String proname = req.getParameter("projectname");
		String strteacher = req.getParameter("teacher");
		String strschool = req.getParameter("school");
		String[] arraygroup = req.getParameterValues("group");
		String[] arraygrade = req.getParameterValues("grade");
		String database = req.getParameter("database");
		String fromyear = req.getParameter("fromyear");
		String toyear = req.getParameter("toyear");
		
		String strvalue, condition = null;
		ResultSet rs = null;
		int proid = 0;
		int groupid = 0;
		int i;
		//System.out.println("**************************going into write new project process**********************");
		//get the project id from the project table
		
			sql = "SELECT * from project where projectname ='"+preproject+"'";
			System.out.println(sql);
			rs = querySelect(sql);
			
			while(rs.next()){
				proid = rs.getInt("idProject");
			}
		
		//write the relevant information into project table
		sql = "UPDATE project " +
				"SET " +
				"projectname = '"+proname+"', " +
				"teacher = '"+strteacher+"', " +
				"school = '"+strschool+"', " +
				"fromyear = '"+fromyear+"', " +
				"toyear = '"+toyear+"' " +
			    "WHERE idProject = "+proid;
		
		queryUpdate(sql);
		

			
		
		//write the records into project_grade table
		
		sql = "DELETE FROM project_grade WHERE projectid = "+proid;
		queryUpdate(sql);
		
		for(i=0; i< arraygrade.length;i++)
		{
			//System.out.println("user choosed "+arraygrade.length+" grades");
			//System.out.println("the "+i+"th grade is "+arraygrade[i]);
			strvalue = "(`projectid`, `grade`) VALUES ("+proid+" ,"+"'" +arraygrade[i]+"');";
			sql = "INSERT INTO project_grade " + strvalue;
			queryUpdate(sql);
			//System.out.println(strvalue);
//			WritebacktoDB("project_grade", "INSERT", strvalue);
			
			
			
		}
		
		//write the records into project_group table
		sql = "DELETE FROM project_group WHERE projectid = '"+proid + "'";
		System.out.println(sql);
		queryUpdate(sql);
		
		for(i=0; i<arraygroup.length;i++)
		{
			//get the group id from the group table
			try{
			rs = querySelect("SELECT idgroup from `group_table` where title ='"+arraygroup[i]+"';");
			if(rs.next())
				groupid = rs.getInt(1);
			strvalue = "(`projectid`, `groupid`) VALUES ("+proid+" ," +groupid+")";
			
			WritebacktoDB("project_group", "INSERT", strvalue);
			
			}catch(SQLException e){
				System.out.println("error3");
				e.printStackTrace();		
			}finally{
				if(rs != null){
					try{
						rs.close();
					}catch(SQLException e){
						e.printStackTrace();
					}
					
				}
			} 
			
		}
		
		//System.out.println("******************going out from write new project process successfully***********************");
//		queryUpdate(sql);
	}
}
