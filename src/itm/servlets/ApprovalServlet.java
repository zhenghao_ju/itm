/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package itm.servlets;

import code.MailUtilGmail;
import code.RegisterDatabase;
import java.io.IOException;
import java.io.PrintWriter;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Stan
 */
public class ApprovalServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /*
             * TODO output your page here. You may use following sample code.
             */
          //JDBC
            RegisterDatabase rd=new RegisterDatabase();
            String[] pars=new String[11];
            pars[0]=request.getParameter("Fname");
            pars[1]=request.getParameter("Lname");
            pars[2]=request.getParameter("url");
            pars[3]=request.getParameter("db");
            pars[4]=request.getParameter("org");
            pars[5]=request.getParameter("id");
            pars[6]=request.getParameter("Email");
            pars[7]=request.getParameter("phone");
            pars[8]=request.getParameter("password");
             pars[9]=request.getParameter("port");
              pars[10]=request.getParameter("status");
            rd.Approval_Register(pars[0],pars[1],pars[5],pars[8],pars[6],pars[7],pars[2],pars[9],pars[3],pars[4],pars[10]);
             //DB register approved, send email to notify user
            String from="itm.test.send@gmail.com";
            HttpSession ses=request.getSession();
          
             String subject="";
             String body="";
             String url="";
             String serverURL=request.getRequestURL().toString();
             serverURL=serverURL.substring(0,serverURL.length()-15);
            if((!(Boolean)ses.getAttribute("lan"))){
                subject="Your Database registration on ITM has been approved. (Do not reply)";
                body="Dear "+pars[0]+",\n\n"
                        + "Thanks for registering ITM database, you can go to <a href=\""+serverURL+"\">ITM</a> to use data from your database now. <br>This is an automated message from ITM, do not reply.<br><br>"
                        + "Idea Thread Mapper";
                url="/en/Approval.jsp";
            } else{
                subject="您的服务器注册申请已通过（勿回复）";
                body="尊敬的"+pars[1]+pars[0]+",<br>"
                        + "感谢您申请在ITM上注册数据库，我们很高兴地通知您，您的申请已经通过。现在即可在<a href=\""+serverURL+"\">ITM</a>上使用您所注册的数据库了。\n"
                        + "本邮件为自动生成，请勿回复。";
                url="/cn/Approval.jsp";
            }
            //send email
            try{
                MailUtilGmail.sendMail(pars[6], from, subject, body, true);
            } catch(MessagingException e){
                this.log(
                "Unable to send email. \n" +
                "You may need to configure your system as " +
                "described in chapter 15. \n" +
                "Here is the email you tried to send: \n" +
                "=====================================\n" +
                "TO: " + pars[6] + "\n" +
                "FROM: " + from + "\n" +
                "SUBJECT: " + subject + "\n" +
                "\n" +
                body + "\n\n");
            }
           RequestDispatcher dispatcher=getServletContext().getRequestDispatcher(url);
           dispatcher.forward(request, response);
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
