package itm.servlets;

import itm.controllers.ProjectController;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class addArea
 */
public class EditProject extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditProject() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {		
               req.setCharacterEncoding("UTF-8");
		res.setContentType("text/html;charset=UTF-8");
		try {
			String dbname = req.getParameter("database");
			ProjectController obj = new ProjectController(dbname);
			obj.updateproject(req);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		req.setAttribute("database",req.getParameter("database"));
		req.setAttribute("projectname",req.getParameter("projectname"));
		
                 HttpSession session=req.getSession();
                                        short isCN=Short.parseShort(session.getAttribute("lan").toString());
                                         String url ="";
                                        if(isCN==1)
					  url ="/cn/New_CreateProject.jsp?database="+req.getParameter("database")+"&ifsucceed=yes&projectname="+req.getParameter("projectname");
                                        else
                                           url ="/en/New_CreateProject.jsp?database="+req.getParameter("database")+"&ifsucceed=yes&projectname="+req.getParameter("projectname");         
		RequestDispatcher rd=getServletContext().getRequestDispatcher(url);
		rd.forward(req, res);	
	}

}
